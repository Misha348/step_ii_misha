#include "QueueWithPriority.h"
#include <iostream>

QueueWithPriority::QueueWithPriority(int maxSize)
{
	// �������� ������������ ����� �����
	this->maxSize = maxSize;
	// ��������� ����
	data = new int[maxSize];
	priorities = new int[maxSize];
	// �� ������� ����� �������
	size = 0;
}

QueueWithPriority::~QueueWithPriority()
{
	// ��������� �����
	delete[]data;
	delete[]priorities;
}


void QueueWithPriority::AddWithPriority(int elem, int priority)
{
	// ���� ����� �� ����,
	// ��� ���������� ����� �������
	// � �������� ����� �����
	if (!IsFull())
	{
		data[size] = elem;
		priorities[size] = priority;
		++size;
	}
}

int QueueWithPriority::ExtractElemWithHighPriority()
{
	// ���� ����� �� �������, ��� ���������
	// ���, ���� ������� ������ � "��������" �����
	if (!IsEmpty())
	{
		// �������� ������ �������		
		int prIndex = 0;
		int prValue = priorities[0];

		// ����� ���������������� ��������
		for (int i = 1; i < size; ++i)
		{
			if (priorities[i] > prValue)
			{
				prValue = priorities[i];
				prIndex = i;
			}
		}

		int element = data[prIndex];

		// ϳ������� �� ��������
		for (int i = prIndex; i < size - 1; i++)
		{
			data[i] = data[i + 1];
			priorities[i] = priorities[i + 1];
		}

		// ������ �����
		--size;					// �������� ����� �����

		// ʳ������ �����
		//data[size - 1] = element;	// ���������� ������ ������� ������
								// �� ������� ����� �����

		return element; // ��������� ���������� �������
	}
}

int QueueWithPriority::Peek() const
{
	// �������� ������ �������		
	int prIndex = 0;
	int prValue = priorities[0];

	// ����� ���������������� ��������
	for (int i = 1; i < size; ++i)
	{
		if (priorities[i] > prValue)
		{
			prValue = priorities[i];
			prIndex = i;
		}
	}
	return data[prIndex];
}

bool QueueWithPriority::IsEmpty() const
{
	// �������?
	return size == 0;
}

bool QueueWithPriority::IsFull() const
{
	// ����������� ���������?
	return size == maxSize;
}

int QueueWithPriority::GetCount() const
{
	// ����� �����
	return size;
}

void QueueWithPriority::Clear()
{
	// ������� �����
	size = 0;
}

void QueueWithPriority::Show() const
{
	std::cout << "\n-----------------------------------\n";
	// ����� �����
	for (int i = 0; i < size; i++)
		std::cout << data[i] << "\tPriority: " << priorities[i] << std::endl;
	std::cout << "\n-----------------------------------\n";
}
