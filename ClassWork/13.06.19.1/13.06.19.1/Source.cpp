#define _CRT_SECURE_NO_WARNINGS

#include "QueueWithPriority.h"
#include <ctime>
#include <cstdlib>
#include <iostream>

using std::cout;
using std::endl;

void main()
{
	srand(time(NULL));

	QueueWithPriority q1(7);

	while (!q1.IsFull())
		q1.AddWithPriority(rand() % 100, rand() % 5);

	q1.Show();
	cout << "Peek element: " << q1.Peek() << endl;
	q1.Show();
	cout << "Extrack element: " << q1.ExtractElemWithHighPriority() << endl;
	q1.Show();

	while (!q1.IsEmpty())
		cout << "Extrack element: " << q1.ExtractElemWithHighPriority() << endl;

	q1.Show();


	time_t now = time(0);
	tm* localtm = localtime(&now);
	cout << "The local date and time is: " << asctime(localtm) << endl;

	system("pause");
}