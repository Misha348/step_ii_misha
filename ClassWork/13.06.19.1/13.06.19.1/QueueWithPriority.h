#pragma once
#pragma once

// ����� � ���������� - �������� ��������� �����, �� ����������� ��
// ���� ������������� ���� ��������
class QueueWithPriority
{
	// �����
	int * data;
	// ��������
	int * priorities;
	// ������������ ����� �����
	int maxSize;
	// ��������� ����� �����
	int size;

public:
	// �����������
	
	QueueWithPriority(int maxSize);
	// ����������
	~QueueWithPriority();

	// ���������� �������� � �����
	void AddWithPriority(int elem, int priority);
	// ��������� �������� � �����
	int ExtractElemWithHighPriority();
	// ���������� ��������������� �������
	int Peek() const;
	// �������� �� ����� � ���������
	bool IsEmpty() const;
	// �������� �� ����� � �����
	bool IsFull() const;
	// ʳ������ �������� � ����
	int GetCount() const;
	// ������� �����
	void Clear();
	// ����� �������� � ����
	void Show() const;
};
