#pragma once
#include <iostream>
using namespace std;

class SmartArr
{
private:
	int **arr;
	int rows;
	int cols;

public:
	void AddRow();
	void ShowArr() const;

	SmartArr();
	SmartArr(int val, int rows, int cols);

	SmartArr(const SmartArr& arr);



	~SmartArr();





};
