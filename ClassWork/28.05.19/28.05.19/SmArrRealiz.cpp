#include <iostream>
#include "SmartArr.h"
using namespace std;


SmartArr::SmartArr()
{
	int **arr = nullptr;
	rows = 0;
	cols = 0;
}

SmartArr::SmartArr(int val, int rows, int cols)
{
	this->rows = rows;
	this->cols = cols;

	arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		arr[i] = new int[cols];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = val;
		}
	}
}

void SmartArr::ShowArr() const
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << arr[i][j] << " ";
		}
		cout << endl;
	}
}

SmartArr::~SmartArr()
{
	for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;
}

SmartArr::SmartArr(const SmartArr& arr)
{
	this->rows = arr.rows;
	this->cols = arr.cols;

	this->arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		this->arr[i] = new int[cols];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			this->arr[i][j] = arr.arr[i][j];
		}		
	}

}

void AddRow()
{

}