#include <iostream>
#include "Header.h"
using namespace std;


Time24::Time24(int hou, int min, int sec)
{
	this->hours = hou;
	this->minutes = min;
	this->seconds = sec;
}

void Time24::ShowTime24()
{
	cout << this->hours << ":" << this->minutes << ":" << this->seconds << endl;
}

void Time24::ConvertSec(int sec)
{
	int tmpSec = sec;

	this->hours = tmpSec / 3600;
	this->seconds = tmpSec - (this->hours * 3600);
	this->minutes = this->seconds / 60;
	this->seconds = this->seconds - (this->minutes * 60);
}

void Time24::SetTime24(int sec, int min, int hou)
{
	this->seconds = sec;
	this->minutes = min;
	this->hours = hou;
}

int Time24::GetTime24InSec() const
{
	return (this->hours * 3600) + (this->minutes * 60) + this->seconds;
}

Time24::operator Time()
{
	if (this->hours > 12 && this->hours <= 23)
	{
		this->hours = (this->hours - 12);
		AmPm = false;

		if (this->seconds >= 0 && this->seconds <= 30)
		{
			this->minutes += 0;
			this->seconds += 0;
		}
		if (this->seconds > 30 && this->seconds <= 59)
		{
			this->minutes += 1;
			this->seconds = 0;
		}
	}

	else if (this->hours >= 0 && this->hours <= 11)
	{
		this->hours += 0;
		AmPm = true;

		if (this->seconds >= 0 && this->seconds <= 30)
		{
			this->minutes += 0;
			this->seconds += 0;
		}
		if (this->seconds > 30 && this->seconds <= 59)
		{
			this->minutes += 1;
			this->seconds = 0;
		}
	}

	return Time(hours, minutes, AmPm);
}



Time24::~Time24() {};



Time::Time(int hou, int min, bool amPm)
{
	this->hour = hou;
	this->minute = min;
	this->period = amPm;
}

void Time::ShowTime()
{
	if (this->hour == 12 && this->minute >= 0)
	{
		cout << this->hour << ":" << this->minute << "p.m." << endl;
	}
	else if(this->hour <= 11 )
		cout << this->hour << ":" << this->minute << "a.m." << endl;	
}

Time::operator Time24()
{
	if (this->period == true)
	{
		this->hour += 0;
		this->minute += 0;
	}

	else if (this->period == false)
	{
		this->hour += 12;
		this->minute -= 0;
	}

	return Time24(hour, minute, 0);
}

Time::~Time() {};