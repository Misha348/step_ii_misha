#pragma once
#include <iostream>
using namespace std;

class Time;

class Time24
{
private:
	int seconds, minutes, hours;
	bool AmPm;

	friend ostream& operator << (ostream& out, const Time24& tm);

public:

	Time24() : seconds(0), minutes(0), hours(0) {}
	Time24(int hou, int min, int sec);

	void ShowTime24();
	void ConvertSec(int sec);
	void SetTime24(int sec, int min, int hou);
	int GetTime24InSec() const;



	operator Time();	

	~Time24();
};


class Time
{
private:
	int hour, minute;
	bool period;

	friend ostream& operator << (ostream& out, const Time& tm);

public:
	Time() : minute(0), hour(0) {}
	Time(int hou, int min, bool amPm);

	void ShowTime();


	operator Time24();
	
	

	~Time();
};