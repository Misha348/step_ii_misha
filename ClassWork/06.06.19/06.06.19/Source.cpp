#include <iostream>
#include "Header.h"
using namespace std;

ostream& operator << (ostream& out, const Time24& tm)
{
	if (tm.AmPm == true)
		out << tm.hours << " : " << tm.minutes << " " << "am" << endl;
	if (tm.AmPm == false)
		out << tm.hours << " : " << tm.minutes << " " << "pm" << endl;

	return out;
}

ostream& operator << (ostream& out, const Time& tm)
{
	out << tm.hour << " : " << tm.minute << " : " << 0 << endl;
	return out;
}

int main()
{
	Time24 time1 = Time24(18, 12, 59);
	Time time2;

	time2 = (Time)time1;
	cout << time1;
	cout << "=======================" << endl;
	bool periodAmPm;
	int choise = 0;
	cout << "enter period: [ 1 ] = am.  [ 2 ] - pm."; cin >> choise;

	if (choise == 1)
	{
		periodAmPm = true;
	}
	if (choise == 2)
	{
		periodAmPm = false;
	}

	Time time3 = Time(10, 35, periodAmPm);
	Time24 time4;

	time4 = (Time24)time3;
	cout << time3;

	system("pause");
	return 0;
}