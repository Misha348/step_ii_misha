#pragma once
#include <string>
#include <iostream>
using namespace std;

template <typename T>
class Value
{
public:
	Value() : Value(0) {};
	~Value();
	Value(T num);

	void Print() const;
	Value(const Value &);
	Value(Value &&);
	Value & operator = (const Value&);
	Value & operator = (Value&&);


private:
	T * pValue;
	string name = "o";
	static int id;
};

template <typename T>
Value<T>::~Value()
{
	if (pValue != nullptr)
		delete pValue;
	pValue = nullptr;
}

template <typename T>
void Value<T>::Print() const
{
	if (this->pValue != nullptr)
		cout << name << " : " << *pValue << endl;
	else
		cout << "pValue doesn't exist!!" << endl;
}

template <typename T>
Value<T>::Value(T num)
{
	pValue = new T;
	*pValue = num;
	++id;
	name = name + to_string(id);

}

template <typename T>
Value<T>::Value(const Value<T> & other)
{
	id++;
	*pValue = *other.pValue;
	this->name = name + to_string(id);
}

template <typename T>
Value<T>::Value(Value<T> && other)
{
	pValue = other.pValue;
	other.pValue = nullptr;
	//this->name = name + to_string(id);
}

template <typename T>
Value<T> & Value<T>::operator=(const Value<T> & other)
{
	if (this != &other)
	{
		delete pValue;
		pValue = new  T;
	}
	*pValue = *other.pValue;
	return *this;
}

template <typename T>
Value<T> & Value<T>::operator=(Value<T> && other)
{
	if (this->pValue != other.pValue)
	{
		pValue = other.pValue;
		other.pValue = nullptr;
	}
	return *this;
}
