#pragma once
#include <string>
#include <iostream>
using namespace std;

template <typename T>
class DynArr
{
private:
	T * arr;
	int size;
	int maxSize;
	const int grow = 1;

	template <typename T>
	friend ostream& operator << (ostream& out, const DynArr<T>& arr);

public:	
	DynArr();
	DynArr(T size, T max = 0);	
	DynArr(const DynArr& other);

	DynArr(DynArr&& other);
	DynArr& operator=(DynArr&& other);
	DynArr& operator=(const DynArr& other);

	void AddElBack(T val);
	void InsertAt(T val, int pos);
	void DeleteFirst();
	void Resize();
	void freeExtra();

	int GetUpperBound();	
	int GetMaxSize();
	

	~DynArr();
	void FillArr();	
};

template<typename T>
void DynArr<T>::freeExtra()
{
	this->size = (GetMaxSize() - 2);
	DynArr<T> tmpArr(size);
	for (int i = 0; i <= size; i++)
	{
		tmpArr.arr[i] = arr[i];
	}
	*this = move(tmpArr);	
}

template<typename T>
void DynArr<T>::Resize()
{
	DynArr<T> tmpArr (size+grow);

	for (int i = 0; i < size; i++)
	{
		tmpArr.arr[i] = arr[i]; 
	}	
	*this = tmpArr;		
}

template<typename T>
void  DynArr<T>::InsertAt(T val, int pos)
{
	if (this->size == maxSize)
		Resize();
	
	DynArr<T> tmpArr(size + grow);
	for (int i = 0; i < pos; i++)
	{
		tmpArr.arr[i] = arr[i];
	}
	tmpArr.arr[pos] = val;
	size++;
	for (int i = (pos + 1); i < size+1; i++)
	{
		tmpArr.arr[i] = arr[i - 1];
	}
	*this = tmpArr;
}

template<typename T>
void DynArr<T>::AddElBack(T val)
{
	if (this->size == maxSize)
	{
		Resize();
		arr[GetUpperBound() + 1] = val;
	}		
	else
		arr[GetUpperBound() + 1] = val;	
		size++;
}

template<typename T>
void DynArr<T>::DeleteFirst()
{	
	DynArr<T> tmpArr(size );
	for (int i = 0; i < size ; i++)
	{
		tmpArr.arr[i] = arr[i + 1]; 
	}
	*this = tmpArr;	
	--size;
}

template<typename T>
int DynArr<T>::GetUpperBound()
{	
	return (size - 1);
}

template<typename T>
ostream& operator << (ostream& out, const DynArr<T>& arr)
{	
	for (int i = 0; i <= arr.size; i++)
	{
		out << arr.arr[i] << "   ";
	}
	return out;	
}

template <typename T>
DynArr<T>::DynArr()
{
	arr = nullptr;
	size = 0;
	maxSize = 0;
}

template<typename T>
DynArr<T>::DynArr(T size, T max)
{
	this->maxSize = grow + size; 
	this->size = size;
	arr = new T [maxSize];
	for (int i = 0; i < maxSize; i++)
	{
		arr[i] = 0;
	}
}

template<typename T>
int DynArr<T>::GetMaxSize()
{		
	return maxSize;
}

template<typename T>
DynArr<T>::DynArr(const DynArr<T>& other)
{
	this->size = other.size;
	this->arr = new T[other.size];
	this->maxSize = other.maxSize;
	for (int i = 0; i < size; i++)
	{
		this->arr[i] = other.arr[i];
	}
}

template<typename T>
DynArr<T>& DynArr<T>::operator=(const DynArr<T> & other)
{
	if (arr != nullptr) delete[] arr;			

	
	this->size = other.size;
	this->maxSize = other.maxSize;
	arr = new T[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = other.arr[i];
	}
	
	return *this;
}

template<typename T>
DynArr<T>::DynArr(DynArr<T> && other)
{
	this->size = other.size;
	this->arr = other.arr;
	other.arr = nullptr;
	this->maxSize = other.maxSize;
}

template<typename T>
DynArr<T>& DynArr<T>::operator=(DynArr<T> && other)
{
	if (this == &other)
		return *this;

	if (other.arr == nullptr)
	{
		this->arr = nullptr;
		this->size = 0;
	}
	else
	{
		this->arr = other.arr;
		this->size = other.size;
		this->maxSize = other.maxSize;

		other.arr = nullptr;
	}
	return *this;
}

template<typename T>
void DynArr<T>::FillArr()
{
	for (int i = 0; i < size; i++)
	{
		cout << "el. - " << i << " = "; cin >> arr[i];
	}
}

template<typename T>
DynArr<T>::~DynArr()
{
	delete[] arr;
}





