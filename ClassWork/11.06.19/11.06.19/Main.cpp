#include <string>
#include <iostream>
#include "TplClass.h"
using namespace std;

int main()
{
	DynArr<int> arr(3);
	arr.FillArr();
	cout << arr;
	cout << "\n=======================\n" << endl;

	arr.AddElBack(50);
	cout << arr;
	cout << "\n=======================\n" << endl;
	
	arr.DeleteFirst();
	cout << arr;
	cout << "\n=======================\n" << endl;
	
	arr.AddElBack(10);
	cout << arr;
	cout << "\n=======================\n" << endl;
	
	arr.InsertAt(66, 1);
	cout << arr;
	cout << "\n=======================\n" << endl;

	int maxS = 	arr.GetMaxSize();
	cout << maxS;
	cout << "\n=======================\n" << endl;

	arr.freeExtra();
	cout << arr;
	   

	system("pause");
	return 0;
}


