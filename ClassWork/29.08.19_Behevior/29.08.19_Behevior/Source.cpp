#include <iostream>
#include <string>
#include <ctime>
#include <conio.h>
#include <windows.h>
using namespace std;

class  Figure
{
private:
	int coord_X;
	int coord_Y;
	int width;
	int high;

public:
	Figure(int X, int Y, int W, int H) : coord_X(X), coord_Y(Y), width(W), high(H) {	}

	void SetCoord(int X, int Y)
	{
		if (X > 0 && Y > 0)
		{
			coord_X = X;
			coord_Y = Y;
		}
	}

	void SetSize(int W, int H)
	{
		if (W > 0 && H > 0)
		{
			width = W;
			high = H;
		}
	}

	void RandomMove()
	{
		coord_X = rand() % 20;
		coord_X = rand() % 20;
	}

	void RandomChangeSize()
	{
		width = rand() % 5;
		high = rand() % 5;
	}

	void ChangeSize(int newWidth, int newHigh)
	{
		SetSize(newWidth, newHigh);
	}

	void ChangeCoord(int X, int Y)
	{
		SetCoord(X, Y);
	}

	void PrintFigure()
	{
		for (int i = 0; i < coord_Y; i++)
		{
			cout << "\n";
		}

		for (int i = 0; i < high; i++)
		{
			for (int i = 0; i < coord_X; i++)
			{
				cout << " ";
			}	
			for (int j = 0; j < width; j++)
			{
				cout << "#";
			}
			cout << endl;
		}
	}

	void Round()
	{
		system("cls");
		RandomChangeSize();
		RandomMove();
		PrintFigure();
	}

	~Figure()
	{
	}

};


int main()
{
	srand(time(0));
	Figure fig = Figure(2, 2, 4, 4);
	fig.PrintFigure();
	Sleep(2000);
	system("cls");

	fig.ChangeCoord(6, 7);
	fig.PrintFigure();
	Sleep(2000);
	system("cls");

	fig.RandomChangeSize();
	fig.PrintFigure();
	Sleep(2000);
	system("cls");
	int input;

	do
	{		
		input = _getch();
		switch (input)
		{
		case (int)' ': fig.Round();
			break;
		case 's':
			break;
		case 'z':
			break;
		}
	} while (input != (int)'q');



	system("pause");
	return 0;
}