#include <iostream>
#include <string>
using namespace std;

class IRealCarDrivingSkils
{
public:
	virtual void MoveKeyInLock() = 0;
	virtual void TurnHelmToLeft() = 0;
	virtual void TurnHelmToRight() = 0;
	virtual void PressGassPedalWithFrontwardTransmission() = 0;
	virtual void PressGassPedalWithBackwardTransmission() = 0;
	virtual void PressBreakPedal() = 0;
};

class RealCarDrivingSkils : public IRealCarDrivingSkils
{
public:
	void MoveKeyInLock()
	{
		cout << "CAR - engeen had been started." << endl;
	}
	void TurnHelmToLeft()
	{
		cout << "CAR - the car moves to the left." << endl;
	}
	void TurnHelmToRight()
	{
		cout << "CAR - the car moves to the right." << endl;
	}
	void PressGassPedalWithFrontwardTransmission()
	{
		cout << "CAR - the car moves straight." << endl;
	}
	void PressGassPedalWithBackwardTransmission()
	{
		cout << "CAR - the car moves backward." << endl;
	}
	void PressBreakPedal()
	{
		cout << "CAR - the car moves slowler." << endl;
	}
};

class IGameCarDrivingSkils
{
	virtual void PressButtonL1() = 0;
	virtual void PressButtonL2() = 0;
	virtual void PressLeftArrow() = 0;
	virtual void PressRightArrow() = 0;
	virtual void PressUpperArrow() = 0;
	virtual void PressDownArrow() = 0;
};

class GameCarDrivingSkils: public IGameCarDrivingSkils
{
public:
	void PressButtonL1()
	{
		cout << "button L1 -  switch ON car's engeen." << endl;
	}
	void PressButtonL2()
	{
		cout << "button L2 - switch OFF car's engeen." << endl;
	}

	void PressLeftArrow()
	{
		cout << "button LEFT ARROW - turn car to LEFT." << endl;
	}
	void PressRightArrow()
	{
		cout << "button RIGHT ARROW - turn car to RIGHT." << endl;
	}
	void PressUpperArrow()
	{
		cout << "button UPPER ARROW - move car FOREWARD." << endl;
	}
	void PressDownArrow()
	{
		cout << "button DOWN ARROW - the car moves slowler / car moves BACKWARD." << endl;
	}
	
};

class Adapter: public IRealCarDrivingSkils
{
private:
	GameCarDrivingSkils *skill;

public:
	Adapter(GameCarDrivingSkils *skills): skill(skills) {}

	void MoveKeyInLock()
	{
		skill->PressButtonL1();
	}	
	void TurnHelmToLeft()
	{
		skill->PressLeftArrow();
	}
	void TurnHelmToRight()
	{
		skill->PressRightArrow();
	}
	void PressGassPedalWithFrontwardTransmission()
	{
		skill->PressUpperArrow();
	}
	void PressGassPedalWithBackwardTransmission()
	{
		skill->PressDownArrow();
	}

	void PressBreakPedal()
	{
		skill->PressButtonL2();
	}	
};

class Driver
{
public:	
	void Drive(IRealCarDrivingSkils &driver)
	{		
		driver.MoveKeyInLock();
		driver.PressGassPedalWithFrontwardTransmission();
		driver.PressGassPedalWithBackwardTransmission();
		
		driver.TurnHelmToLeft();
		driver.TurnHelmToRight();
		driver.PressBreakPedal();
	}
};

int main()
{
	RealCarDrivingSkils *setOfRealSkills = new RealCarDrivingSkils();
	GameCarDrivingSkils *setOfGameSkills = new GameCarDrivingSkils();	

	Driver driver;
	Adapter ad(setOfGameSkills);
	driver.Drive(ad);

	system("pause");
	return 0;
}