#include <iostream>
#include <string>
#include <vector>
#include "CartDeck.h"
using namespace std;

int main()
{
	Deck myDeck;
	myDeck.InitDeck();
	myDeck.PrintDeck();
	myDeck.Shufle();
	myDeck.PrintDeck();

	system("pause");
	return 0;

}