#pragma once
#include <iostream>
#include <string>
using namespace std;

class DynamicArr
{
private:
	int *arr;
	int size;

	friend ostream& operator << (ostream& out, const DynamicArr& ar);

public:	
	DynamicArr();
	DynamicArr(int size);
	DynamicArr(const DynamicArr& arr);

	DynamicArr(DynamicArr&& other);
	DynamicArr& operator=(DynamicArr&& other);

	void FillArr();
	void ShowArr();
	~DynamicArr();

	operator int()
	{
		int sum = 0;
		for (int i = 0; i < size; i++)
		{
			sum += this->arr[i];
		}
		return sum;
	}

	operator char*()
	{
		char* str = new char[size + 1];

		for (int i = 0; i < size; i++)
		{
			str[i] = '0' + arr[i];
		}
		str[size] = '\0';

		return str;
	}
	   	


	DynamicArr operator+(const DynamicArr &arr1) const
	{
		DynamicArr newArr(size + arr1.size);

		for (int i = 0; i < size; i++)
		{
			newArr.arr[i] = arr[i];
		}
		for (int i = size, j = 0; i < size + arr1.size; i++, ++j)
		{
			newArr.arr[i] = arr1.arr[j];
		}		
		return newArr;
	}

	DynamicArr& operator=(const DynamicArr &other)
	{	
		if (arr != nullptr) delete[] arr;
		arr = new int[other.size];
		for (int i = 0; i < other.size; i++)
		{
			arr[i] = other.arr[i];
		}
		this->size = other.size;
		return *this;
	}

	void operator()(int numb);

	int& operator[](int index)
	{
		return arr[index];
	}

	bool operator>(const DynamicArr &other) const
	{
		return this->size > other.size;
	}

	bool operator < (const DynamicArr &other) const
	{
		return this->size < other.size;
	}

	bool operator >= (const DynamicArr &other) const
	{
		return this->size >= other.size;
	}

	bool operator <= (const DynamicArr &other) const
	{
		return this->size <= other.size;
	}

	bool operator == (const DynamicArr &other) const
	{
		if (this->size == other.size)
		{
			int c = 0;
			for (int i = 0; i < size; i++)
			{		
				if (this->arr[0] != other.arr[0])
					c++;
				if (this->arr[0] == other.arr[0])
					continue;
								
			}
			if (c == 0)
				return this->arr == other.arr;
			else
				return this->arr != other.arr;
		}
		
		//return this->size == other.size;
		
	}

	











	





	




	

};