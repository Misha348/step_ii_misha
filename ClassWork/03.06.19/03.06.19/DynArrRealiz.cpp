#include <iostream>
#include <string>
#include "DynArr.h"
using namespace std;

DynamicArr::DynamicArr()
{
	arr = nullptr;
	size = 0;
}

DynamicArr::DynamicArr(int size)
{
	this->size = size;
	arr = new int[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = 0;
	}
}

//copy
DynamicArr::DynamicArr(const DynamicArr& other)
{
	this->size = other.size;

	
	this->arr = new int[other.size];
	for (int i = 0; i < size; i++)
	{
		this->arr[i] = other.arr[i];
	}
}

void DynamicArr::FillArr()
{
	for (int i = 0; i < size; i++)
	{
		cout << "el. - " << i << " = "; cin >> arr[i];		
	}
}

void DynamicArr::ShowArr()
{	
	cout << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "el. - " << i << " = " << arr[i] << endl;
	}
}

void DynamicArr::operator()(int numb)
{
	for (int i = 0; i < size; i++)
	{
		this->arr[i] += numb;
	}
}

DynamicArr::~DynamicArr()
{
	delete[] arr;
}

//-----------------------------------------------------

DynamicArr::DynamicArr(DynamicArr&& other)
{
	this->size = other.size;
	this->arr = other.arr;
	other.arr = nullptr;
}

DynamicArr& DynamicArr::operator=(DynamicArr&& other)
{
	if (this == &other)
		return *this;

	if (other.arr == nullptr)
	{
		this->arr = nullptr;
		this->size = 0;
	}
	else
	{
		this->arr = other.arr;
		this->size = other.size;
		other.arr = nullptr;
	}
	return *this;
}