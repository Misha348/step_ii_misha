#include <iostream>
#include <string>
#include "DynArr.h"
using namespace std;

ostream& operator << (ostream& out, const DynamicArr& ar)
{
	for (int i = 0; i < ar.size; i++)
	{
		out << ar.arr[i] << "\t";
	}
	return out;
}

DynamicArr CreateDynamicArr()
{
	DynamicArr arr(4);
	return arr;
}


int main()
{
	DynamicArr myArr = CreateDynamicArr();
	myArr.FillArr();
	cout << myArr;

	int size;
	cout << "enter size: "; cin >> size;
	DynamicArr arr = DynamicArr(size);
	arr.FillArr();
	cout << "==================" << endl;
	arr.ShowArr();
	cout << "==================" << endl;

	//
	int size2;		
	cout << "enter size: "; cin >> size2;//
	DynamicArr arr1 = DynamicArr(size2);//
	arr1.FillArr();
	cout << "==================" << endl;

	DynamicArr arrRes = DynamicArr(size + size2);//
	
	arrRes = (arr + arr1);	
	arrRes.ShowArr();
	cout << "==================" << endl;

	//
	/*arr1 = arr;
	arr1.ShowArr();	
	cout << "==================" << endl;

	if (arr > arr1)
	{
		cout << "true";
	}		
	else
		cout << "false" << endl;
	cout << "==================" << endl;*/
	
	if (arr == arr1)
	{
		cout << "true - eq" << endl;
	}
	else
		cout << "false - not eq" << endl;
	cout << "==================" << endl;

	cout << arr1[1] << endl;
	cout << "==================" << endl;

	arr1(10);
	arr1.ShowArr();
	cout << "==================" << endl;

	int someVal = 0;
	someVal = (int)arr1;
	cout << "Trasform to (int) = " << someVal << endl;
	cout << "==================" << endl;



	//char someArr[10];
	char* str = (char*)arr1;	
	cout << "Trasform to (char) = " << str << endl;
	cout << "==================" << endl;
	cout << arr1;
	cout << "==========================================" << endl;

	

	string str12(str);

	str12.length();
	str12.c_str();


	system("pause");
	return 0;
}