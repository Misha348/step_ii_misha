#include <iostream>
#include <string>
#include <fstream>
using namespace std;

struct Player
{
	string name;
	string surname;
	int point;
	int games;
	double averPointsPerGame;
};

double AvPointsPerGame(const Player &player);

void CreaeteNewPlayer(Player &player);
void AddOnePlayer(Player *&arrPlayer, int &quantity, const Player &newPlayer);
void RemovePlayer(Player *&arrPlayer, int &quantity, int posPlayerToDel);

int SearchByName(Player *&arrPlayer, int &quantity);
void ShowPlayer(Player &player);
void ShowPlayers(Player *arrPlayer, int quantity);

void AddPlayersToFile(Player *&arrPlayer, int quantity);
void GetPlayersFromFile(Player *&arrPlayer, int quantity);

void Menu()
{
	cout << "[ 1 ] - Add player.\n[ 2 ] - Remove player by name.\n[ 3 ] - Find player." << endl;
	cout << "[ 4 ] - Show players.\n[ 5 ] - Save into file players.\n[ 6 ] - Get from file players.\n[ 0 ] - Exit. " << endl;
}

int main()
{
	int choise = 0;
	bool exit = false;

	int quantity = 0;
	Player player;
	Player *arrPlayer = nullptr;

	while (exit != true)
	{
		system("cls");
		Menu();
		cin >> choise;
		if(choise == 1)
		{
			CreaeteNewPlayer(player);
			AddOnePlayer(arrPlayer, quantity, player);
		}
		if(choise == 3)
		{
			cout << "Enter name for searching: ";
			int posToShow = SearchByName(arrPlayer, quantity);
			ShowPlayer(arrPlayer[posToShow]);
			system("pause");
		}
		if(choise == 2)
		{
			cout << "Enter name for removing: ";
			int posPlayerToDel = SearchByName(arrPlayer, quantity);
			RemovePlayer(arrPlayer, quantity, posPlayerToDel);
		}
		if(choise == 4)
		{
			ShowPlayers(arrPlayer, quantity);
			system("pause");
		}
		if (choise == 5)
		{
			AddPlayersToFile(arrPlayer, quantity);
			cout << "Has saved." << endl;
			system("pause");
		}
		if (choise == 6)
		{
			GetPlayersFromFile(arrPlayer, quantity);
			system("pause");
		}
		if (choise == 0)
		{
			exit = true;
			break;
		}
	}	

	system("pause");
	return 0;
}

void CreaeteNewPlayer(Player &player)
{	
	cout << "  Name: "; cin >> player.name;
	cout << "  Surname: "; cin >> player.surname;
	cout << "  Points: "; cin >> player.point;
	cout << "  Games: "; cin >> player.games;
	player.averPointsPerGame = AvPointsPerGame(player);
	cout << endl;	
}

double AvPointsPerGame(const Player &player)
{
	return (double)player.games / player.point;
}

void ShowPlayer(Player &player)
{		
	cout << "  name: " << player.name << endl;
	cout << "  surname: " << player.surname << endl;
	cout << "  points: " << player.point << endl;
	cout << "  games: " << player.games << endl;
	cout << "  aver.point: " << player.averPointsPerGame << endl;			
}

void ShowPlayers(Player *arrPlayer, int quantity)
{
	for (int i = 0; i < quantity; i++)
	{
		cout << "=== [ " << i + 1 << " ] ===" << endl;
		ShowPlayer(arrPlayer[i]);
		cout <<  endl;
	}
}

void AddOnePlayer(Player *&arrPlayer, int &quantity, const Player &newPlayer)
{
	Player *tmpArrPlayer = new Player[quantity + 1];
	for (int i = 0; i < quantity; i++)
	{
		tmpArrPlayer[i] = arrPlayer[i];
	}
	tmpArrPlayer[quantity] = newPlayer;
	delete[] arrPlayer;
	arrPlayer = tmpArrPlayer;
	quantity++;	
}

void RemovePlayer(Player *&arrPlayer, int &quantity, int posPlayerToDel)
{	
	if (posPlayerToDel < 0 || posPlayerToDel >= quantity)
		return;
	Player *tmpArrPlayer = new Player[quantity - 1];
	for (int i = 0, j = 0; i < quantity; i++)
	{
		if (i != posPlayerToDel)
		{
			tmpArrPlayer[j] = arrPlayer[i];
			j++;
		}
	}
	delete[] arrPlayer;
	arrPlayer = tmpArrPlayer;
	quantity--;
}

void AddPlayersToFile(Player *&arrPlayer, int quantity)
{
	ofstream wrFile("File_For_Players.txt", ios_base::out);	
	for (int i = 0; i < quantity; i++)
	{
		wrFile << arrPlayer[i].name << endl;
		wrFile << arrPlayer[i].surname << endl;
		wrFile << arrPlayer[i].point << endl;
		wrFile << arrPlayer[i].games << endl;
		wrFile << arrPlayer[i].averPointsPerGame << endl;
	}	
	wrFile.close();
}

void GetPlayersFromFile(Player *&arrPlayer, int quantity)
{	
	Player *newArrPlayer = new Player[quantity];
	ifstream in("File_For_Players.txt", ios_base::in);
	for (int i = 0; i < quantity; i++)
	{
		in >> newArrPlayer[i].name;
		in >> newArrPlayer[i].surname;
		in >> newArrPlayer[i].point;
		in >> newArrPlayer[i].games;
		in >> newArrPlayer[i].averPointsPerGame;
	}		
	in.close();
	delete[] arrPlayer;
	arrPlayer = newArrPlayer;

	ShowPlayers(newArrPlayer, quantity);
}

int SearchByName(Player *&arrPlayer, int &quantity)
{
	string searchingname;	
	cin >> searchingname;

	for (int i = 0; i < quantity; i++)
	{
		if (searchingname == arrPlayer[i].name)
			return i;
	}
}

