#include <iostream>
#include <string>
#include <fstream>
using namespace std;

class Player
{
private:
	string name;
	int age;
	float points;
	bool isMale;

public:
	// Setters
	void SetName(const string newName)
	{
		if (newName.length <= 0)
			return;

			name = newName;	
	}
	void SetAge(const int newAge)
	{
		if (newAge < 0 || newAge > 300)
			return;
		age = newAge;	
	}
	void SetPoints(const float newPoints)
	{
		if (newPoints < 0)
			return;
		points = newPoints;
	}
	void SetGender(const bool newGender)
	{
		isMale = newGender;
	}

	// Geters
	string GetNAme()
	{
		return name;
	
	}





	void Show()
	{
		cout << "name: " << name << endl;
		cout << "age: " << age << endl;
		cout << "points: " << points << endl;
		cout << "gender: " << (isMale ? "Male" : "Female" ) << endl;
	
	}

	void Fill()
	{
		cout << "name: "; cin >> name;
		cout << "age: "; cin >> age;
		cout << "points: "; cin >> points;
		cout << "gender  ( 0 - Male 1 Female ): "; cin >> isMale;
	}
};




int main()
{
	Player p;
	Player p2;

	cout << "============" << endl;
	p.Show();
	cout << "============" << endl;

	p.GetNAme();
	



	system("pause");
	return 0;
}