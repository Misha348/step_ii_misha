#include "List.h"
#include <iostream>
using std::cout;
using std::endl;

void List::AddTail(int data)
{
	Element * newElement = new Element;
	newElement->num = data;
	newElement->next = nullptr;
	newElement->prev = nullptr;

	if (head == nullptr)
		head = tail = newElement; //why
	else
	{
		tail->next = newElement; //why
		newElement->prev = tail; //why
		tail = newElement; //why
	}
	++size;
}

void List::AddHead(int data)
{
	Element * newElement = new Element;
	newElement->num = data;
	newElement->prev = nullptr;
	newElement->next = nullptr;

	if (head == nullptr)
		head = tail = newElement;
	else
	{
		newElement->next = head;
		head->prev = newElement;
		head = newElement;
	}
	++size;
}

void List::Add(int pos, int data)
{
	if (pos > size + 1 || pos <= 0)
		return;

	if (pos == 1)
	{
		AddHead(data);
		return;
	}
	if (pos == size + 1)
	{
		AddTail(data);
		return;
	}

	Element * newElem = new Element;
	newElem->num = data;

	Element * temp = head;	

	for (int p = 1; p != pos; ++p)
		temp = temp->next;

	newElem->next = temp;
	newElem->prev = temp->prev;
	temp->prev->next = newElem;
	temp->prev = newElem;

	++size;
}

void List::DeleteHead()
{
	if (!IsEmpty())
	{
		Element * temp = head;
		head = head->next;
		head->prev = nullptr;
		delete temp;

		if (head == nullptr) 
			tail = nullptr;  // why??

		--size;
	}
}

void List::DeletePos(int pos)
{
	if (pos > size || pos <= 0)
		return;

	if (pos == 1)
	{
		DeleteHead();
		return;
	}
	if (pos == size)
	{
		DeleteTail();
		return;
	}

	Element * delEl = head;

	for (int p = 1; p != pos; ++p) // 
		delEl = delEl ->next;

	delEl->prev->next = delEl->next; //?
	delEl->next->prev = delEl->prev; //?

	delete delEl;
	--size;
}

void List::DeleteTail()
{
	Element * delTail = tail;
	tail = tail->prev;
	delete delTail;

	if (tail == nullptr) //?
		head = nullptr;
	else
		tail->next = nullptr;
	--size;
}



int List::Search(int val)
{
	Element * searchlEl = head;
	int i = 0;

	for (int p = 1; p != size; ++p)
	{
		if (searchlEl->num == val)
		{
			i++;
			return p;
		}		
		searchlEl = searchlEl->next;
	}
	return 0;	
}

//void List::SortList()
//{
//	for (Element * i = head; i != nullptr; i = i->next)
//	{
//		for (Element * j = i->next; j != nullptr; j = j->next)
//		{
//			if (j->num < i->num)
//			{
//				int tmp = i->num;
//				i->num = j->num;
//				j->num = tmp;
//			}
//			
//		}
//	}
//}

void List::SortList()
{
	Element * min;
	for (Element * i = head; i != nullptr; i = i->next)
	{
		min = i;
		for (Element * j = i->next; j != nullptr; j = j->next)
		{
			if (j->num < min->num)
			{
				min = j;
			}			
		}
		int tmp = i->num;
		i->num = min->num;
		min->num = tmp;
	}
}



void List::ShowList() const
{
	if (IsEmpty())
	{
		cout << "List is empty!\n";
		return;
	}

	/*Element * current = head;
	do
	{
		cout << "Element: " << current->num << endl;
		current = current->next;
	} while (current->next != nullptr);*/

	for (Element * item = head; item != nullptr; item = item->next)
	{
		cout << "Element: " << item->num << endl;
	}

	/*Element * current = head;
	while (current != nullptr)
	{
		cout << "Element: " << current->num << endl;
		current = current->next;
	}*/
}