#include "List.h"
#include <iostream>
using namespace std;



int main()
{
	List list;

	list.AddTail(4);
	list.AddTail(10);
	list.AddTail(30);
	list.ShowList();
	cout << "--------------------\n";

	list.Add(1, 3);
	list.ShowList();
	cout << "--------------------\n";

	list.Add(2, 9);
	list.ShowList();
	cout << "--------------------\n";

	/*list.Add(40, 29);
	list.ShowList();
	cout << "--------------------\n";*/
	list.DeletePos(2);
	list.ShowList();
	cout << "--------------------\n";
	list.Search(4);
	cout << "--------------------\n";
	list.DeleteTail();
	list.ShowList();
	cout << "--------------------\n";
	/*list.SortList();
	list.ShowList();*/
	cout << "--------------------\n";
	list.AddTail(111);
	list.ShowList();
	cout << "--------------------\n";
	list.AddHead(125);
	list.ShowList();
	cout << "--------------------\n";
	list.SortList();
	list.ShowList();


	system("pause");
	return 0;
}
