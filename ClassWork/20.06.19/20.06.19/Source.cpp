#include "BinTree.h"
#include <iostream>
using namespace std;

int main()
{
	Tree tree;
	tree.Add(4);
	tree.Add(5);
	tree.Add(1);
	tree.Add(2);
	tree.Add(3);
	//tree.

	cout << tree.Find(10) << endl;
	cout << "-------------------\n";
	
	tree.PrintSort();
	cout << "-------------------\n";

	if (tree.FindLine(3))
		cout << "Found\n";
	else
		cout << "Not found\n";
	cout << "-------------------\n";

	

	system("pause");
	return 0;
}