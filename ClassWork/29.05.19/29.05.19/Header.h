#pragma once
#include <iostream>;
using namespace std;


class String
{
private:
	char *str;
	int length;

public:
	static int countOfObj;
	void CreateStrKeyBoard();
	void ShowString() const;
	
	String();
	String(const String& string);
	
	~String();
};