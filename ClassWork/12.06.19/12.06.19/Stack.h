#pragma once
#include <iostream>
using namespace std;

class Stack
{
	
private:
	enum { EMPTY = -1, FULL = 10 };
	char str[FULL + 1];
	int topIndex;


public:
	Stack() { topIndex = EMPTY; }
	~Stack() = default;
	
	bool Push(char sb);
	char Pop();
	bool IsEmpty() const;
	bool IsFull() const;	
};

bool Stack::Push(char sb)
{
	if (!IsFull())
	{
		str[++topIndex] = sb;
		return true;
	}
	return false;
}

char Stack::Pop()
{
	if (!IsEmpty())
		return str[topIndex--];
	return 0;
}

bool Stack::IsEmpty() const
{
	return topIndex == EMPTY;
}

bool Stack::IsFull() const
{
	return topIndex == FULL;
}



//Stack::Stack()
//{
//	char *str = nullptr;
//	size = 0;
//}

//Stack::Stack(char* c_str)
//{
//	size = strlen(c_str);
//	str = new char[size + 1];
//	for (int i = 0; i < size; i++)
//	{
//		str[i] = c_str[i];
//	}
//	str[size] = '\0';
//}


