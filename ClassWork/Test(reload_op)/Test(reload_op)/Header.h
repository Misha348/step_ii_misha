#pragma once
class Time
{
private:
	int ss;
	int mm;
	int hh;


	friend void operator--(Time & time);
	friend Time operator++(Time & time, int i);
	friend bool operator==(const Time & left, const Time & right);
public:

	void Transfer(int);
	Time();
	Time(int, int, int);
	Time(int);

	void Show() const;
	void Increment();
	int GetSec();
	bool operator>(const Time & other) const;		//
	bool operator<(const Time & other) const;		//
	bool operator>=(const Time & other) const;
	bool operator<=(const Time & other) const;
	bool operator==(const Time & other) const;	//	
	bool operator!=(const Time & other) const;

	void operator ()(int, int, int);
	void operator ()(int);
	void operator ++();		//
	void operator --();		//
	Time & operator ++(int i);		//
	Time & operator --(int i);		//



};