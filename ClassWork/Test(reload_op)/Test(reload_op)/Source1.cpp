#include "Header.h"
#include<iostream>
using namespace std;

void Time::Transfer(int sec)
{
	int tmp = 0;
	if (sec < 3600)
	{
		hh = 0;
		if (sec < 60)
		{
			mm = 0;
			this->ss = sec;
		}
		else
		{
			this->ss = sec % 60;
			mm = (sec - (sec % 60)) / 60;
		}
	}
	else
	{
		tmp = sec % 3600;
		hh = (sec - tmp) / 3600;
		if (tmp < 60)
		{
			mm = 0;
			this->ss = tmp;
		}
		else
		{
			this->ss = tmp % 60;
			mm = (tmp - (tmp % 60)) / 60;
		}
	}
}

Time::Time()
{
	ss = 0;
	mm = 0;
	hh = 0;
}

Time::Time(int ss, int mm, int hh)
{
	this->ss = ss;
	this->mm = mm;
	this->hh = hh;
}

Time::Time(int sec)
{
	int tmp = 0;
	if (sec < 3600)
	{
		hh = 0;
		if (sec < 60)
		{
			mm = 0;
			this->ss = sec;
		}
		else
		{
			this->ss = sec % 60;
			mm = (sec - (sec % 60)) / 60;
		}
	}
	else
	{
		tmp = sec % 3600;
		hh = (sec - tmp) / 3600;
		if (tmp < 60)
		{
			mm = 0;
			this->ss = tmp;
		}
		else
		{
			this->ss = tmp % 60;
			mm = (tmp - (tmp % 60)) / 60;
		}
	}

}

void Time::Show() const
{
	cout << ss << "s :" << mm << "m :" << hh << "h" << endl;
}

void Time::Increment()
{
	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
	sec++;
	Transfer(sec);
}

int Time::GetSec()
{
	return (this->hh * 3600) + (this->mm * 60) + (this->ss);
}

//bool Time::operator>(const Time & other) const
//{
//	return (this->hh * 3600) + (this->mm * 60) + (this->ss)< (other.hh * 3600) + (other.mm * 60) + (other.ss);
//}
//
//bool Time::operator<(const Time & other) const
//{
//	return (this->hh * 3600) + (this->mm * 60) + (this->ss) > (other.hh * 3600) + (other.mm * 60) + (other.ss);
//}

bool Time::operator>=(const Time & other) const
{
	return (this->hh * 3600) + (this->mm * 60) + (this->ss) >= (other.hh * 3600) + (other.mm * 60) + (other.ss);
}

bool Time::operator<=(const Time & other) const
{
	return (this->hh * 3600) + (this->mm * 60) + (this->ss) <= (other.hh * 3600) + (other.mm * 60) + (other.ss);
}

//bool Time::operator==(const Time & other) const
//{
//	return (this->hh * 3600) + (this->mm * 60) + (this->ss) == (other.hh * 3600) + (other.mm * 60) + (other.ss);
//}

bool Time::operator!=(const Time & other) const
{
	return (this->hh * 3600) + (this->mm * 60) + (this->ss) != (other.hh * 3600) + (other.mm * 60) + (other.ss);
}

void Time::operator()(int hh, int mm, int ss)
{
	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
	int newSec = (hh * 3600) + (mm * 60) + (ss);
	Transfer(sec + newSec);
}
void Time::operator()(int newSec)
{
	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);

	Transfer(sec + newSec);
}

//void Time::operator++()
//{
//	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
//	sec++;
//	Transfer(sec);
//	
//}
//
//void Time::operator--()
//{
//	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
//	sec--;
//	Transfer(sec);
//		
//}
//
//Time &Time::operator++(int i)
//{
//	Time tmp;
//	tmp.hh = this->hh;
//	tmp.mm = this->mm;
//	tmp.ss = this->ss;
//
//	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
//	sec++;
//	Transfer(sec);
//
//	return tmp;
//}
//
//Time &Time::operator--(int i)
//{
//	Time tmp;
//	tmp.hh = this->hh;
//	tmp.mm = this->mm;
//	tmp.ss = this->ss;
//
//	int sec = (this->hh * 3600) + (this->mm * 60) + (this->ss);
//	sec--;
//	Transfer(sec);
//
//	return tmp;
//}