#include "Header.h"
#include<iostream>
using namespace std;

// friend func.
void operator--(Time & time)
{
	int sec = (time.hh * 3600) + (time.mm * 60) + (time.ss);
	sec--;
	time.Transfer(sec);
}

Time operator++(Time & time, int i)
{
	Time tmp;
	int sec = (time.hh * 3600) + (time.mm * 60) + (time.ss);
	tmp.Transfer(sec);
	sec++;
	time.Transfer(sec);
	return tmp;

}

bool operator==(const Time & left, const Time & right)
{
	return (left.hh * 3600) + (left.mm * 60) + (left.ss) == (right.hh * 3600) + (right.mm * 60) + (right.ss);
}



bool operator>(Time & left, Time & right)
{
	return left.GetSec() > right.GetSec();
}

bool operator<(Time & left, Time & right)
{
	return left.GetSec() < right.GetSec();
}

Time operator--(Time & time, int i)
{
	Time tmp;
	int sec = time.GetSec();
	tmp.Transfer(sec);
	sec--;
	time.Transfer(sec);
	return tmp;
}

int main()
{
	Time a(3721);

	a.Show();
	a(1, 23, 45);
	a.Show();
	a(20);
	b.Show();
	a--;

	Time b = a++;
	a.Show();
	b.Show();

	system("pause");
	return 0;

}