#include <iostream>
#include <string>
using namespace std;

class Wheel
{
private:
	int diam;
	string mark;
	int maxPresure;

public:
	Wheel()
	{
		diam = 0;
		mark = "non";
		maxPresure = 0;
	}
	Wheel(int dim, string mar, int maxP)
	{
		diam = dim;
		mark = mar;
		maxPresure = maxP;
	}

	void ShowWheel()
	{
		cout << "Diametr - " << diam << endl;
		cout << "Mark - " << mark << endl;
		cout << "Max presure - " << maxPresure << endl;
	}

};

class Engine
{
private:
	int power;
	int volume;
	string model;

public:
	Engine()
	{
		power = 0;
		volume = 0;
		model = "non";
	}

	Engine(int pow, int vol, string mod)
	{
		power = pow;
		volume = vol;
		model = mod;
	}

	void ShowEngine()
	{
		cout << "Power - " << power << endl;
		cout << "Volume - " << volume << endl;
		cout << "Model - " << model << endl;
	}
};

class Corps
{
private:
	int width;
	int high;
	string color;

public:
	Corps()
	{
		width = 0;
		high = 0;
		color = "non";
	}
	Corps(int wid, int hig, string col)
	{
		width = wid;
		high = hig;
		color = col;
	}
	void ShowCorp()
	{
		cout << "width - " << width << endl;
		cout << "high - " << high << endl;
		cout << "color - " << color << endl;
	}
};

class Driver
{
private:
	string name;
	string surname;
	int age;
	int expir;

public:
	Driver()
	{
		name = "non";
		surname = "non";
		age = 0;
		expir = 0;
	}

	Driver(string name, string surname, int age, int expir)
	{
		this->name = name;
		this->surname = surname;
		this->age = age;
		this->expir = expir;
	}

	void ShowDrInfo()
	{
		cout << "name - " << name << endl;
		cout << "surname - " << surname << endl;
		cout << "age - " << age << endl;
		cout << "expirience - " << expir << endl;
	}
};

class Car
{
private:
	Wheel *wheel;
	Engine eng;
	Corps corp;
	Driver *driver;

public:
	Car(int dim, int whAmount, string mar, int maxP, int pow, int vol, string mod, int wid, int hig, string col)
	{
		wheel = new Wheel[whAmount];
		for (int i = 0; i < whAmount; i++)
		{
			wheel[i] = Wheel(dim, mar, maxP);
		}
		eng = Engine(pow, vol, mod);
		corp = Corps(wid, hig, col);		
	}

	Car(int dim, int whAmount, string mar, int maxP, int pow, int vol, string mod, int wid, int hig, string col, string name, string surname, int age, int expir) 
	{
		wheel = new Wheel[whAmount];
		for (int i = 0; i < whAmount; i++)
		{
			wheel[i] = Wheel(dim, mar, maxP);
		}
		eng = Engine(pow, vol, mod);
		corp = Corps(wid, hig, col);
		driver = new Driver(name, surname, age, expir);
	}

	void ShowCar()
	{
		cout << " wheel: \n"; wheel->ShowWheel();
		cout << "\n engeen: \n"; eng.ShowEngine();
		cout << "\n corps: \n"; corp.ShowCorp();
		if (driver)
		{
			cout << "\n driver: \n"; driver->ShowDrInfo();
		}					
		else
			cout << "\n no driver in the car" << endl;
	}

	void AddDr( Driver * newDriver)
	{		
		driver = newDriver;
	}
	~Car()
	{
		delete[] wheel;		
	}
};

int main()
{
	//Car myCar(40, 4, "michellin", 50, 300, 3000, "extra", 180, 160, "red", "Vova", "Vovk", 38, 20);
	//myCar.ShowCar();
	//cout << "=====================\n";

	Car myCar(40, 4, "michellin", 50, 300, 3000, "extra", 180, 160, "red");	
	myCar.ShowCar();
	cout << "---------------\n";
	Driver driver("Vasya", "Borysyuk", 47, 25);
	driver.ShowDrInfo();
	cout << "---------------\n";

	myCar.AddDr(&driver);
	myCar.ShowCar();
	
	system("pause");
	return 0;
}
//Corps newCorp(200, 200, "black");