#pragma once
#include <iostream>
#include <string>
using namespace std;

class Student
{
private:
	string name;
	string surname;
	int birthYear;
	int age;
	string department;
	int studingYear;

	void IdentAge();

public:
	void Show() const;	

	Student();
	Student(string newName, string newSurname, int yearOfBirth, string newDepartment, int yearOfStuding);
	
	
};