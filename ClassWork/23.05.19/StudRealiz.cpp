#include <iostream>
#include <string>
#include "ClassStud.h"
using namespace std;

void Student::Show() const
{
	cout << "name: " << name << endl;
	cout << "surname: " << surname << endl;
	cout << "birth year: " << birthYear << endl;
	cout << "age: " << age << endl;
	cout << "department: " << department << endl;
	cout << "studing year: " << studingYear << endl;
}

void Student::IdentAge() 
{
	age = 2019 - birthYear;
}

Student::Student()
{
	name = "non";
	surname = "non";
	birthYear = 0;
	age = 0;
	department = "non";
	studingYear = 0;
}

Student::Student(string newName, string newSurname, int yearOfBirth, string newDepartment, int yearOfStuding)
{
	name = newName;
	surname = newSurname;
	birthYear = yearOfBirth;
	department = newDepartment;
	studingYear = yearOfStuding;
	IdentAge();
}