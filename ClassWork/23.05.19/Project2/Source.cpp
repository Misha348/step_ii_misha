#include <iostream>
#include <string>
#include <ctime>
#include "ClassArr.h"

using namespace std;

int main()
{
	srand(unsigned(time(NULL)));
	cout << "[ 1 ] creating of array:\n" << endl;

	int size = 0;
	cout << "enter size: "; cin >> size;
	int val = 0;
	cout << "enter value: "; cin >> val;	
	cout << "======================" << endl;

	cout << "\tI CONSTRUCTOR" << endl;	
	Array arr = Array(val, size);	
	cout << "======================" << endl;

	cout << "\tII CONSTRUCTOR" << endl;
	int *brr2 = new int[size];
	for (int i = 0; i < size; i++)
	{
		brr2[i] = rand() % 10;
	}
	Array arr1(brr2, size);
	arr1.ShowArr(brr2, size);
	cout << "======================" << endl;

	int newEl = 0;
	cout << "enter new element: " << endl;
	cin >> newEl;
	arr1.AddElToArr(brr2, size, newEl);
	arr1.ShowArr(brr2, size);
	cout << "======================" << endl;
	
	int posTodel = 0;
	cout << "enter pos. to del. : " << endl;
	cin >> posTodel;
	arr1.RemovElFromArr(brr2, size, posTodel);
	arr1.ShowArr(brr2, size);

	system("pause");
	return 0;
}