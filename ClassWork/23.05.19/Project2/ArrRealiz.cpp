#include <iostream>
#include <string>
#include "ClassArr.h"
using namespace std;

void Array::AddElToArr(int *&arr, int &size, int newEl)
{
	int *newArr = new int[size + 1];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}
	newArr[size] = newEl;
	size++;

	delete[] arr;
	arr = newArr;
}

void Array::RemovElFromArr(int *&arr, int &size, int posToDel)
{
	int *newArr = new int[size];
	for (int i = 0; i < size; i++)
	{
		newArr[i] = arr[i];
	}
	size--;
	for (int i = (posToDel - 1); i < size; i++)// (posToDel - 1) cause of improving wiev of elements (but it should be - posToDel);
	{
		newArr[i] = newArr[i + 1];
	}
	delete[] arr;
	arr = newArr;
}

void Array::ShowArr(int *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << "el." << i + 1 << " = " << arr[i] << endl;
	}
}

Array::Array()
{
	arr = nullptr;
	size = 0;
}


Array::Array(int value, int newSize)
{
	size = newSize;
	arr = new int[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = value;
	}
	ShowArr(arr, size);
}


Array::Array(int *someArr, int newSize)
{
	size = newSize;
	arr = new int[size];
	for (int i = 0; i < size; i++)
	{
		arr[i] = someArr[i];
	}
}

Array::~Array()
{
	delete[] arr;
}




