#pragma once
#include <iostream>
#include <string>
using namespace std;

class Array
{
private:
	int *arr;
	int size;

public:
	void AddElToArr(int *&arr, int &size, int newEl);
	void RemovElFromArr(int *&arr, int &size, int posToDel);
	void ShowArr(int *arr, int size);

	Array();
	Array(int value, int size);
	Array(int *arr, int size);
	
	~Array();

};
