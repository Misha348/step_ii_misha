#pragma once
#include <iostream>
#include <string>
using namespace std;

class Figure
{
protected:
	int length;

public:
	Figure() : length(0) { }
	Figure(int length) : length(length) { }

	virtual float Square() = 0;
	virtual void ShowSquare() = 0;

};