#include <iostream>
#include <string>
#include "Square.h"
using namespace std;

class Rectangle: public Figure
{
private:
	int width;

public:
	Rectangle(int length, int width) : Figure(length), width(width) { }
	
	virtual float Square()
	{
		int sqr = 0;
		sqr = length * width;
		return sqr;
	}

	virtual void ShowSquare()
	{
		cout << "Rectangle square = " << Square() << endl;
	}
};

class Circle : public Figure
{
private:
	
	float pi;

public:
	Circle(int length, int pi) : Figure(length), pi(pi){ }

	virtual float Square()
	{
		int rad = 0;
		int diam = 0;
		diam = length / pi;
		rad = diam / 2;
		int sqr = 2 * pi * rad;
		return sqr;		
	}

	virtual void ShowSquare()
	{
		cout << "Circle square = " << Square() << endl;
	}
};

int main()
{
	const int size = 2;
	cout << "1 - rectangle\n2 - Circle\n" << endl;

	Figure *arrFig[size];
	for (int i = 0; i < size; i++)
	{
		cout << "  Figure [ " << i + 1 << " ] " << endl;
		int type;
		cin >> type;
		switch (type)
		{
		case 1:
			arrFig[i] = new Rectangle(5, 7);
			break;
		case 2:
			arrFig[i] = new Circle(70, 3.14);
		}
	}

	for (int i = 0; i < size; i++)
	{
		arrFig[i]->ShowSquare();		
	}


	system("pause");
	return 0;
}
