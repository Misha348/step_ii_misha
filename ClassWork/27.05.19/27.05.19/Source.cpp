#include <iostream>
#include <string>
using namespace std;

class Stud
{
private:
	string name;
	string surname;
	int age;
	double mark;

public:

	void SetName(string newName)
	{
		name = newName;
	}

	void SetSurname(string newSurname)
	{
		surname = newSurname;	
	}

	void SetAge(int newAge)
	{
		age = newAge;
	}

	void SetMark(double newMark)
	{
		mark = newMark;
	}

	string GetName() const
	{
		return name;
	}

	string GetSurname() const
	{
		return surname;
	}

	int GetAge() const
	{
		return age;	
	}

	double GetMark() const
	{
		return mark;
	}
	
	void FillStud()
	{
		cout << "name: "; cin >> name;
		cout << "surname: "; cin >> surname;
		cout << "age: "; cin >> age;
		cout << "mark: "; cin >> mark;
	}

	void ShowStud() const
	{
		cout << "name: " << name << endl;
		cout << "surname: " << surname << endl;
		cout << "age: " << age << endl;
		cout << "mark: " << mark << endl;
	}

	Stud()
	{	
		name = "non";
		surname = "non";
		age = 0;
		mark = 0;
	}

	Stud(string newName, string newSurname, int newAge, double newMark)
	{
		name = newName;
		surname = newSurname;
		age = newAge;
		mark = newMark;
	}
	~Stud() {}
	
};

class Group
{
private:
	Stud *arr;
	int size;
	string specialization;
	double GroupAverPoint;

	double CalculateAverGrMark()
	{
		double sum = 0;
		for (int i = 0; i < size; i++)
		{
			sum += arr[i].GetMark();
		}
		GroupAverPoint = sum / size;
		return GroupAverPoint;
	}

public:

	void SetGroupSpec(string specializ)
	{
		specialization = specializ;	
	}		

	string GetSpec() const
	{
		return specialization;
	}

	Group(Stud *studArr, int newSize)
	{
		size = newSize;
		arr = new Stud[size];
		for (int i = 0; i < size; i++)
		{
			arr[i] = studArr[i];
		}			
	}
	~Group(){}

	void Show()
	{
		cout << "SPEC: "<< GetSpec() << endl;
		for (int i = 0; i < size; i++)
		{
			arr[i].ShowStud();			
		}
		cout << "Gr Av Point: "<< CalculateAverGrMark() << endl;
	}

	const Group& GedBetterGroup(const Group& group) const
	{
		if (group.GroupAverPoint > this->GroupAverPoint)
		{
			return group;
		}
		else
		{
			return *this;
		}
	}
};

void GroupCreation()
{

}

int main()
{	
	int quantity = 0;		
	cout << "Enter quantity of students for 1 group: "; cin >> quantity;
	Stud *student = new Stud[quantity];

	for (int i = 0; i < quantity; i++)
	{
		student[i].FillStud();
		cout << endl;
	}
	system("cls");

	Group group1(student, quantity);
	group1.SetGroupSpec("math");
	
	
	int quantity1 = 0;
	cout << "Enter quantity of students for 2 group: "; cin >> quantity1;
	Stud *student1 = new Stud[quantity1];

	for (int i = 0; i < quantity1; i++)
	{
		student1[i].FillStud();
		cout << endl;
	}
	system("cls");

	Group group2(student1, quantity1);
	group2.SetGroupSpec("phisic");
	group1.Show();
	cout << "===========================" << endl;
	group2.Show();

	Group betterGroup = group1.GedBetterGroup(group2);
	cout << "==========Better============" << endl;
	betterGroup.Show();
	
	system("pause");
	return 0;
}