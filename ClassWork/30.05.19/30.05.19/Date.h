#pragma once

class Date
{
	int day, month, year;

public:

	Date();
	Date(int d, int m, int y);
	void Show();
	Date operator-(const Date& obj);

	void GetDayDiff(const Date& obj);


};