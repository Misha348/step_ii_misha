#include <iostream>
#include "Date.h"
using namespace std;

Date::Date()
{
	day = 8;
	month = 7;
	year = 2017;
}

Date::Date(int d, int m, int y)
{
	this->day = d;
	this->month = m;
	this->year = y;
}

void Date::Show()
{
	if (day < 0)
	{
		day = day * (-1);
	}
	if (month < 0)
	{
		month = month * (-1);
	}
	if (year < 0)
	{
		year = year * (-1);
	}

	cout << "day dif: " << day << endl;
	cout << "month dif: " << month << endl;
	cout << "year dif: " << year << endl;
}

Date Date::operator-(const Date& obj)
{	
	return Date(this->day - obj.day, this->month - obj.month, this->year - obj.year);
}

void Date::GetDayDiff(const Date & obj)
{
	int coutExtra = 0;
	coutExtra = this->year / 4;
	int diff = 0;
	diff = this->day + (this->month * 30) + (this->year * 365) + coutExtra;
	cout << "\n" << "days diff: " << diff << endl;
}