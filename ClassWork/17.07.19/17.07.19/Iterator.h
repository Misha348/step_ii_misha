#pragma once
#include <iostream>


template <class T>
class base_iterator
{
protected:
	T* data;
public:
	base_iterator() {}
	base_iterator(T* data) : data(data) {}
	virtual base_iterator& operator ++ () = 0;
	virtual base_iterator& operator ++ (int) = 0;
	virtual base_iterator& operator --() = 0;
	virtual T operator * () const
	{
		return *data;
	}
};

#include "IteratorRealiz.cpp"