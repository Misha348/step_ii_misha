#include <iostream>
#include <string>
#include <cctype>
using namespace std;

class StringConverter
{
protected:
	char *str;
	int size;

public:
	StringConverter()
	{
		str = nullptr;
		size = 0;
	}

	StringConverter(const char* str)
	{
		size = strlen(str);
		this->str = new char[size + 1];
		for (int i = 0; i < size; i++)
		{
			this->str[i] = str[i];
		}
		this->str[size] = '\0';
	}

	void Show()
	{
		cout << " your string: " << str << endl;
	}

	virtual void Convert() = 0;
	virtual void ShowConvertString() = 0;
	
	~StringConverter()
	{
		delete[] str;
	}
};

class ASCII_CodeConverter: public StringConverter
{
private:
	int size;
	int *convertRes;
public:
	ASCII_CodeConverter(const char *str) : StringConverter(str) { }

	void Convert()
	{			
		delete[]convertRes;
		size = strlen(str);
		convertRes = new int[size];
		for (int i = 0; i < size; i++)
		{
			this->convertRes[i] = (int)str[i];
		}		
	}		

	void ShowConvertString()
	{
		for (int i = 0; i < size; i++)
		{
			cout << convertRes[i] << " ";
		}
	}

	~ASCII_CodeConverter()
	{
		delete[]convertRes;
	}
};

class ReflectCase_Converter: public StringConverter
{
private:
	char *convRes;

public:
	ReflectCase_Converter(const char *str): StringConverter(str) {	}

	void Convert()
	{
		if (convRes != nullptr)
		delete[]convRes;

		size = strlen(str);
		convRes = new char[size];
		for (int i = 0; i < size; i++)
		{
			if (!isalpha(this->str[i]))
			{
				this->convRes[i] = str[i];
			}

			if (islower(this->str[i]))
				this->convRes[i] = toupper(this->str[i]);
			else
				this->convRes[i] = tolower(this->str[i]);
			
			
		}	
	}

	void ShowConvertString()
	{
		for (int i = 0; i < size; i++)
		{		
			cout << convRes[i] << " ";
		}
	}

	~ReflectCase_Converter()
	{
		if (convRes != nullptr);
		delete[]convRes;
	}

};



int main()
{
	ASCII_CodeConverter myString("ghDFpojkDEEEE");
	myString.Convert();
	myString.ShowConvertString();
	cout << "\n==================\n" << endl;

	ReflectCase_Converter myString1("77aEWQuNV");
	myString1.Convert();
	myString1.ShowConvertString();


	system("pause");
	return 0;
}