#pragma once
#include <iostream>
using namespace std;

namespace DinamicDataStructures
{
	namespace Queues
	{
		namespace RegularQueue
		{
			class Queue
			{
			private:
				int * data;
				int maxSize;
				int size;

			public:
				Queue(int maxSize);
				~Queue();

				void Add(int elem);
				void Extract();
				bool IsEmpty() const;
				bool IsFull() const;				
				void Show() const;
			};
		}

		namespace CircleQueue
		{
			class Queue
			{
			private:
				int * data;
				int maxSize;
				int size;

			public:
				Queue(int maxSize);
				~Queue();

				void Add(int elem);
				int Extract();
				bool IsEmpty() const;
				bool IsFull() const;
				void Show() const;
			};
		}

		namespace CircleQueue
		{
			class QueueWithPriority
			{
			private:
				int * data;				
				int * priorities;				
				int maxSize;				
				int size;

			public:
				QueueWithPriority(int maxSize);				
				~QueueWithPriority();				
				void AddWithPriority(int elem, int priority);				
				int ExtractElemWithHighPriority();									
				bool IsEmpty() const;				
				bool IsFull() const;							
				void Show() const;
			};

		}
	}
}