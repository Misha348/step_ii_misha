#include <iostream>
#include "Header.h"
using namespace std;

DinamicDataStructures::Lists::SingleLinckedList::List::List()
{
	head = nullptr;
	tail = nullptr;
	size = 0;
}

DinamicDataStructures::Lists::SingleLinckedList::List::~List()
{
	DelAll();
}

void DinamicDataStructures::Lists::SingleLinckedList::List::AddToBegining(int num)
{
	Element * newElm = new Element;
	newElm->numb = num;
	newElm->next = nullptr;

	if (head == nullptr)
		head = tail = newElm;
	else
	{
		newElm->next = head;
		head = newElm;
	}
	size++;
}

void DinamicDataStructures::Lists::SingleLinckedList::List::AddToEnd(int num)
{
	Element * newElm = new Element;
	newElm->numb = num;
	newElm->next = nullptr;

	if (head == nullptr)
		head = tail = newElm;
	else
	{
		tail->next = newElm;
		tail = newElm;
	}
	size++;
}

void DinamicDataStructures::Lists::SingleLinckedList::List::DelAll()
{
	while (head != nullptr)
		DeleteHead();
}

void DinamicDataStructures::Lists::SingleLinckedList::List::DeleteHead()
{
	if (!IsEmpty())
	{
		Element *tmp = head;
		head = head->next;
		delete tmp;
	}
	size--;
}

bool DinamicDataStructures::Lists::SingleLinckedList::List::IsEmpty() const
{
	return head == nullptr;
}

void DinamicDataStructures::Lists::SingleLinckedList::List::Show() const
{
	if (IsEmpty())
	{
		cout << "List is empty!\n";
		return;
	}
	Element *current = head;
	while (current != nullptr)
	{
		cout << "Element: " << current->numb << endl;
		current = current->next;
	}
}

//===========================================================================

void DinamicDataStructures::Lists::DoubleLinckedList::List::AddTail(int data)
{
	Element * newElement = new Element;
	newElement->num = data;
	newElement->next = nullptr;
	newElement->prev = nullptr;

	if (head == nullptr)
		head = tail = newElement; 
	else
	{
		tail->next = newElement; 
		newElement->prev = tail; 
		tail = newElement; 
	}
	++size;
}

void  DinamicDataStructures::Lists::DoubleLinckedList::List::AddHead(int data)
{
	Element * newElement = new Element;
	newElement->num = data;
	newElement->prev = nullptr;
	newElement->next = nullptr;

	if (head == nullptr)
		head = tail = newElement;
	else
	{
		newElement->next = head;
		head->prev = newElement;
		head = newElement;
	}
	++size;
}

void DinamicDataStructures::Lists::DoubleLinckedList::List::DeleteHead()
{
	if (!IsEmpty())
	{
		Element * temp = head;
		head = head->next;
		head->prev = nullptr;
		delete temp;

		if (head == nullptr)
			tail = nullptr;  

		--size;
	}
}

void DinamicDataStructures::Lists::DoubleLinckedList::List::DeleteTail()
{
	Element * delTail = tail;
	tail = tail->prev;
	delete delTail;

	if (tail == nullptr)
		head = nullptr;
	else
		tail->next = nullptr;
	--size;
}


void  DinamicDataStructures::Lists::DoubleLinckedList::List::DeletePos(int pos)
{
	if (pos > size || pos <= 0)
		return;

	if (pos == 1)
	{
		DeleteHead();
		return;
	}
	if (pos == size)
	{
		DeleteTail();
		return;
	}

	Element * delEl = head;

	for (int p = 1; p != pos; ++p) 
		delEl = delEl->next;

	delEl->prev->next = delEl->next; 
	delEl->next->prev = delEl->prev; 

	delete delEl;
	--size;
}

void DinamicDataStructures::Lists::DoubleLinckedList::List::ShowList() const
{
	if (IsEmpty())
	{
		cout << "List is empty!\n";
		return;
	}	

	for (Element * item = head; item != nullptr; item = item->next)
	{
		cout << "Element: " << item->num << endl;
	}	
}


