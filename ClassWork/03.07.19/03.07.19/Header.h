#pragma once
#include <iostream>
using namespace std;

namespace DinamicDataStructures
{
	namespace Lists
	{
		namespace SingleLinckedList
		{
			struct  Element
			{
				int numb;
				Element *next;

			};

			class List
			{
			private:
				Element *head;
				Element *tail;
				int size;

			public:
				List();
				~List();

				void AddToBegining(int num);
				void AddToEnd(int num);
				void DeleteHead();
				void DelAll();										

				bool IsEmpty() const;
				void Show() const;
			};
		}

		namespace DoubleLinckedList
		{
			class List
			{
			private:				
				struct Element
				{
					int num;		
					Element * next;	
					Element * prev;	
				};

				Element * head;		
				Element * tail;		
				int size;

			public:				
				List()
				{					
					head = tail = nullptr;
					size = 0;
				}

				bool IsEmpty() const { return size == 0; }

				void AddTail(int data);				
				void AddHead(int data);				
							
				void DeleteHead();				
				void DeletePos(int pos);				
				void DeleteTail();									
				void ShowList() const;
				
			};
		}
	}
}