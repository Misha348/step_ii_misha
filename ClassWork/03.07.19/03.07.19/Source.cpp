#include <iostream>
#include "Header.h"
#include "Queues.h"
#include <Windows.h>
#include <ctime>
using namespace std;
HANDLE hConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);


int main()
{	
	srand(time(NULL));
	//SLL
	{
		SetConsoleTextAttribute(hConsoleHandle, 10);
		cout << "\t\t\tSINGLE LINCKED LIST" << endl;
		SetConsoleTextAttribute(hConsoleHandle, 7);

		DinamicDataStructures::Lists::SingleLinckedList::List list;

		cout << "  --ADD to BEGINING--\n";
		list.AddToBegining(100);
		list.Show();
		cout << "\n====================\n";

		cout << "  --ADD to END--\n";
		list.AddToEnd(3);
		list.AddToEnd(4);
		list.AddToEnd(555);
		list.AddToEnd(6);
		list.AddToEnd(7);
		list.Show();
		cout << "\n====================\n";

		cout << "  --DELETE HEAD--\n";
		list.DeleteHead();
		list.Show();
		cout << "\n====================\n";

		cout << "  --DELETE ALL--\n";
		list.DelAll();
		list.Show();
		cout << "\n====================\n";
	}
	
	//DLL
	{
		SetConsoleTextAttribute(hConsoleHandle, 10);
		cout << "\t\t\tDOUBLE LINCKED LIST" << endl;
		SetConsoleTextAttribute(hConsoleHandle, 7);

		DinamicDataStructures::Lists::DoubleLinckedList::List list;

		cout << "  --ADD to BEGINING--\n";
		list.AddHead(100);
		list.ShowList();
		cout << "\n====================\n";

		cout << "  --ADD to END--\n";
		list.AddTail(4);
		list.AddTail(10);
		list.AddTail(7);
		list.AddTail(30);
		list.AddTail(15);
		list.ShowList();
		cout << "\n====================\n";

		cout << "  --DELETE POSSITION--\n";
		list.DeletePos(2);
		list.ShowList();
		cout << "\n====================\n";

		cout << "  --DELETE TAIL--\n";
		list.DeleteTail();
		list.ShowList();
		cout << "\n====================\n";

		cout << "  --DELETE HEAD--\n";
		list.DeleteHead();
		list.ShowList();
		cout << "\n====================\n";
	}
	
	//REGQUEUE
	{
		SetConsoleTextAttribute(hConsoleHandle, 10);
		cout << "\t\t\tREGULAR QUEUE" << endl;
		SetConsoleTextAttribute(hConsoleHandle, 7);

		DinamicDataStructures::Queues::RegularQueue::Queue q(8);

		cout << "  --ADD ELEMENTS--\n";
		for (int i = 0; i < 8; i++)
			q.Add(rand() % 30);

		q.Show();
		cout << "\n====================\n" << endl;

		cout << "  --EXTRACT ELEMENTS--\n";
		q.Extract();
		q.Show();
		cout << "\n====================\n" << endl;
	}

	//CIRCLEQUEUE
	{
		SetConsoleTextAttribute(hConsoleHandle, 10);
		cout << "\t\t\tCIRCLE QUEUE" << endl;
		SetConsoleTextAttribute(hConsoleHandle, 7);

		DinamicDataStructures::Queues::RegularQueue::Queue q(10);

		cout << "  --ADD ELEMENTS--\n";
		for (int i = 0; i < 10; i++)
			q.Add(rand() % 30);

		q.Show();
		cout << "\n====================\n" << endl;

		cout << "  --EXTRACT ELEMENTS--\n";
		q.Extract();
		q.Show();
		cout << "\n====================\n" << endl;
	}
	
	//PriorityQueue
	{
		SetConsoleTextAttribute(hConsoleHandle, 10);
		cout << "\t\t\tQUEUE WITH PRIOTITY" << endl;
		SetConsoleTextAttribute(hConsoleHandle, 7);

		cout << "  --ADD ELEMENTS--\n\n";
		DinamicDataStructures::Queues::CircleQueue::QueueWithPriority q1(10);
		while (!q1.IsFull())
			q1.AddWithPriority(rand() % 100, rand() % 10);
		q1.Show();
		cout << "\n====================\n" << endl;

		cout << "  --EXTRACT ELEMENT with HIGH PRIORITY--\n\n";
		cout << "Extrack element: " << q1.ExtractElemWithHighPriority() << endl;
		q1.Show();
	}

	system("pause");
	return 0;
}

