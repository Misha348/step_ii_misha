#include <iostream>
#include "Header.h"
#include "Queues.h"
using namespace std;

DinamicDataStructures::Queues::RegularQueue::Queue::Queue(int maxSize)
{	
	this->maxSize = maxSize;	
	data = new int[maxSize];	
	size = 0;
}

DinamicDataStructures::Queues::RegularQueue::Queue::~Queue()
{	
	delete[]data;
}


void DinamicDataStructures::Queues::RegularQueue::Queue::Add(int elem)
{	
	if (!IsFull())
		data[size++] = elem;
}

void DinamicDataStructures::Queues::RegularQueue::Queue::Extract()
{
	if (!IsEmpty())
	{
		int first = data[0];
		for (int i = 0; i < size - 1; i++)
		{
			data[i] = data[i + 1];
		}	
		--size;		
	}	
}

bool DinamicDataStructures::Queues::RegularQueue::Queue::IsEmpty() const
{	
	return size == 0;
}

bool DinamicDataStructures::Queues::RegularQueue::Queue::IsFull() const
{	
	return size == maxSize;
}

void DinamicDataStructures::Queues::RegularQueue::Queue::Show() const
{	
	for (int i = 0; i < maxSize; i++)
		cout << data[i] << " ";	
}

//========================================================================

DinamicDataStructures::Queues::CircleQueue::Queue::Queue(int maxSize)
{
	this->maxSize = maxSize;
	data = new int[maxSize];
	size = 0;
}

DinamicDataStructures::Queues::CircleQueue::Queue::~Queue()
{
	delete[]data;
}


void DinamicDataStructures::Queues::CircleQueue::Queue::Add(int elem)
{
	if (!IsFull())
		data[size++] = elem;
}

int DinamicDataStructures::Queues::CircleQueue::Queue::Extract()
{
	if (!IsEmpty())
	{
		int first = data[0];
		for (int i = 0; i < size - 1; i++)
		{
			data[i] = data[i + 1];
		}		
		data[size - 1] = first;
		return first;
	}

}

bool DinamicDataStructures::Queues::CircleQueue::Queue::IsEmpty() const
{
	return size == 0;
}

bool DinamicDataStructures::Queues::CircleQueue::Queue::IsFull() const
{
	return size == maxSize;
}

void DinamicDataStructures::Queues::CircleQueue::Queue::Show() const
{

	for (int i = 0; i < maxSize; i++)
		cout << data[i] << " ";
}

//========================================================================

DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::QueueWithPriority(int maxSize)
{
	
	this->maxSize = maxSize;	
	data = new int[maxSize];
	priorities = new int[maxSize];	
	size = 0;
}

DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::~QueueWithPriority()
{	
	delete[]data;
	delete[]priorities;
}

void DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::AddWithPriority(int elem, int priority)
{	
	if (!IsFull())
	{
		data[size] = elem;
		priorities[size] = priority;
		++size;
	}
}

int DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::ExtractElemWithHighPriority()
{
	
	if (!IsEmpty())
	{			
		int prIndex = 0;
		int prValue = priorities[0];
		
		for (int i = 1; i < size; ++i)
		{
			if (priorities[i] > prValue)
			{
				prValue = priorities[i];
				prIndex = i;
			}
		}

		int element = data[prIndex];
		
		for (int i = prIndex; i < size - 1; i++)
		{
			data[i] = data[i + 1];
			priorities[i] = priorities[i + 1];
		}		
		--size;			

		return element;
	}
}

bool DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::IsEmpty() const
{	
	return size == 0;
}

bool DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::IsFull() const
{	
	return size == maxSize;
}

void  DinamicDataStructures::Queues::CircleQueue::QueueWithPriority::Show() const
{	
	for (int i = 0; i < size; i++)
		cout << data[i] << "\tPriority: " << priorities[i] << endl;	
}