#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
using namespace std;

struct Time
{
	int hours;
	int minutes;
	Time () {}
	Time(int hou, int min) : hours(hou), minutes(min) {}
};

class Train
{
public:
	int trNumb;
	Time timeDepart;
	string destinat;
	//friend ostream& operator << (ostream& out, Train& tr);

public:
	Train() : trNumb(0), timeDepart(0, 0), destinat("non") { }
	Train(int trNumb, int hou, int min, string destin) : trNumb(trNumb), timeDepart(hou, min), destinat(destin) { }

	void Showtrain()
	{
		cout << "train no.   - " << trNumb << endl;
		cout << "time of depart - " << timeDepart.hours << " : " << timeDepart.minutes << endl;
		cout << "destination  - " << destinat << endl;
	}

	void SetTrTime(int hou, int min)
	{
		timeDepart.hours = hou;
		timeDepart.minutes = min;
	}

	bool operator > (const Train &other) const
	{
		return (timeDepart.hours * 60 + timeDepart.minutes) > (timeDepart.hours * 60 + timeDepart.minutes);
	}	
};

class Depo
{
private:
	
	vector<Train> trains;

public:
	Depo() { }
	Depo(int trNumb, Time time, string destin, string station) { }
	

	void Addtrain(int trNum, int hou, int min, string dest)
	{
		Train newTrain(trNum, hou, min, dest);
		trains.push_back(newTrain);
	}

	void ShowAllTrainsInfo()
	{
		for (Train item : trains)
		{
			cout << "train no.   - " << item.trNumb << endl;
			cout << "time of depart - " << item.timeDepart.hours << " : " << item.timeDepart.minutes << endl;
			cout << "destination  - " <<  item.destinat << endl;
			cout << endl;
		}
	}

	void ShowTrainByNumb(int searNum)
	{
		bool hadFound;
		for (Train item : trains)
		{
			if (item.trNumb == searNum)
			{
				item.Showtrain();
				hadFound = true;
				break;
			}
			else
				hadFound = false;
		}	
		if(hadFound == false)
			cout << "no such train" << endl;
	}

	void ShowTrainByDest(string dest)
	{
		bool hadFound;
		for (Train item : trains)
		{
			if (item.destinat == dest)
			{
				item.Showtrain();
				hadFound = true;
				break;
			}
			else
				hadFound = false;
		}
		if (hadFound == false)
			cout << "no such train" << endl;
	}	

	void Sort()
	{			
		for (int i = 0; i < trains.size(); i++)	
		{
			int k = i;
			Train x = trains[i];
			
			for (int j = i + 1; j < trains.size(); j++)
			{
				if (trains[j].timeDepart.hours < x.timeDepart.hours)
				{
					k = j;		
					x = trains[j];	
				}
			}
			
			trains[k] = trains[i];
			trains[i] = x;
		}

		//comparator
		/*sort(trains.begin(), trains.end(), [](const Train& a, const Train &b)
		{
			return a.timeDepart.hours < b.timeDepart.hours;
		});*/	
	}

	void RedactTime(int trNumb, int hou, int min)
	{
		for (Train &item : trains)
		{
			if (item.trNumb == trNumb)
			{
				item.SetTrTime(hou, min);
				
				//item.timeDepart.hours = hou;
				//item.timeDepart.minutes = min;				
			}
		}	

	/*	for (int i = 0; i < trains.size(); i++)
		{
			if (trains[i].trNumb == trNumb)
			{
				trains[i].timeDepart.hours = hou;
				trains[i].timeDepart.minutes = min;

			}
		}*/
	}

	void RecortToFile()
	{
		ofstream wrFile("Tr.txt", ios_base::out);

		for (int i = 0; i < trains.size(); i++)
		{
			wrFile << "\n" << trains[i].trNumb << " "
				   << trains[i].timeDepart.hours << " "
				   << trains[i].timeDepart.minutes << " "
				   << trains[i].destinat;
		}
		wrFile.close();		
	}

	void ReadFromFile()
	{
		ifstream rdFile("Tr.txt", ios_base::in);
		
		while (!rdFile.eof())
		{
			Train recTrain;

			rdFile >> recTrain.trNumb;
			rdFile >> recTrain.timeDepart.hours;
			rdFile >> recTrain.timeDepart.minutes;
			rdFile >> recTrain.destinat;			

			trains.push_back(recTrain);
		}
		rdFile.close();		
	}
};

//ostream& operator << (ostream &out, Train &tr)
//{
//	out << "train no.   - " << tr.trNumb << "\n"
//		<< "time of depart - " << tr.timeDepart.hours << " : " << tr.timeDepart.minutes << "\n"
//		<< "destination  - " << tr.destinat << "\n";
//	cout << endl;
//}