#include <iostream>
#include <string>
#include <vector>
#include "Train.h"
using namespace std;

int main()
{
	Depo depo;

	depo.Addtrain(1111, 12, 34, "dest_1");
	depo.Addtrain(2222, 10, 25, "dest_2");
	depo.Addtrain(3333, 11, 30, "dest_3");
	depo.Addtrain(4444, 18, 35, "dest_4");
	depo.Addtrain(5555, 3, 40, "dest_5");
	depo.Addtrain(6666, 8, 45, "dest_6");
	
	depo.ShowAllTrainsInfo();
	depo.ShowTrainByNumb(1212);
	depo.ShowTrainByDest("dest_5");
	cout << "=============================" << endl;

	depo.Sort();
	depo.ShowAllTrainsInfo();
	cout << "====================" << endl;
	depo.RedactTime(1111, 5, 15);
	depo.ShowTrainByNumb(1111);

	depo.RecortToFile();
	cout << "=============================" << endl;
	depo.ReadFromFile();
	cout << "+++++++++++++++++++++++++++++" << endl;
	depo.ShowAllTrainsInfo();
	
	
	
	system("pause");
	return 0;
}