#include <iostream>
#include <string>
using namespace std;

class Corps
{
private:
	string size;
	string color;

public:
	Corps(string size, string color): size(size), color(color) { }

	void HoldInnerDet()
	{
		cout << "corps hold different inner details" << endl;
	}
	void Show()
	{
		cout << "size - " << size << endl;
		cout << "color - " << color << endl;
	}

};

class Engeen
{
private:
	int volume;
	int power;

public:

	Engeen(int volume, int power) : volume(volume), power(power) { } 

	void StartWork()
	{
		cout << "engeen starts work proceses in car" << endl;
	}

	void Show()
	{
		cout << "volume - " << volume << endl;
		cout << "power - " << power << endl;
	}
};

class Wheel
{
private:
	int diametr;
	int presure;

public:

	Wheel(int diametr, int presure): diametr(diametr), presure(presure) { }

	void MoveCar()
	{
		cout << "car moves with help of wheels " << endl;
	}

	void Show()
	{
		cout << "diametr - " << diametr << endl;
		cout << "presure - " << presure << endl;
	}

};


class Car: public Corps, public Engeen, public Wheel
{
private:
	string type;
	string mark;

public:
	Car(string size, string color, int volume, int power, int diametr, int presure, string type, string mark) :
		Corps(size, color), Engeen(volume, power), Wheel(diametr, presure), type(type), mark(mark) { }



	void Show()
	{
		cout << "\tCAR" << endl;
		cout << "  corps:" << endl;
		Corps::HoldInnerDet();
		Corps::Show();

		cout << "  engeen:" << endl;
		Engeen::StartWork();
		Engeen::Show();

		cout << "  wheels:" << endl;
		Wheel::MoveCar();
		Wheel::Show();
		cout << "\ntype - " << type << endl;
		cout << "mark - " << mark << endl;
	}
};

int main()
{
	Car car("huge", "black", 2500, 350, 35, 50, "passenger", "marcedess");
	car.Show();

	system("pause");
	return 0;
}

