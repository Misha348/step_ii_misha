#include <iostream>
#include <string>
using namespace std;

class Passport
{
protected:
	int passNumb;
	string name;
	string surname;
	string regPlace;

public:
	Passport(): passNumb(0), name("non"), surname("non"), regPlace("non")
	{}
	Passport(int pNumb, string name, string surname, string rPlace  )
		:passNumb(pNumb), name(name), surname(surname), regPlace(rPlace)
	{}

	void PrintPassData() const
	{
		cout << "pass id - " << passNumb << endl;
		cout << "name - " << name << endl;
		cout << "surname - " << surname << endl;
		cout << "regist pl. - " << regPlace << endl;
	}
};

class ForignPassport : public Passport
{
private:
	int forPID;
	string nation;
	string visa;
	bool IsActive;

public:
	ForignPassport(int id, string nation, string visa, bool act, int pNumb, string name, string surname, string rPlace)

		:Passport(pNumb, name, surname, rPlace), nation(nation), forPID(id), visa(visa), IsActive(act)  {}

	void PrintPassData() const
	{
		cout << "name - " << name << endl;
		cout << "surname - " << surname << endl;
		cout << "regist pl. - " << regPlace << endl;
		cout << "pass id - " << forPID << endl;
		cout << "nation - " << nation << endl;
		cout << "visa - " << visa << endl;
		if(IsActive)
		cout << "activity - is active" << endl;	
		else
			cout << "activity - not active" << endl;
	}
};

int main()
{
	Passport pas(234567, "Mykola", "Prisyagnyuk", "Rivne");
	pas.PrintPassData();
	cout << "----------------\n";

	ForignPassport forPass(999999, "Ukrainian", "german", true, 234567, "Mykola", "Prisyagnyuk", "Rivne");
	forPass.PrintPassData();

	system("pause");
	return 0;
}