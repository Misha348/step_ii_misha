#include <iostream>
#include <string>
#include <map>
#include "Dictionary.h"
using namespace std;

ostream& operator << (ostream& out, const Word& word)
{	
	out << word.word << endl;	
	return out;
}

ostream& operator << (ostream& out, const Meaning& def)
{
	out << def.definit << endl;
	return out;
}

//map<string, int>::iterator findByValue(map<string, string>& m, string value)
//{
//	for (map<string, int>::iterator i = m.begin(); i != m.end(); ++i)
//	{
//		if (i->second == value)
//		{
//			return i;
//		}
//	}
//	return m.end();
//}

int main()
{
	map<Word, Meaning>m;
	m.insert(make_pair(Word("cat"), Meaning("��")));
	m.insert(make_pair(Word("dog"), Meaning("������")));
	m.insert(make_pair(Word("chair"), Meaning("������")));
	m.insert(make_pair(Word("tambe"), Meaning("���")));
	m.insert(make_pair(Word("mouse"), Meaning("����")));

	for (auto i : m)
	{
		cout << "word: " << i.first << " meaning: " << i.second << endl;
		cout << "------------------\n";
	}
	cout << "==========================\n" << endl;

	m.erase("mouse");
	cout << "==========================\n" << endl;

	map<Word, Meaning>::iterator it = m.find("dog");
	if (it == m.end())
	{
		cout << "Not found.\n";
	}
	else
	{
		cout << "Found\n";
		cout << "Key: " << it->first << " Value: " << it->second << endl;
	}




	system("pause");
	return 0;
}