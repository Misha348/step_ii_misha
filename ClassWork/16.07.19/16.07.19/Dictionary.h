#pragma once
#include <iostream>
#include <string>
#include <map>
using namespace std;

class Word
{
private:
	string word;

public:
	Word() { }
	Word(string word) : word(word) { }
	Word(const char* word) : word(string(word)) { }

	void ShowWord()
	{
		cout << "word: " << word << endl;
	}

	friend ostream& operator << (ostream& out, const Word& word);
	bool operator < (const Word& other) const
	{
		return this->word < other.word;
	}

	bool operator==(const Word& other) const
	{
		return this->word == other.word;
	}
};

class Meaning
{
private:
	string definit;

public:
	Meaning(string definit) : definit(definit) { }
	void ShowDefinit()
	{
		cout << "definit. - " << definit << endl;
	}
	friend ostream& operator << (ostream& out, const Meaning& def);
};



