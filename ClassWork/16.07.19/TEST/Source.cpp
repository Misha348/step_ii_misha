#include <iostream>
#include <map>
#include <string>
using namespace std;

struct ID
{
	int id;
	string name;

	ID() {}
	ID(int id, string name) : id(id), name(name)
	{

	}

	void Show() const
	{
		cout << "ID: " << id << endl;
		cout << "Name: " << name << endl;
	}

	bool operator < (const ID& other) const
	{
		return this->name < other.name;
	}
};

map<string, int>::iterator findByValue(map<string, int>& m, int value)
{
	for (map<string, int>::iterator i = m.begin(); i != m.end(); ++i)
	{
		if (i->second == value)
		{
			return i;
		}
	}
	return m.end();
}

void main()
{
	/*map<ID, int> m;

	ID id1(1, "Taras");
	pair<ID, int> pair(id1, 100);
	m.insert(pair);

	m.insert(make_pair(ID(3, "Anna"), 200));
	m.insert(make_pair(ID(2, "Blabla"), 6335));

	m[ID(7, "Anna")] = 40;
	cout << "Anna: " << m[ID(7, "Anna")] << endl;

	for (auto i : m)
	{
		i.first.Show();
		cout << "Value: " << i.second << endl;
		cout << "------------------\n";
	}*/

	map<int, int> m;
	map<string, int> m2;

	m2.insert(make_pair("Valia", 3));
	auto result = m2.insert(make_pair("Valia2", 4));

	if (result.second)
	{
		cout << "Added\n";
	}
	else
	{
		cout << "Ignore\n";
	}
	cout << "Value: " << m2["Valia"] << endl;
	cout << "Value: " << m2["Valia2"] << endl;

	// Search element
	map<string, int>::iterator it = m2.find("Valia22");
	if (it == m2.end())
	{
		cout << "Not found.\n";
	}
	else
	{
		cout << "Found\n";
		cout << "Key: " << it->first << " Value: " << it->second << endl;
	}

	it = findByValue(m2, 5);
	if (it != m2.end())
		cout << "Key: " << it->first << " Value: " << it->second << endl;

	if (it != m2.end())
		m2.erase(it);

	for (auto i : m2)
	{
		cout << "Key: " << i.first << " Value: " << i.second << endl;
		cout << "------------------\n";
	}

	m2["Valia9"] = 100500;

	for (auto i : m2)
	{
		cout << "Key: " << i.first << " Value: " << i.second << endl;
		cout << "------------------\n";
	}

	system("pause");
}