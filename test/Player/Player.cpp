#include "Player.h"

// Private methods
void Player::SetAvgPointsPerGame()
{
	avgPointsPerGame = points / games;
}

// Public methods
void Player::Show() const
{
	cout << "Name: " << name << endl;
	cout << "Age: " << age << endl;
	cout << "Points: " << points << endl;
	cout << "Games: " << games << endl;
	cout << "Avg: " << avgPointsPerGame << endl;
	cout << "Gender: " << (isMale ? "Male" : "Female") << endl;
}

void Player::Fill()
{
	cout << "Name: "; cin >> name;
	cout << "Age: "; cin >> age;
	cout << "Points: "; cin >> points;
	cout << "Games: "; cin >> games;
	cout << "Gender (0 - female, 1 - male): "; cin >> isMale;

	SetAvgPointsPerGame();
}

void Player::ChangeGender()
{
	SetGender(!isMale);
}

// Setters
void Player::SetName(const string newName)
{
	if (newName.length() <= 0)
	{
		return;
	}

	name = newName;
}
void Player::SetAge(const int newAge)
{
	if (newAge < 0 || newAge > 300)
		return;

	age = newAge;
}
void Player::SetPoints(const int newPoints)
{
	if (newPoints < 0)
		return;

	points = newPoints;
	SetAvgPointsPerGame();
}
void Player::SetGames(const int newGames)
{
	if (newGames < 0)
		return;

	games = newGames;
	SetAvgPointsPerGame();
}
void Player::SetGender(const bool newIsMale)
{
	isMale = newIsMale;
}

// Getters
string Player::GetName() const
{
	return name;
}
int Player::GetAge() const
{
	return age;
}
int Player::GetGames() const
{
	return games;
}
int Player::GetPoints() const
{
	return points;
}
int Player::GetAveragePointsPerGame() const
{
	return avgPointsPerGame;
}