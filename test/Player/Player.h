#pragma once
#include <string>
#include <iostream>
using namespace std;

class Player
{
private:
	string name;
	int age;
	float points;
	bool isMale;
	int games;
	double avgPointsPerGame;

	void SetAvgPointsPerGame();

public:
	// Setters
	void SetName(const string newName);
	void SetAge(const int newAge);
	void SetPoints(const int newPoints);
	void SetGames(const int newGames);
	void SetGender(const bool newIsMale);

	void ChangeGender();

	// Getters
	string GetName() const;
	int GetAge() const;
	int GetGames() const;
	int GetPoints() const;
	int GetAveragePointsPerGame() const;

	void Show() const;

	void Fill();
};