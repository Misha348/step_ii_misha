#include "Queue.h"
#include <ctime>
#include <cstdlib>

void main()
{
	srand(time(0));

	// Створення черги
	Queue q(25);

	// Добавляємо 5 елементів в чергу
	for (int i = 0; i < 5; i++)
		q.Add(rand() % 30);

	// Показ черги
	q.Show();
	// Вилучення елемента
	q.Extract();
	// Показ черги
	q.Show();

	system("pause");
}