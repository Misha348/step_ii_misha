#include "Stack.h"
#include <iostream>
using namespace std;

void main()
{
	Stack st;

	st.Push(56);
	cout << "Count: " << st.GetCount() << endl;
	st.Push(23);
	st.Push(77);
	st.Push(88);
	cout << "Count: " << st.GetCount() << endl;
	cout << "Element: " << st.Pop() << endl;
	cout << "Count: " << st.GetCount() << endl;

	system("pause");
}

// (fg(5)"иироро"