#include "Header.h"

using namespace TeacherRes;	// підключення всіх членів з простору імені
using StudentRes::id;		// підключення конкретного члена з простору імені

void UseAnonymousNamespace()
{
	A classA;
	classA.Show();
}

void main()
{
	UseAnonymousNamespace();

	TeacherInfo t1;
	t1.Show();
	TeacherInfo t2;
	t2.Show();

	StudentRes::StudentInfo s1; // явне вказання простору імені
	s1.Show();
	StudentRes::StudentInfo s2;
	s2.Show();

	TeacherInfo t3;
	t3.Show();
	TeacherInfo t4;
	t4.Show();

	StudentRes::StudentInfo s3;
	s3.Show();
	StudentRes::StudentInfo s4;
	s4.Show();

	system("pause");
}