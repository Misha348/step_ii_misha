#include "Header.h"

int StudentRes::id = 0;
int TeacherRes::id = 0;
int StudentRes::avgMark = 0;

void StudentRes::StudentInfo::Show() const
{
	cout << "Student...\n";
	cout << "ID: " << this->id << endl;
	cout << "Next ID: " << StudentRes::id << endl;
}

void TeacherRes::TeacherInfo::Show() const
{
	cout << "Teacher...\n";
	cout << "ID: " << this->id << endl;
	cout << "Next ID: " << TeacherRes::id << endl;
}