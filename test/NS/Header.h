#pragma once
#include <iostream>

using std::cout;
using std::endl;

namespace StudentRes
{
	extern int id;
	class StudentInfo
	{
		int id;
	public:
		StudentInfo()
		{
			id = StudentRes::id++;
		}
		void Show() const;
	};
}

namespace TeacherRes
{
	extern int id;
	class TeacherInfo
	{
		int id;
	public:
		TeacherInfo()
		{
			id = TeacherRes::id++;
		}
		void Show() const;
	};
}

namespace StudentRes
{
	// продовження простору імен StudentRes
	extern int avgMark;
}

namespace
{
	class A
	{
	public:
		void Show() const
		{
			cout << "Class in anonymous namespace!\n";
		}
	};
}