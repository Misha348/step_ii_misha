#include "Value.h"
#define LINE cout << "_______________________" << endl;

template <typename V>
void Fill(V *, int);

int Value<int>::id = 0;
int Value<char>::id = 0;
void main()
{
	Value<int> o1(12), o2(20), o3(100);
	Value <char> s(97);
	s.Print();

	LINE
	LINE
	o1.Print();
	o2.Print();
	o3.Print();

	LINE



		o1 = move(o2);
	o1.Print();
	o2.Print();

	LINE

		o2 = move(o3);
	o2.Print();
	o3.Print();

	LINE

		int arr[5];
	Fill(arr, 5);

	char str[10];
	Fill(str, 10);
	system("pause");
}

template <typename V>
void Fill(V *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		arr[i] = i + 45;
		cout << arr[i] << " ";
	}
	cout << endl;
}