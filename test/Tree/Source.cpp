#include "Tree.h"
#include <iostream>
using namespace std;

void main()
{
	Tree tree;
	tree.Add(4);
	tree.Add(5);
	tree.Add(1);
	tree.Add(2);
	tree.Add(3);

	
	tree.PrintSort();
	cout << "-------------------\n";

	if (tree.FindLine(3))
		cout << "Found\n";
	else
		cout << "Not found\n";

	system("pause");
}