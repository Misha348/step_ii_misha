#include "List.h"
#include <iostream>
using namespace std;

void main()
{
	List list;

	list.AddTail(4);
	list.AddTail(10);
	list.AddTail(30);
	list.ShowList();
	cout << "--------------------\n";

	/*list.Add(1, 3);
	list.ShowList();
	cout << "--------------------\n";

	list.Add(2, 9);
	list.ShowList();
	cout << "--------------------\n";*/

	list.Add(40, 29);
	list.ShowList();
	cout << "--------------------\n";

	system("pause");
}