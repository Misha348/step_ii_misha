#include "Date.h"

int Date::DaysInMonth(int month, int year) const
{
	switch (month)
	{
	case 1: case 3: case 5: case 7: case 9: case 11:
		return 31;
		break;
	case 4: case 6: case 8: case 10: case 12:
		return 30;
		break;
	case 2:
		if (!LeapYear(year))
		{
			return 28;
		}
		else
		{
			return 29;
		}
	default:
		{
		//return (-1);
		break;
		}
	}
}

bool Date::LeapYear(int year) const
{
	//��-�������� ���������� ��������� ���, ����� �������� ������ ������, �� ���������� �������� ��� ���, 
	//������� ���� ������ 100. ����� ���� ���� ����������� ������ �����, ����� �������� ��� � �� 400.
	return (year % 4 == 0 && year % 100 == 0 && year % 400 == 0 || year % 4 == 0 && year % 100 != 0);
}
