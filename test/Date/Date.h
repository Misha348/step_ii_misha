#pragma once
#include <iostream>
#include <fstream>

using namespace std;

class Date
{
private:
	int day;
	int month;
	int year;
public:
	Date() : day(0), month (0), year (0) {}
	Date(int day, int month, int year)
	{
		(*this).day = day;
		(*this).month = month;
		(*this).year = year;
	}
	int DaysInMonth(int month, int year) const;
	bool LeapYear(int year) const;

	int operator-(const Date& other) const
	{
		int res = 0;	
		res = DaysInMonth(month, year) - day;

		int tmp = 0;
		for (int i = month+1; i <=12; i++)
		{
			res += DaysInMonth(i, year);
		}

		for (int i = other.year+1; i < year; i++)
		{

				if (LeapYear(i) != true)
				{
					res = res + 365;
				}
				else
				{
					res = res + 366;
				}

		}
		





		return res;
	}
	

};
