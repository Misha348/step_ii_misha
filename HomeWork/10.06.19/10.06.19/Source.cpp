#include <iostream>
#include "StrReplConstr.h"
using namespace std;

StringOfChars CreateStrOfChars()
{
	char myString[] = { "ctrc_ctr_v" };
	StringOfChars testStr(myString);
	return testStr;
}

int main()
{
	
	StringOfChars charArr = CreateStrOfChars();
	charArr.Show();
	cout << "==============================" << endl;



	char myString1[] = { "abc" };
	char myString2[] = { "abc" };

	StringOfChars str1 = StringOfChars(myString1);
	StringOfChars str2 = StringOfChars(myString2);

	str1.Show();
	str2.Show();
	cout << "==============================" << endl;

	StringOfChars strRes;
	strRes = str1 + str2;
	strRes.Show();
	cout << "==============================" << endl;

	/*str1 = str2;
	str1.Show();
	cout << "==============================" << endl;*/
	
	StringOfChars strRes1;
	strRes1 = str1 * str2;
	strRes1.ShowSimilarSbm();




	system("pause");
	return 0;
}