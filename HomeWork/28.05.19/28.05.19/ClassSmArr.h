#pragma once
#include <iostream>;
using namespace std;

class SmartArr
{
private:
	int **arr;
	int rows;
	int cols;	

public:
	void AddRow(int pos);
	void ShowArr();
	void FindMaxEl();
	void FindMinEl();

	SmartArr();
	SmartArr(int rows, int cols);
	SmartArr(const SmartArr& arr);
	~SmartArr();

};

