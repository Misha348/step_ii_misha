#include <iostream>
#include <ctime>
#include "ClassSmArr.h"
using namespace std;

void SmartArr::AddRow( int pos)
{
	rows++;
	int **newArr = new int*[rows];
	for (int i = 0; i < rows; i++)
	{
		newArr[i] = new int[cols];
	}
	
	for (int i = 0; i < pos; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}

	for (int i = pos; i < (pos + 1); i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = 1111111;
		}
	}

	for (int i = pos + 1; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i - 1][j];
		}
	}

	delete[] arr;
	arr = newArr;

}

void SmartArr::FindMaxEl()
{
	int max = arr[0][0];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (arr[i][j] > max)
			{
				max = arr[i][j];
			}
		}
	}
	cout << "MAX = " << max << endl;
}

void SmartArr::FindMinEl()
{
	int min = arr[0][0];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			if (arr[i][j] < min)
			{
				min = arr[i][j];
			}
		}
	}
	cout << "MIN = " << min << endl;
}

SmartArr::SmartArr()
{
	arr = nullptr;
	rows = 0;
	cols = 0;
}

SmartArr::SmartArr(int rows, int cols)
{
	
	this->rows = rows;
	this->cols = cols;

	arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		arr[i] = new int[cols];
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = rand() % 20;
		}
	}
}

void SmartArr::ShowArr()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}
}

SmartArr::SmartArr(const SmartArr& arr)
{
	this->rows = arr.rows;
	this->cols = arr.cols;

	this->arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		this->arr[i] = new int[cols];
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			this->arr[i][j] = arr.arr[i][j];
		}
	}
}

SmartArr::~SmartArr()
{
	for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;
}