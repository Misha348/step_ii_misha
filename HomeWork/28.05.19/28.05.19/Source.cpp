#include <iostream>
#include <ctime>
#include "ClassSmArr.h"
using namespace std;

SmartArr CreateArr(int rows, int cols)
{
	SmartArr arr = SmartArr(rows, cols);	
	cout << "\t CONSTRUCTOR I - ( CREATED in function  [CreateArr] )\n" << endl;
	arr.ShowArr();
	cout << "================================" << endl;
	return arr;
}

int main()
{
	srand(unsigned(time(NULL)));	
	int rows = 0, p = 0, pos = 0, cols = 0;
	int max = 0;

	cout << "Enter quantity of rows: "; cin >> rows;
	cout << "Enter quantity of cols: "; cin >> cols;	
	
	SmartArr arr1 = CreateArr(rows, cols);
	cout << "\t( RECEIVED from function )\n" << endl;
	arr1.ShowArr();

	cout << "================================" << endl;
	cout << "\t CONSTRUCTOR II - COPY." << endl;
	SmartArr arr2 = SmartArr(arr1);
	arr2.ShowArr();

	cout << "================================" << endl;
	arr2.FindMaxEl();
	arr2.FindMinEl();
	cout << "================================" << endl;

	
	cout << "enter posit. for adding a row: "; 
	do 
	{
		cin >> p;
		pos = p - 1;
		if (pos > rows || pos < 0)
			cout << "No such amount of rows in arr. Try again:";
	
	} while (pos > rows || pos < 0);

	arr2.AddRow(pos);
	arr2.ShowArr();
	

	

	system("pause");
	return 0;
}