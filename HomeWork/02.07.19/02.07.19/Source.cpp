#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cctype>
#include <iostream>
using namespace std;

class OutOfRangeExept
{
public:
	OutOfRangeExept() { }

	void messageOutOfrange()
	{
		cout << " To big digit for int type\n" << endl;
	}
};

class OutOfNumSystExept
{
public:
	OutOfNumSystExept() { }

	void messageOutOfNumSyst()
	{
		cout << "line is out of numeric system" << endl;
	}
};

class stringConverter
{
private:
	char *str;
	int size;
	int convRes;
	bool isNegative;

public:
	stringConverter()
	{
		str = nullptr;
		size = 0;	
		convRes = 0;
		isNegative = false;
	} 

	stringConverter( const char *str)
	{		
		this->size = strlen(str);
		this->str = new char[size + 1];
		for (int i = 0; i < size; i++)
		{
			this->str[i] = str[i];
		}
		this->str[size] = '\0';		
	}

	~stringConverter()
	{
		delete[] str;
	}

	void Convert()
	{				
		for (int i = 0; i < size; i++)
		{		
			if (str[i] == '-')
			{
				isNegative = true;
				continue;
			}
				
			if (isdigit(str[i]))
			{
				if (i < 10)
				{
					if (i == 0)
					{
						convRes += ((int)str[i] - '0');
					}
					if (i > 0)
					{
						convRes *= 10;
						convRes += ((int)str[i] - '0');
					}
				}
				else
				{
					convRes = 0;
					throw OutOfRangeExept();
				}					
			}
			else
				throw OutOfNumSystExept();
		}
		if (isNegative == true)
			convRes *= (-1);				
	}

	void ShowString()
	{
		cout << "char *str = ";
		for (int i = 0; i < size; i++)
		{
			cout << str[i] << "";
		}
	}

	void ShowConvertString()
	{
		cout << "   to int = " << convRes << endl;
	}
};


int main()
{
	stringConverter myString("-645634784");
	myString.ShowString();
	cout << "\n==========================" << endl;

	try
	{
		myString.Convert();
	}
	catch(OutOfRangeExept exept)
	{
		exept.messageOutOfrange();
	}
	
	myString.ShowConvertString();
	cout << "\n===============================================\n" << endl;

	stringConverter myString1("1pi9go8yf");
	myString1.ShowString();
	cout << "\n==========================" << endl;
	try
	{
		myString1.Convert();
	}
	catch (OutOfNumSystExept exept)
	{
		exept.messageOutOfNumSyst();
	}


	/*char c[10] = "123";
	int i = atoi(c);
	cout << i;*/
	


	system("pause");
	return 0;
}