#include <iostream>
#include <string>
#include <fstream>
#include "Header.h"
using namespace std;	

int main()
{
	string path;
	cout << "Enter path to file (F.txt): "; getline(cin, path);
	File myFile(path);

	cout << "\t[ 1 ] - show file data.\n";
	myFile.ShowFiledata();
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 2 ] - find specified line.\n" << endl;
	string s;
	cout << "enter some line for searching: "; getline(cin, s);
	myFile.FindSpecifiedLine(s);	
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 3 ] - change symbol.\n" << endl;
	char sb;
	char sb1;
	cout << "enter symbol for changing: "; cin >> sb;
	cout << "enter symbol to changing: "; cin >> sb1;
	myFile.ChangeSymbol(sb, sb1);
	myFile.ShowFiledata();
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 4 ] - change line.\n" << endl;
	cin.ignore();
	string s1;
	string s2;
	cout << "enter some line for changing (delete): "; getline(cin, s1);
	cout << "enter some line for changing (input): "; getline(cin, s2);
	myFile.ChangeLine(s1, s2);
	myFile.ShowFiledata();
	cout << "\n=============================================\n" << endl;	

	cout << "\t[ 5 ] - count quantity of lines in file.\n" << endl;
	cout << "lines = " << myFile.CountQuantityOfLinesInFile() << endl;
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 6 ] - count quantity of sentences in file.\n" << endl;
	cout << "sentences = " << myFile.CountQuantityOfSentencesInFile() << endl;
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 7 ] - count quantity of words in file.\n" << endl;
	cout << "words = " << myFile.CountQuantityOfWordsInFile() << endl;
	cout << "\n=============================================\n" << endl;

	cout << "\t[ 8 ] - replace line.\n" << endl;
	myFile.ReplaceLine();
	myFile.ShowFiledata();
	cout << "\n=============================================\n" << endl;

	system("pause");
	return 0;
}
