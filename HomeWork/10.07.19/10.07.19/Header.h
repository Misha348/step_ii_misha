#pragma once
#include <iostream>
#include <string>
//#include <algorithm>
#include <fstream>
using namespace std;

class File
{
private:
	string pathToFile;
	string data;	

public:
	File(string pathToFile) : pathToFile(pathToFile)
	{
		ifstream input;
		input.open(pathToFile, ios_base::in);

		if (!input.is_open())
		{
			cout << "Can't open the file.";			
		}
		else
		{
			while (!input.eof())
			{
				char s;				
				input.get(s);
				data += s;
			}			
		}		
	}

	~File()
	{
		ofstream output(pathToFile, ios_base::out | ios_base::trunc);

		if (!output.is_open())
		{
			cout << "Can't open the file.";
		}
		output << data;
		output.close();
	}	

	void ShowFiledata()
	{		
		cout << "\n" << data;
	}

	void FindSpecifiedLine(string s)
	{
		int k = 0;
		size_t pos = -1;
		do
		{
			pos = data.find(s, pos + 1);

			if (pos != string::npos)
			{
				k++;
			}

		} while (pos != string::npos);
		cout << "quantity of specified lines: " << k << endl;		
	}

	void ChangeSymbol(char sb, char sb1)
	{
		int c = 0;
		int len = data.length();		
		for (int i = 0; i < len; i++)
		{
			if (data[i] == sb)
			{
				data[i] = sb1;
				c++;
			}				
		}
		cout << "amount of changings: " << c << endl;
	}

	void ChangeLine(string s1, string s2)
	{
		int len = s2.length();
		int k = 0;
		size_t pos = -1;
		do
		{
			pos = data.find(s1, pos + 1);

			if (pos != string::npos)
			{				
				data.erase(pos, len);
				data.insert(pos, s2);
				k++;
			}

		} while (pos != string::npos);
		cout << "quantity of changed lines: " << k << endl;
	}

	void  ReplaceLine()
	{
		reverse(data.begin(), data.end());		
	}

	int CountQuantityOfLinesInFile()
	{
		int k = 0;
		for (int i = 0; i < data.length(); i++)
		{
			if (data[i] == '\n')
			{
				k++;
			}
		}
		return k + 1;
	}

	int CountQuantityOfSentencesInFile()
	{
		int k1 = 0;
		for (int i = 0; i < data.length(); i++)
		{			
			if (data[i] == '.')
			{
				k1++;
			}
		}
		return k1 - 1;
	}

	int CountQuantityOfWordsInFile()
	{
		int k2 = 0;
		for (int i = 0; i < data.length(); i++)
		{
			if ( (data[i] != ' ') && (data[i + 1] == ' ') && (data[i+2] != ' ') || (data[i] == '.') && (data[i + 1] == '\n')/* && (data[i + 2] != ' ')*/ )
			{
				k2++;
			}
		}
		return k2 + 1;
	}

};