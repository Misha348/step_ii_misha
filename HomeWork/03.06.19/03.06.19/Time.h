#pragma once
#include <iostream>
using namespace std;

class Time
{
private:
	int seconds, minutes, hours;	

	friend void operator--(Time &time);
	friend Time operator++(Time &time, int i);
	friend bool operator==(const Time& left, const Time& right);
	friend bool operator>(const Time& left, const Time& right);

public:
	Time() : seconds(0), minutes(0), hours(0) {}
	Time(int sec);		
	Time(int hou, int min, int sec);
	void ShowTime();
	void ConvertSec(int sec);
	

	//bool operator>(const Time &other) const;
	bool operator<(const Time &other) const;
	bool operator>=(const Time &other) const;
	bool operator<=(const Time &other) const;
	//bool operator==(const Time &other) const;
	bool operator!=(const Time &other) const;

	Time operator+(const Time& other) const;
	
	Time& operator++();
	Time& operator--();
	Time& operator++(int i);
	Time& operator--(int i);

	//void operator--();

	void operator()(int hou, int min, int sec);
	void operator()(int sec);

	

	int GetSeconds() const;

};