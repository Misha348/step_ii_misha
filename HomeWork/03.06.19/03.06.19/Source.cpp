#include <iostream>
#include "Time.h"
using namespace std;

// friend func.
void operator--(Time &time)
{
	int sec = (time.hours * 3600) + (time.minutes * 60) + time.seconds;
	sec--;
	time.ConvertSec(sec);
}

Time operator++(Time &time, int i)
{
	Time tmp;
	int sec = (time.hours * 3600) + (time.minutes * 60) + time.seconds;
	tmp.ConvertSec(sec);
	++sec;
	time.ConvertSec(sec);
	return tmp;
}

bool operator==(const Time& left, const Time& right)
{
	return (left.hours * 3600) + (left.minutes * 60) + (left.seconds) == (right.hours * 3600) + (right.minutes * 60) + (right.seconds);
}

bool operator>(const Time& left, const Time& right)
{
	//TODO to make logic (to compare hours+mins+sec) for using only private fields (friend reload should be used omly with private fields.)
	return (left.GetSeconds()) > (right.GetSeconds());
}

int main()
{
	Time time1 = Time(22, 13, 18);
	cout << "Time 1 - ";  time1.ShowTime();

	Time time2 = Time(18, 59, 59);
	cout << "Time 2 - ";  time2.ShowTime();
	cout << "=========================" << endl;

	Time time4 = Time(0, 0, 1);
	Time time5 = time1 + time4;
	cout << " (+ 1 sec to Time 1)  ";
	time5.ShowTime();
	cout << "=========================" << endl;	

	/*if (time1 > time2)
		cout << "time1 > time2 - true" << endl;
	else
		cout << "!(time1 > time2) - false" << endl;
	cout << "=========================" << endl;

	if (time1 < time2)
		cout << "time1 < time2 - true" << endl;
	else
		cout << "!(time1 < time2) - false" << endl;
	cout << "=========================" << endl;

	if (time1 >= time2)
		cout << "time1 >= time2 - true" << endl;
	else
		cout << "!(time1 >= time2) - false" << endl;
	cout << "=========================" << endl;

	if (time1 <= time2)
		cout << "time1 < time2 - true" << endl;
	else
		cout << "!(time1 < time2) - false" << endl;
	cout << "=========================" << endl;

	if (time1 == time2)
		cout << "time1 == time2 - true" << endl;
	else
		cout << "time1 != time2 - false" << endl;
	cout << "=========================" << endl;

	if (time1 != time2)
		cout << "!(time1 != time2) - true" << endl;
	else
		cout << "time1 != time2 - false" << endl;
	cout << "=========================" << endl;
	
	int sec = 0;
	cout << "enter quantity of seconds:"; cin >> sec;
	Time time6 = Time(sec);
	time6.ShowTime();
	cout << "=========================" << endl;*/

	//===============================================================
	
	/*++time2;	
	time2.ShowTime();
	cout << "1=========================" << endl;

	--time2;
	time2.ShowTime();
	cout << "2=========================" << endl;

	Time tmp = time2++;
	time2.ShowTime();
	cout << "3=========================" << endl;
	tmp.ShowTime();
	cout << "3.5=========================" << endl;	

	Time tmp1 = time2--;
	time2.ShowTime();
	cout << "4=========================" << endl;
	tmp1.ShowTime();
	cout << "4.5=========================" << endl;*/

	//==================================================================
	/*time2(3, 7, 13);
	time2.ShowTime();
	cout << "=========================" << endl;

	time2(60);
	time2.ShowTime();*/
	//==================================================================
	

	/*Time tmp2 = time2--;
	time2.ShowTime();
	cout << "1=========================" << endl;
	tmp2.ShowTime();
	cout << "1.5=========================" << endl; 
	*/

	/*++time2;
	time2.ShowTime();
	cout << "=========================" << endl;*/

	//==================================================================

	if (time1 == time2)
		cout << "time1 == time2 - true" << endl;
	else
		cout << "time1 != time2 - false" << endl;
	cout << "=========================" << endl;

	if(time1 > time2)
	cout << "time1 > time2 - true" << endl;
	else
	cout << "!(time1 > time2) - false" << endl;
	cout << "=========================" << endl;

	system("pause");
	return 0;
}