#include <iostream>
#include "Time.h"
using namespace std;

Time::Time(int sec)
{
	int tmpSec1 = sec;	
	
	this->hours = tmpSec1 / 3600;
	this->seconds = tmpSec1 - (this->hours * 3600);
	this->minutes = this->seconds / 60;
	this->seconds = this->seconds - (this->minutes * 60);	
}

Time::Time(int hou, int min, int sec)
{
	this->hours = hou;
	this->minutes = min;
	this->seconds = sec;
}

void Time::ShowTime()
{
	cout << this->hours << ":" << this->minutes << ":" << this->seconds << endl;
}

void Time::ConvertSec( int sec)
{
	int tmpSec = sec;

	this->hours = tmpSec / 3600;
	this->seconds = tmpSec - (this->hours * 3600);
	this->minutes = this->seconds / 60;
	this->seconds = this->seconds - (this->minutes * 60);
}



//bool Time::operator>(const Time &other) const
//{
//	return this->hours > other.hours;
//}

bool Time::operator<(const Time &other) const
{
	return this->hours < other.hours;
}

bool Time::operator>=(const Time &other) const
{
	return this->hours >= other.hours;
}

bool Time::operator<=(const Time &other) const
{
	return this->hours <= other.hours;
}

//bool Time::operator==(const Time &other) const
//{
//	return (this->hours == other.hours && this->minutes == other.minutes && this->seconds == other.seconds);
//}

bool Time::operator!=(const Time &other) const
{
	return (this->hours != other.hours || this->minutes != other.minutes || this->seconds != other.seconds);
}

Time Time::operator+(const Time& other) const
{
	return Time(this->hours + other.hours, this->minutes + other.minutes, this->seconds + other.seconds);
}

Time& Time::operator++()
{
	this->seconds = (this->hours * 3600) + (this->minutes * 60) + this->seconds;
	++seconds;
	ConvertSec(seconds);
	return *this;
}

Time& Time::operator--()
{
	this->seconds = (this->hours * 3600) + (this->minutes * 60) + this->seconds;
	--seconds;
	ConvertSec(seconds);
	return *this;
}

Time& Time::operator++(int i)
{
	Time tmp;
	tmp.seconds = this->seconds;
	tmp.minutes = this->minutes;
	tmp.hours = this->hours;

	int tmpSec = (this->hours * 3600) + (this->minutes * 60) + this->seconds;
	tmpSec++;
	ConvertSec(tmpSec);

	return tmp;
}

Time& Time::operator--(int i)
{
	Time tmp;
	tmp.seconds = this->seconds;
	tmp.minutes = this->minutes;
	tmp.hours = this->hours;

	int tmpSec = (this->hours * 3600) + (this->minutes * 60) + this->seconds;
	tmpSec--;
	ConvertSec(tmpSec);

	return tmp;
}

void Time::operator()(int hou, int min, int sec)
{
	int thisTmpSec = (this->hours * 3600) + (this->minutes * 60) + this->seconds;
	int tmpSec2 = (hou * 3600) + (min * 60) + sec + thisTmpSec;
	ConvertSec(tmpSec2);	
}

void Time::operator()(int sec)
{
	if (this->seconds == 59)
	{
		this->minutes += 1;
		this->seconds = sec - 1;
	}
	else if(this->seconds < 59)
	{
		this->seconds += sec;
		if (this->seconds > 59)
		{
			this->minutes += 1;
			this->seconds = this->seconds - sec - 1;
		}
	}
}

int Time::GetSeconds() const
{
	return (this->hours * 3600) + (this->minutes * 60) + this->seconds;
}