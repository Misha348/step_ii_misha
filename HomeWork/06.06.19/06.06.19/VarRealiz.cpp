#include <iostream>
#include <string>
#include "Var.h"
using namespace std;

Var::Var()
{
	Int = 0;
	Double = 0;
	String = " ";
	type = 0;
}

Var::Var(const int i_number)
{
	type = 1;
	Int = i_number;
}

Var::Var(const double d_number)
{
	type = 2;
	Double = d_number;
}

Var::Var(const string s_string)
{
	type = 3;
	String = s_string;
}

void Var::Show()
{
	if (type == 1)
		cout << Int;
	else if (type == 2)
		cout << Double;
	else if (type == 3)
		cout << String;
}

int Var::ToInt() const
{
	if (type == 1)
		return Int;
	else if (type == 2)
		return (int)Double;
	else if (type == 3)
		return stoi(String);
}

double Var::ToDouble() const
{
	if (type == 1)
		return  (double)Int;
	else if (type == 2)
		return Double;
	else if (type == 3)
		return stod(String);
}

string Var::ToString() const
{
	if (type == 1)
		return  to_string(Int);
	else if (type == 2)
		return  to_string(Double);
	else if (type == 3)
		return  String;
}




