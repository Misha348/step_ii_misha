#pragma once
#include <iostream>
#include <string>
using namespace std;

// TODO 1) - str - type

class Var
{
private:
	int Int;
	double Double;
	string String;
	int type;

	//friend Var operator*( Var& thisObj, const Var& otherObj);
	//friend string operator *(const string& leftStr, const string& rightStr);

	friend Var operator+(const Var& thisObj, const Var& otherObj); // try to put const
	friend Var operator-(const Var& thisObj, const Var& otherObj);
	friend Var operator*(const Var& thisObj, Var& otherObj);
	friend Var operator/(const Var& thisObj, Var& otherObj);

	friend Var operator+=( Var& thisObj, const Var& otherObj);
	friend Var operator-=(Var& thisObj, const Var& otherObj);
	friend Var operator*=( Var& thisObj, Var& otherObj);
	friend Var operator/=( Var& thisObj, Var& otherObj);

	friend bool operator>(const Var& thisObj, const Var& otherObj);
	friend bool operator<(const Var& thisObj, const Var& otherObj);
	friend bool operator>=(const Var& thisObj, const Var& otherObj);
	friend bool operator<=(const Var& thisObj, const Var& otherObj);

	friend bool operator==(const Var& thisObj, const Var& otherObj);
	friend bool operator!=(const Var& thisObj, const Var& otherObj);

	

public:
	Var();
	Var(const int i_number);
	Var(const double d_number);
	Var(const string s_string);

	void Show();

	/*Var& operator=(const Var& otherObj)
	{
		this->String = otherObj.String;
		return *this;
	}*/

	int ToInt() const;
	double ToDouble() const;
	string ToString() const;




	/*char ConvertToChar(string String)
	{
		const char * ps;
		ps = String.c_str();
		return *ps;
	}*/




};