#include <iostream>
#include <string>
#include <Windows.h>
#include "Var.h"
using namespace std;
HANDLE hConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

void MakeLine()
{
	SetConsoleTextAttribute(hConsoleHandle, 2);
	cout << "########################################" << endl;
	SetConsoleTextAttribute(hConsoleHandle, 7);
}

Var operator+(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int + otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double + otherObj.ToDouble());
	else if (thisObj.type == 3)
		return  Var(thisObj.String + otherObj.ToString());
}

Var operator-(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int - otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double - otherObj.ToDouble());
	else if (thisObj.type == 3)
		return  Var(thisObj.String - otherObj.ToString());
}

Var operator*(const Var& thisObj, Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int * otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double * otherObj.ToDouble());

	else if (thisObj.type == 3)
	{
		if (otherObj.type != 3)
		{
			otherObj.String = otherObj.ToString();
		}
		const char * str1;
		str1 = (thisObj.String).c_str();

		const char * str2;
		str2 = (otherObj.String).c_str();

		int k = 0;
		char tmpStr[30];
		for (int i = 0; i < strlen(str1); i++)
		{
			for (int j = 0; j < strlen(str2); j++)
			{
				if (str1[i] == str2[j])
				{
					tmpStr[k] = str1[i];
					++k;
				}
			}
		}
		tmpStr[k] = 0;
		
		int length = strlen(tmpStr);
		char* newTmp = new char[length + 1];

		int c = 0;
		int l = 0;

		for (int i = 0; i < length; i++)
		{
			int c = 0;
			for (int j = 0; j < length; j++)
			{
				if ((tmpStr[i]) != (newTmp[j]))
				{
					c++;
					if (c == length)
					{
						newTmp[l] = tmpStr[i];
						l++;
					}
				}
				if ((tmpStr[i]) == (newTmp[j]))
				{					
				}
			}
		}
		newTmp[l] = '\0';

		Var newStr(newTmp);
		delete[] newTmp;
		return newStr;
	}		
}

Var operator/(const Var& thisObj, Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int / otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double / otherObj.ToDouble());

	else if (thisObj.type == 3)
	{
		if (otherObj.type != 3)
		{
			otherObj.String = otherObj.ToString();
		}
		const char * str1;
		str1 = (thisObj.String).c_str();

		const char * str2;
		str2 = (otherObj.String).c_str();

		int k = 0;

		char tmpStr[30];
		for (int i = 0; i < strlen(str1); i++)
		{
			int f = 0;
			for (int j = 0; j < strlen(str2); j++)
			{
				if (str1[i] != str2[j])
				{
					++f;
				}
				if (f == strlen(str2))
				{
					tmpStr[k] = str1[i];
					++k;
				}
			}
		}
		tmpStr[k] = 0;
		Var newStr(tmpStr);
		return newStr;
	}
}

Var operator+=(Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int += otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double += otherObj.ToDouble());
	else if (thisObj.type == 3)
		return  Var(thisObj.String += otherObj.ToString());
}

Var operator-=(Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int -= otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double -= otherObj.ToDouble());
	/*else if (thisObj.type == 3)
		return  Var(thisObj.String -= otherObj.ToString());*/
}

Var operator*=( Var& thisObj, Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int *= otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double *= otherObj.ToDouble());
	
	else if (thisObj.type == 3)
	{
		if (otherObj.type != 3)
		{
			otherObj.String = otherObj.ToString();
		}
		const char * str1;
		str1 = (thisObj.String).c_str();

		const char * str2;
		str2 = (otherObj.String).c_str();

		int k = 0;
		char tmpStr[30];
		for (int i = 0; i < strlen(str1); i++)
		{
			for (int j = 0; j < strlen(str2); j++)
			{
				if (str1[i] == str2[j])
				{
					tmpStr[k] = str1[i];
					++k;
				}
			}
		}
		tmpStr[k] = 0;

		int length = strlen(tmpStr);
		char* newTmp = new char[length + 1];

		int c = 0;
		int l = 0;

		for (int i = 0; i < length; i++)
		{
			int c = 0;
			for (int j = 0; j < length; j++)
			{
				if ((tmpStr[i]) != (newTmp[j]))
				{
					c++;
					if (c == length)
					{
						newTmp[l] = tmpStr[i];
						l++;
					}
				}
				if ((tmpStr[i]) == (newTmp[j]))
				{
				}
			}
		}
		newTmp[l] = '\0';

		Var newStr(newTmp);
		delete[] newTmp;
		return newStr;
	}
		
}

Var operator/=( Var& thisObj, Var& otherObj)
{
	if (thisObj.type == 1)
		return Var(thisObj.Int /= otherObj.ToInt());
	else if (thisObj.type == 2)
		return  Var(thisObj.Double /= otherObj.ToDouble());
	else if (thisObj.type == 3)
	{
		if (otherObj.type != 3)
		{
			otherObj.String = otherObj.ToString();
		}
		const char * str1;
		str1 = (thisObj.String).c_str();

		const char * str2;
		str2 = (otherObj.String).c_str();

		int k = 0;
		
		char tmpStr[30];
		for (int i = 0; i < strlen(str1); i++)
		{
			int f = 0;
			for (int j = 0; j < strlen(str2); j++)
			{				
				if (str1[i] != str2[j])
				{
					++f;								
				}
				if (f == strlen(str2))
				{
					tmpStr[k] = str1[i];
					++k;
				}
			}
		}
		tmpStr[k] = 0;	
		Var newStr(tmpStr);
		return newStr;		
	}		
}

bool operator>(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int > otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double > otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String > otherObj.ToString();
	}
}

bool operator<(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int < otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double < otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String < otherObj.ToString();
	}
}

bool operator>=(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int >= otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double >= otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String >= otherObj.ToString();
	}
}

bool operator<=(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int <= otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double <= otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String <= otherObj.ToString();
	}
}

bool operator==(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int == otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double == otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String == otherObj.ToString();
	}
}

bool operator!=(const Var& thisObj, const Var& otherObj)
{
	if (thisObj.type == 1)
	{
		return thisObj.Int != otherObj.ToInt();
	}
	if (thisObj.type == 2)
	{
		return thisObj.Double != otherObj.ToDouble();
	}
	if (thisObj.type == 3)
	{
		return thisObj.String != otherObj.ToString();
	}
}

int main()
{
	Var i = Var(10);
	Var d = Var(22.3);
	Var str = Var("37");

	/*Var str1 = Var("123");
	Var res = (str * str1);
	res.Show();*/

	cout << "int\ti = "; i.Show();
	cout << "\ndouble  d = "; d.Show();
	cout << "\nstring  str = "; str.Show();
	cout << "\n=================\n" << endl;

	Var i1;

	/*cout << "================= INT + TYPE ===========" << endl;
	i1 = i + i;
	cout << "i + i = ";	i1.Show(); cout << endl;
	i1 = i + d;
	cout << "i + d = ";	i1.Show(); cout << endl;
	i1 = i + str;
	cout << "i + str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE + TYPE ========" << endl;
	i1 = d + d;
	cout << "d + d = ";	i1.Show(); cout << endl;
	i1 = d + i;
	cout << "d + i = ";	i1.Show(); cout << endl;
	i1 = d + str;
	cout << "d + str = ";	i1.Show(); cout << endl;

	cout << "================= STR + TYPE ===========" << endl;
	i1 = str + str;
	cout << "str + str = ";	i1.Show(); cout << endl;
	i1 = str + i;
	cout << "str + i = ";	i1.Show(); cout << endl;
	i1 = str + d;
	cout << "str + d = ";	i1.Show(); cout << endl;*/

	//MakeLine();

	/*cout << "================= INT - TYPE ===========" << endl;
	i1 = i - i;
	cout << "i - i = ";	i1.Show(); cout << endl;
	i1 = i - d;
	cout << "i - d = ";	i1.Show(); cout << endl;
	i1 = i - str;
	cout << "i - str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE - TYPE ========" << endl;
	i1 = d - d;
	cout << "d - d = ";	i1.Show(); cout << endl;
	i1 = d - i;
	cout << "d - i = ";	i1.Show(); cout << endl;
	i1 = d - str;
	cout << "d - str = ";	i1.Show(); cout << endl;*/

	/*cout << "================= STRING - TYPE ========" << endl;
	i1 = str - str;
	cout << "str - str = ";	i1.Show(); cout << endl;
	i1 = str - i;
	cout << "str - i = ";	i1.Show(); cout << endl;
	i1 = str - d;
	cout << "str - d = ";	i1.Show(); cout << endl;*/

	//MakeLine();

	/*cout << "================= INT * TYPE ===========" << endl;
	i1 = i * i;
	cout << "i * i = ";	i1.Show(); cout << endl;
	i1 = i * d;
	cout << "i * d = ";	i1.Show(); cout << endl;
	i1 = i * str;
	cout << "i / str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE * TYPE ========" << endl;
	i1 = d * d;
	cout << "d * d = ";	i1.Show(); cout << endl;
	i1 = d * i;
	cout << "d * i = ";	i1.Show(); cout << endl;
	i1 = d * str;
	cout << "d * str = ";	i1.Show(); cout << endl;

	cout << "================= STRING * TYPE ========" << endl;
	i1 = str * str;
	cout << "str * str = ";	i1.Show(); cout << endl;
	i1 = str * i;
	cout << "str * i = ";	i1.Show(); cout << endl;
	i1 = str * d;
	cout << "str * d = ";	i1.Show(); cout << endl;*/

	//MakeLine();

	/*cout << "================= INT / TYPE ===========" << endl;
	i1 = i / i;
	cout << "i / i = ";	i1.Show(); cout << endl;
	i1 = i / d;
	cout << "i / d = ";	i1.Show(); cout << endl;
	i1 = i / str;
	cout << "i / str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE / TYPE ========" << endl;
	i1 = d / d;
	cout << "d / d = ";	i1.Show(); cout << endl;
	i1 = d / i;
	cout << "d / i = ";	i1.Show(); cout << endl;
	i1 = d / str;
	cout << "d / str = ";	i1.Show(); cout << endl;
	
	cout << "================= STRING / TYPE ========" << endl;
	i1 = str / str;
	cout << "str / str = ";	i1.Show(); cout << endl;
	i1 = str / i;
	cout << "str / i = ";	i1.Show(); cout << endl;
	i1 = str / d;
	cout << "str / d = ";	i1.Show(); cout << endl;*/

	//MakeLine();

	/*cout << "================= INT += TYPE ===========" << endl;
	i1 = i += i;
	cout << "i += i = ";	i1.Show(); cout << endl;
	i1 = i += d;
	cout << "i += d = ";	i1.Show(); cout << endl;
	i1 = i += str;
	cout << "i += str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE += TYPE ========" << endl;
	i1 = d += d;
	cout << "d += d = ";	i1.Show(); cout << endl;
	i1 = d += i;
	cout << "d += i = ";	i1.Show(); cout << endl;
	i1 = d += str;
	cout << "d += str = ";	i1.Show(); cout << endl;

	cout << "================= STRING += TYPE ========" << endl;
	i1 = str += str;
	cout << "str += str = ";	i1.Show(); cout << endl;
	i1 = str += i;
	cout << "str += i = ";	i1.Show(); cout << endl;
	i1 = str += d;
	cout << "str += d = ";	i1.Show(); cout << endl;*/

	MakeLine();

	/*cout << "================= INT -= TYPE ===========" << endl;
	i1 = i -= i;
	cout << "i -= i = ";	i1.Show(); cout << endl;
	i1 = i -= d;
	cout << "i -= d = ";	i1.Show(); cout << endl;
	i1 = i -= str;
	cout << "i -= str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE -= TYPE ========" << endl;
	i1 = d -= d;
	cout << "d -= d = ";	i1.Show(); cout << endl;
	i1 = d -= i;
	cout << "d -= i = ";	i1.Show(); cout << endl;
	i1 = d -= str;
	cout << "d -= str = ";	i1.Show(); cout << endl;*/

	///*cout << "================= STRING -= TYPE ========" << endl;
	//i1 = str += str;
	//cout << "str += str = ";	i1.Show(); cout << endl;
	//i1 = str += i;
	//cout << "str += i = ";	i1.Show(); cout << endl;
	//i1 = str += d;
	//cout << "str += d = ";	i1.Show(); cout << endl;*/

	MakeLine();

	/*cout << "================= INT *= TYPE ===========" << endl;
	i1 = i *= i;
	cout << "i *= i = ";	i1.Show(); cout << endl;
	i1 = i *= d;
	cout << "i *= d = ";	i1.Show(); cout << endl;
	i1 = i *= str;
	cout << "i *= str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE *= TYPE ========" << endl;
	i1 = d *= d;
	cout << "d *= d = ";	i1.Show(); cout << endl;
	i1 = d *= i;
	cout << "d *= i = ";	i1.Show(); cout << endl;
	i1 = d *= str;
	cout << "d *= str = ";	i1.Show(); cout << endl;*/

	/*cout << "================= STRING *= TYPE ========" << endl;
	i1 = str *= str;
	cout << "str *= str = ";	i1.Show(); cout << endl;
	i1 = str *= i;
	cout << "str *= i = ";	i1.Show(); cout << endl;
	i1 = str *= d;
	cout << "str *= d = ";	i1.Show(); cout << endl;*/

	MakeLine();

	/*cout << "================= INT /= TYPE ===========" << endl;
	i1 = i /= i;
	cout << "i /= i = ";	i1.Show(); cout << endl;
	i1 = i /= d;
	cout << "i /= d = ";	i1.Show(); cout << endl;
	i1 = i /= str;
	cout << "i /= str = ";	i1.Show(); cout << endl;

	cout << "================= DOUBLE /= TYPE ========" << endl;

	Var i3 = Var(5);
	Var d1 = Var(5.5);
	Var str1 = Var("50");
	Var i2;

	i2 = (d1 /= d1);
	cout << "d /= d = ";	i2.Show(); cout << endl;
	i2 = d1 /= i3;
	cout << "d /= i = ";	i2.Show(); cout << endl;
	i2 = d1 /= str1;
	cout << "d /= str = ";	i2.Show(); cout << endl;

	cout << "================= STRING /= TYPE ========" << endl;
	i1 = str /= str;
	cout << "str /= str = ";	i1.Show(); cout << endl;
	i1 = str /= i;
	cout << "str /= i = ";	i1.Show(); cout << endl;
	i1 = str /= d;
	cout << "str /= d = ";	i1.Show(); cout << endl;*/

	MakeLine();

	cout << "================= INT > TYPE ===========" << endl;
	if (i > i)
		cout << "i > i - false";
	else
		cout << "i !> i - true" << endl;


	if (i > d)
		cout << "i > d - false";
	else
		cout << "i !> d - true" << endl;

	if (i > str)
		cout << "i > str - false";
	else
		cout << "i !> str - true" << endl;

	cout << "================= DOUBLE > TYPE ========" << endl;

	if (d > d)
		cout << "d > d - false";
	else
		cout << "d !> d - true" << endl;
	if (d > i)
		cout << "d > i - true" << endl;
	else
		cout << "d !> i - false " << endl;

	if (d > str)
		cout << "d > str - false" << endl;
	else
		cout << "d !> str - true" << endl;

	cout << "================= STRING > TYPE ========" << endl;

	if (str > i)
		cout << "str > i - true" << endl;
	else
		cout << "str !> i - false " << endl;

	if (str > d)
		cout << "str > d - true";
	else
		cout << "str !> d - false" << endl;

	if (str > str)
		cout << "str > str - false";
	else
		cout << "str !> str - true" << endl;

	MakeLine();

	cout << "================= INT < TYPE ===========" << endl;
	if (i < i)
		cout << "i > i - false" << endl;
	else
		cout << "i !< i - true" << endl;	
	if (i < d)
		cout << "i < d - true" << endl;
	else
		cout << "i !< d - false" << endl;

	if (i < str)
		cout << "i < str - false" << endl;
	else
		cout << "i !< str - true" << endl;

	cout << "================= DOUBLE < TYPE ========" << endl;

	if (d < d)
		cout << "d < d - false" << endl;
	else
		cout << "d !< d - true" << endl;	
	if (d < i)
		cout << "d < i - false" << endl;
	else
		cout << "d !< i - true " << endl;

	if (d < str)
		cout << "d < str - false" << endl;
	else
		cout << "d !< str - true" << endl;

	cout << "================= STRING < TYPE ========" << endl;

	if (str < i)
		cout << "str < i - false" << endl;
	else
		cout << "str !< i - true " << endl;

	if (str < d)
		cout << "str < d - false" << endl;
	else
		cout << "str !< d - true" << endl;

	if (str < str)
		cout << "str < str - false" << endl;
	else
		cout << "str !< str - true" << endl;

	MakeLine();

	cout << "================= INT >= TYPE ===========" << endl;
	if (i >= i)
		cout << "i >= i - true" << endl;
	else
		cout << "i ! >= i - false" << endl;	
	if (i >= d)
		cout << "i >= d - false" << endl;
	else
		cout << "i ! >= d - true" << endl;

	if (i >= str)
		cout << "i >= str - false" << endl;
	else
		cout << "i ! >= str - true" << endl;

	cout << "================= DOUBLE >= TYPE ========" << endl;

	if (d >= d)
		cout << "d >= d - true" << endl;
	else
		cout << "d ! >= d - false" << endl;
	
	if (d >= i)
		cout << "d >= i - true" << endl;
	else
		cout << "d ! >= i - false " << endl;

	if (d >= str)
		cout << "d >= str - false" << endl;
	else
		cout << "d ! >= str - true" << endl;

	cout << "================= STRING >= TYPE ========" << endl;
	if (str >= i)
		cout << "str >= i - true" << endl;
	else
		cout << "str ! >= i - false " << endl;

	if (str >= d)
		cout << "str >= d - true" << endl;
	else
		cout << "str ! >= d - false" << endl;

	if (str >= str)
		cout << "str >= str - true" << endl;
	else
		cout << "str ! >= str - true" << endl;

	MakeLine();

	cout << "================= INT >= TYPE ===========" << endl;
	if (i <= i)
		cout << "i <= i - true" << endl;
	else
		cout << "i ! <= i - false" << endl;
	if (i >= d)
		cout << "i <= d - false" << endl;
	else
		cout << "i ! <= d - true" << endl;

	if (i >= str)
		cout << "i <= str - false" << endl;
	else
		cout << "i ! <= str - true" << endl;

	cout << "================= DOUBLE >= TYPE ========" << endl;

	if (d <= d)
		cout << "d <= d - true" << endl;
	else
		cout << "d ! <= d - false" << endl;

	if (d <= i)
		cout << "d <= i - false" << endl;
	else
		cout << "d ! <= i - true " << endl;

	if (d <= str)
		cout << "d <= str - true" << endl;
	else
		cout << "d ! <= str - false" << endl;

	cout << "================= STRING <= TYPE ========" << endl;
	if (str <= i)
		cout << "str <= i - false" << endl;
	else
		cout << "str ! <= i - true " << endl;

	if (str <= d)
		cout << "str <= d - false" << endl;
	else
		cout << "str ! <= d - true" << endl;

	if (str <= str)
		cout << "str <= str - true" << endl;
	else
		cout << "str ! <= str - false" << endl;

	MakeLine();

	cout << "================= INT == TYPE ===========" << endl;
	if (i == i)
		cout << "i == i - true" << endl;
	else
		cout << "!(i == i) - false" << endl;
	if (i == d)
		cout << "i == d - false" << endl;
	else
		cout << "!(i == d) - true" << endl;
	if (i == str)
		cout << "i == str - false" << endl;
	else
		cout << "(!i == str) - true" << endl;

	cout << "================= DOUBLE >= TYPE ========" << endl;

	if (d == d)
		cout << "d == d - true" << endl;
	else
		cout << "!(d == d) - false" << endl;

	if (d == i)
		cout << "d == i - false" << endl;
	else
		cout << "!(d == i) - true " << endl;
	if (d == str)
		cout << "d == str - false" << endl;
	else
		cout << "!(d == str) - true" << endl;

	cout << "================= STRING == TYPE ========" << endl;
	if (str == i)
		cout << "str == i - false" << endl;
	else
		cout << "!(str == i) - true " << endl;

	if (str == d)
		cout << "str == d - false" << endl;
	else
		cout << "!(str == d) - true" << endl;

	if (str == str)
		cout << "str == str - true" << endl;
	else
		cout << "!(str == str) - false" << endl;

	MakeLine();

	cout << "================= INT == TYPE ===========" << endl;
	if (i == i)
		cout << "i == i - true" << endl;
	else
		cout << "!(i == i) - false" << endl;
	if (i == d)
		cout << "i == d - false" << endl;
	else
		cout << "!(i == d) - true" << endl;
	if (i == str)
		cout << "i == str - false" << endl;
	else
		cout << "(!i == str) - true" << endl;

	cout << "================= DOUBLE >= TYPE ========" << endl;

	if (d == d)
		cout << "d == d - true" << endl;
	else
		cout << "!(d == d) - false" << endl;

	if (d == i)
		cout << "d == i - false" << endl;
	else
		cout << "!(d == i) - true " << endl;
	if (d == str)
		cout << "d == str - false" << endl;
	else
		cout << "!(d == str) - true" << endl;

	cout << "================= STRING == TYPE ========" << endl;
	if (str == i)
		cout << "str == i - false" << endl;
	else
		cout << "!(str == i) - true " << endl;

	if (str == d)
		cout << "str == d - false" << endl;
	else
		cout << "!(str == d) - true" << endl;

	if (str == str)
		cout << "str == str - true" << endl;
	else
		cout << "!(str == str) - false" << endl;

	MakeLine();

	cout << "================= INT != TYPE ===========" << endl;
	if (i != i)
		cout << "i != i - false" << endl;
	else
		cout << "!(i != i) - true" << endl;
	if (i != d)
		cout << "i != d - true" << endl;
	else
		cout << "!(i != d) - false" << endl;
	if (i != str)
		cout << "i != str - true" << endl;
	else
		cout << "!(i != str) - false" << endl;

	cout << "================= DOUBLE >= TYPE ========" << endl;

	if (d != d)
		cout << "d != d - false" << endl;
	else
		cout << "!(d != d) - true" << endl;

	if (d != i)
		cout << "d != i - true" << endl;
	else
		cout << "!(d != i) - false " << endl;
	if (d != str)
		cout << "d != str - true" << endl;
	else
		cout << "!(d != str) - false" << endl;

	cout << "================= STRING == TYPE ========" << endl;
	if (str != i)
		cout << "str != i - true" << endl;
	else
		cout << "!(str != i) - false " << endl;

	if (str != d)
		cout << "str != d - true" << endl;
	else
		cout << "!(str != d) - false" << endl;

	if (str != str)
		cout << "str != str - false" << endl;
	else
		cout << "!(str != str) - true" << endl;

	/*string s = "10";
	int a = stoi(s) + 6;

	s = to_string(a);
	cout << s << endl;*/


	/*int a = 10;
	string s = "10";
	int c = 0;
	c = a + stoi(s);
	cout << c << endl;*/
	system("pause");
	return 0;
}