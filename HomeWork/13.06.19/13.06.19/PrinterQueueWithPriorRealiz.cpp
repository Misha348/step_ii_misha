#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <ctime>
#include "PrinterQueueWithPrior.h"
using namespace std;

PrintersQueue::PrintersQueue(int maxSize)
{	
	this->maxSize = maxSize;
	data = new string[maxSize];
	prioritet = new int[maxSize];	
	currentSize = 0;
}

PrintersQueue::~PrintersQueue()
{
	delete[] data;
	delete[] prioritet;	
}

void PrintersQueue::AddUserWithPriority(string name, int priority)
{
	if (!QueueIsFull())
	{
		data[currentSize] = name;
		prioritet[currentSize] = priority;		
		++currentSize;

		for (int i = 0; i < currentSize - 1; i++)
		{
			for (int j = i + 1; j < currentSize; j++)
			{
				if (prioritet[i] > prioritet[j])
				{
					int tmp = prioritet[i];
					prioritet[i] = prioritet[j];
					prioritet[j] = tmp;

					string tmpName = data[i];
					data[i] = data[j];
					data[j] = tmpName;
				}
			}
		}

	}
}

string PrintersQueue::GetName() const
{
	return *data;
}

int PrintersQueue::GetPrioritet() const
{
	return *prioritet;
}

bool PrintersQueue::QueueIsEmpty() const
{
	return currentSize == 0;
}

bool PrintersQueue::QueueIsFull() const
{
	return currentSize == maxSize;
}

void PrintersQueue::Clear()
{
	currentSize = 0;
}

string PrintersQueue::ExtractData()
{
	if (!QueueIsEmpty())
	{
		int priorInd = 0;
		int priorVal = prioritet[0];

		for (int i = 1; i < currentSize; ++i)
		{
			if (prioritet[i] > priorVal)
			{
				priorVal = prioritet[i];
				priorInd = i;
			}
		}
		string elem = data[priorInd];

		for (int i = priorInd; i < currentSize - 1; i++)
		{
			data[i] = data[i + 1];
			prioritet[i] = prioritet[i + 1];
		}
		--currentSize;
		return elem;
	}
}

void PrintersQueue::Show() const
{
	time_t now = time(0);
	tm* localtm = localtime(&now);	

	cout << "\n------------------------------------------\n";	
	cout << "\t - Print Statistic:" << endl;
	for (int i = 0; i < currentSize; i++)
		cout << "user:\t" << data[i] << " - " << asctime(localtm) << endl;
	cout << "\n------------------------------------------\n";
}

