#define _CRT_SECURE_NO_WARNINGS
#include "PrinterQueueWithPrior.h"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <windows.h>
using namespace std;

int main()
{
	struct tm *ptr;
	time_t It;
	It = time(NULL);
	ptr = localtime(&It);
	//printf(asctime(ptr));

	int size = 0;
	int prior = 0;
	string name;

	cout << "enter zize of queue: "; cin >> size;
	PrintersQueue requestQue(size);
	PrintersQueue statisticQue(size);

	system("cls");
	cout << "Handle users requests: " << endl;
	while (!requestQue.QueueIsFull())
	{
		cout << "enter users name: "; cin >> name;
		cout << "enter prioritet for request: "; cin >> prior;

		
		
		requestQue.AddUserWithPriority(name, prior);
		statisticQue.AddUserWithPriority(name, prior);
		
		system("cls");
	}		

	cout << "print statistic: " << endl;
	while (!statisticQue.QueueIsEmpty())
	{
		time_t now = time(0);
		tm* localtm = localtime(&now);
		Sleep(2000);
		cout << "\nuser: " << statisticQue.ExtractData() << " - " << asctime(localtm) << endl;
		
	}	

	system("pause");
	return 0;
}