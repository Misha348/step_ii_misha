#pragma once
#include <iostream>
#include <string>
using namespace std;

class PrintersQueue
{
private:
	string *data;
	int *prioritet;
	int maxSize;
	int currentSize;	

public:
	
	PrintersQueue(int maxSize);
	~PrintersQueue();	

	void AddUserWithPriority(string name, int priority);
	bool QueueIsEmpty() const;
	bool QueueIsFull() const;
	void Clear();
	string ExtractData();
	string GetName() const;
	int GetPrioritet() const;
	
	void Show() const;
};