#include <iostream>
#include <string>
#include "ClassStudent.h"
using namespace std;

int main()
{
	Student student;
	student.FillData();
	cout << "-------------------------------\n";
	system("cls");
	student.ShowData();
	cout << "-------------------------------\n";
	
	student.SaveStudDataToFile(student);
	cout << "\tHad been saved.\n";
	cout << "-------------------------------\n";
	cout << "\tHas loaded.\n";
	student.LoadStudDataFromFile(student);	
	cout << "-------------------------------\n";

	system("pause");
	return 0;
}