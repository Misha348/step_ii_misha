#pragma once
#include <string>
#include <iostream>
using namespace std;

class Student
{
private:
	string name;
	string surname;
	int birthYear;
	int birthMonth;
	int birthDay;
	string state;
	string city;
	string tel;

public:
	string GetName() const;
	string GetSurname() const;
	int GetBirthYear() const;
	int GetBirthMonth() const;
	int GetBirthDay() const;
	string GetState() const;
	string GetCity() const;
	string GetTel() const;

	void FillData();
	void ShowData() const;

	void SaveStudDataToFile(Student &student);
	void LoadStudDataFromFile(Student &student);
};