#include <iostream>
#include <string>
#include <fstream>
#include "ClassStudent.h"
using namespace std;


string Student::GetName() const
{
	return name;
}

string Student::GetSurname() const
{
	return surname;
}

int Student::GetBirthYear() const
{
	return birthYear;
}

int Student::GetBirthMonth() const
{
	return birthMonth;
}

int Student::GetBirthDay() const
{
	return birthDay;
}

string Student::GetState() const
{
	return state;
}

string Student::GetCity() const
{
	return city;
}

string Student::GetTel() const
{
	return tel;
}

void Student::FillData()
{
	cout << "Name: "; cin >> name;
	cout << "Surname: "; cin >> surname;
	cout << "Birth year: ";	cin >> birthYear;
	cout << "Birth month: "; cin >> birthMonth;
	cout << "Birth day: "; cin >> birthDay;
	cout << "State: "; cin >> state;
	cout << "City: "; cin >> city;
	cout << "Tel: ";cin >> tel;
}

void Student::ShowData() const
{
	cout << "Name: " << name << endl;
	cout << "Surname: " << surname << endl;
	cout << "Birth year: " << birthYear << endl;
	cout << "Birth month: " << birthMonth << endl;
	cout << "Birth day: " << birthDay << endl;
	cout << "State: " << state << endl;
	cout << "City: " << city << endl;
	cout << "Tel: " << tel << endl;
}

void Student::SaveStudDataToFile(Student &student)
{
	ofstream wrFile("Stud_Data.txt", ios_base::out);

	wrFile << student.name << endl;
	wrFile << student.surname << endl;
	wrFile << student.birthYear << endl;
	wrFile << student.birthMonth << endl;
	wrFile << student.birthDay << endl;
	wrFile << student.state << endl;
	wrFile << student.city << endl;
	wrFile << student.tel << endl;

	wrFile.close();
}

void Student::LoadStudDataFromFile(Student &student)
{
	Student tmpStudent;
	ifstream rdFile("Stud_Data.txt", ios_base::in);

	rdFile >> tmpStudent.name;
	rdFile >> tmpStudent.surname;
	rdFile >> tmpStudent.birthYear;
	rdFile >> tmpStudent.birthMonth;
	rdFile >> tmpStudent.birthDay;
	rdFile >> tmpStudent.state;
	rdFile >> tmpStudent.city;
	rdFile >> tmpStudent.tel;

	rdFile.close();
	student = tmpStudent;

	tmpStudent.ShowData();
}