#pragma once
#include <iostream>
#include <string>
#include "Student.h"
#include"Group.h"
using namespace std;

double Group::CalculateAverGrMark()
{
	double sum = 0;
	for (int i = 0; i < quantity; i++)
	{
		sum += Arr[i].GetMark();
	}
	AverGrMark = sum / quantity;
	return AverGrMark;
}

void Group::SetGroupSpec(string specializ)
{
	this->specializ = specializ;
}

string Group::GetSpec() const
{
	return specializ;
}

Group::Group()
{
	quantity = 0;
	Arr = nullptr;
}

Group::Group(Student *studentsArr, int newSize)
{
	quantity = newSize;
	Arr = new Student[quantity];
	for (int i = 0; i < quantity; i++)
	{
		Arr[i] = studentsArr[i];
	}
}

void Group::AddOneStudent(Student *&studentsArr, int &quantity, const Student &newStudent)
{
	Student *tmpArrStud = new Student[quantity + 1];
	for (int i = 0; i < quantity; i++)
	{
		tmpArrStud[i] = studentsArr[i];
	}
	tmpArrStud[quantity] = newStudent;
	delete[] studentsArr;
	studentsArr = tmpArrStud;
	quantity++;
}