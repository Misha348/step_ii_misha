#pragma once
#include <iostream>
#include <string>
#include"Student.h"
using namespace std;

class Group
{
private:	
	int quantity;
	Student *Arr;

	string name;
	string specializ;
	double AverGrMark;
	double CalculateAverGrMark();	

public:
	void SetGrName(string name);
	string GetGrName() const;
	void SetGroupSpec(string specializ);
	string GetSpec() const;
	
	Group();
	Group(Student *studArr, int newSize);
	void GetStudByMark(int mark);
	void AddOneStudent(Student *&studentsArr, int &quantity, const Student &newStudent);
	const Group& GetBetterGr(const Group& group) const;

	void SortByMark();
	void SortByMissing();

	void ShowGrInfo();

	~Group();

};