#pragma once
#include <iostream>
#include <string>
#include "Student.h"
#include"Group.h"
using namespace std;

void Group::SetGrName(string name)
{	
	this->name = name;
}

string Group::GetGrName() const
{
	return name;
}

double Group::CalculateAverGrMark()
{
	double sum = 0;
	for (int i = 0; i < quantity; i++)
	{
		sum += Arr[i].GetMark();
	}
	AverGrMark = sum / quantity;
	return AverGrMark;
}

void Group::SetGroupSpec(string specializ)
{
	this->specializ = specializ;
}

string Group::GetSpec() const
{
	return specializ;
}

Group::Group()
{
	quantity = 0;
	Arr = nullptr;
}

Group::Group(Student *studentsArr, int newSize)
{
	quantity = newSize;
	Arr = new Student[quantity];
	for (int i = 0; i < quantity; i++)
	{
		Arr[i] = studentsArr[i];
	}
}

void Group::AddOneStudent(Student *&studentsArr, int &oldQuantity, const Student &newStudent)
{
	this->quantity = oldQuantity;
	Arr = new Student[quantity + 1];
	for (int i = 0; i < quantity; i++)
	{
		Arr[i] = studentsArr[i];
	}
	Arr[quantity] = newStudent;
	delete[] studentsArr;
	studentsArr = Arr;
	quantity++;
}

void Group::ShowGrInfo()
{	
	cout << "\t\tSPEC: " << GetSpec() << endl;
	cout << "\t\tGROUP: " << GetGrName() << endl;
	for (int i = 0; i < quantity; i++)
	{
		cout << "  --[ " << i+1 << " ]--" << endl;
		Arr[i].ShowShortData();
		cout << endl;
	}
	cout << "Gr Av Point: " << CalculateAverGrMark() << endl;
	cout << "=============================" << endl;
}

const Group & Group::GetBetterGr(const Group & group) const
{
	if (group.AverGrMark > this->AverGrMark)
	{
		return group;
	}
	else
	{
		return *this;
	}
}

void Group::GetStudByMark(int mark)
{
	int c = 0;
	for (int i = 0; i < quantity; i++)
	{
		if (Arr[i].GetMark() >= mark)
			Arr[i].ShowShortData();
		cout << endl;
		c++;
	}
	if (c == 0)
		cout << "no such students." << endl;
}

void Group::SortByMark()
{
	
	for (int i = 0; i < (quantity - 1); i++)
	{
		for (int j = i + 1; j < quantity; j++)
		{
			if ( Arr[i].GetMark() < Arr[j].GetMark() )
			{
				Student tmp = Arr[i];
				Arr[i] = Arr[j];
				Arr[j] = tmp;
			}
		}
	}
}

void Group::SortByMissing()
{
	for (int i = 0; i < (quantity - 1); i++)
	{
		for (int j = i + 1; j < quantity; j++)
		{
			if (Arr[i].GetMissings() < Arr[j].GetMissings() )
			{
				Student tmp = Arr[i];
				Arr[i] = Arr[j];
				Arr[j] = tmp;
			}
		}
		
	}
}

Group::~Group()
{
	delete[] Arr;
}