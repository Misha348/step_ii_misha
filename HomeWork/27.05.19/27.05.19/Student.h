#pragma once
#include <iostream>
#include <string>

using namespace std;

class Student
{
private:
	string name;
	string surname;
	int age;
	int mark;
	int missing;	

public:

	void SetName(string name);
	void SetSurname(string surname);
	void SetAge(int age);
	void SetMark(int mark);
	void SetMissing(int missing);

	string GetName();
	string GetSurname();
	int GetAge();
	int GetMark();
	int GetMissings();

	Student();	
	Student(string name, string surname, int age, int mark, int missing);	
		
	void SetStData();

	void ShowFullData();
	void ShowShortData();
	
	const Student& GetBetterStud(const Student& student) const;

	~Student();
};