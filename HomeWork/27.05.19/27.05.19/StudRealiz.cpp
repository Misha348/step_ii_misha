#include <iostream>
#include <string>
#include "Student.h"
using namespace std;

void Student::SetName(string name)
{
	//if (name.length > 0)
		this->name = name;
}

void Student:: SetSurname(string surname)
{
	//if (surname.length > 0)
		this->surname = surname;
}

void Student::SetAge(int age)
{
	while (age < 15)
		cout << "to young." << endl;
}

void Student::SetMark(int mark)
{
	while (mark < 1 || mark > 12)
		cout << "incorrect mark" << endl;
}

void Student::SetMissing(int missing)
{
	this->missing = missing;
}

string Student::GetName()
{
	return name;
}

string Student::GetSurname()
{
	return surname;
}

int Student::GetAge()
{
	return age;
}

int Student::GetMark()
{
	return mark;
}

int Student::GetMissings()
{
	return missing;
}

void Student::SetStData()
{
	cout << endl;
	cout << "name: "; cin >> name;
	cout << "surname: "; cin >> surname;
	cout << "age: "; cin >> age;
	cout << "mark: "; cin >> mark;
	cout << "missings: "; cin >> missing;
}

Student::Student()
{
	this->name = "non";
	this->surname = "non";
	this->age = 0;
	this->mark = 0;
	this->missing = 0;
}

Student::Student(string name, string surname, int age, int mark, int missing)
{
	this->name = name;
	this->surname = surname;
	this->age = age;
	this->mark = mark;
	this->missing = missing;
}

void Student::ShowFullData()
{
	cout << "name: " << GetName() << endl;
	cout << "surname" << GetSurname() << endl;
	cout << "age: " << GetAge() << endl;
	cout << "mark: " << GetMark() << endl;
	cout << "missings: " << GetMissings() << endl;
}

void Student::ShowShortData()
{
	cout << "name: " << GetName() << endl;
	cout << "surname: " << GetSurname() << endl;
	cout << "mark: " << GetMark() << endl;
	cout << "missings: " << GetMissings() << endl;
}

const  Student& Student::GetBetterStud(const Student& student) const
{
	if (student.mark > this->mark)
	{
		return student;
	}
	else
	{
		return *this;
	}
}

Student::~Student() {}




