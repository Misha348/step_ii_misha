#pragma once
#include <iostream>
#include <string>
using namespace std;

class Memory
{
public:
	Memory() {}
	virtual void ShowInfo() const
	{
		cout << "-Memory" << endl;
	}
};

class DDR1 : public Memory
{
public:
	DDR1() : Memory() {}
	virtual void ShowInfo() const
	{
		cout << "  - DDR1 Memory" << endl;
	}
};

class DDR2 : public Memory
{
public:
	DDR2() : Memory() {}
	virtual void ShowInfo() const
	{
		cout << "  - DDR2 Memory" << endl;
	}
};