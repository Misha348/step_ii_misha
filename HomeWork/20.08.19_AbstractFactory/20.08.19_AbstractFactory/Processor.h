#pragma once
#include<iostream>
#include<string>
using namespace std;

class Processor
{
public:
	Processor() {}
	virtual void ShowInfo() const
	{
		cout << "-Processor" << endl;
	}
};

class IntelProcessor : public Processor
{
public:
	IntelProcessor() : Processor() {}
	virtual void ShowInfo() const
	{
		cout << "  - Intel Proccesor" << endl;
	}
};

class AMDProcessor : public Processor
{
public:
	AMDProcessor() : Processor() {}

	virtual void ShowInfo() const
	{
		cout << "  - AMD Proccesor" << endl;
	}
};