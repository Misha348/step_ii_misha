#pragma once
#include <iostream>
#include <string>
using namespace std;

class MainBoard
{
public:
	MainBoard() {}
	virtual void ShowInfo() const
	{
		cout << "-MainBoard" << endl;
	}
};

class ASUSMotherboard : public MainBoard
{
public:
	ASUSMotherboard() : MainBoard() {}
	virtual void ShowInfo() const
	{
		cout << "  - ASUS MainBoard" << endl;
	}
};

class MSIMotherboard : public MainBoard
{
public:
	MSIMotherboard() : MainBoard() {}

	virtual void ShowInfo() const
	{
		cout << "  - MSI MainBoard" << endl;
	}
};