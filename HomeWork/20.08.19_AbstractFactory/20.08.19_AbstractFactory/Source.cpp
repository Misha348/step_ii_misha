# include <iostream>
# include <string>
# include "Processor.h"
# include "Boxes.h"
# include "HDD.h"
# include "MainBoard.h"
# include "Memory.h"
# include "Factory.h"
using namespace std;

int main()
{
	IPCFactory *factory1 = new HomePCFactory();
	Configurator config;

	PC *pc1 = config.GetPC(factory1);
	pc1->ShowInfo();
	cout << "\n\n";

	IPCFactory *factory2 = new OfficePCFactory();
	PC *pc2 = config.GetPC(factory2);
	pc2->ShowInfo();


	
	system("pause");
	return 0;
}