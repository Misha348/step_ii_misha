#pragma once
#include <iostream>
#include <string>
using namespace std;

class HDD
{
public:
	HDD() {}
	virtual void ShowInfo()
	{
		cout << "-HDD" << endl;
	}
};

class LGHDD: public HDD
{
public:
	LGHDD(): HDD() {}
	virtual void ShowInfo()
	{
		cout << "  - LGHDD" << endl;
	}
};

class SamsungHDD: public HDD
{
public:
	SamsungHDD() : HDD() {}
	virtual void ShowInfo()
	{
		cout << "  - SamsungHDD" << endl;
	}
};