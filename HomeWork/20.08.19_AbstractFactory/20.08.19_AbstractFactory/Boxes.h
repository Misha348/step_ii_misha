#pragma once
#include <iostream>
#include <string>
using namespace std;

class Box
{
public:
	Box() {}
	virtual void ShowInfo() const
	{
		cout << "-Box" << endl;
	}
};

class BlackBox: public Box
{
public:
	BlackBox(): Box() {}
	virtual void ShowInfo() const
	{
		cout << "  - BlackBox" << endl;
	}
};

class SilverBox: public Box
{
public:
	SilverBox(): Box() {}
	virtual void ShowInfo() const
	{
		cout << "  - SilverBox" << endl;
	}
};
