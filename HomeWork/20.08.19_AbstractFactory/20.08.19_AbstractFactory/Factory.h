#pragma once
#include <iostream>
#include <string>
#include "Processor.h"
#include "Boxes.h"
#include "HDD.h"
#include "MainBoard.h"
#include "Memory.h"
using namespace std;

class IPCFactory
{
public:
	virtual Processor *CreateProcessor() = 0;
	virtual Memory *CreateMemory() = 0;
	virtual MainBoard *CreateMainBoard() = 0;
	virtual Box *CreateBox() = 0;
	virtual HDD *CreateHDD() = 0;
};

class HomePCFactory : public IPCFactory
{
public:
	Processor *CreateProcessor()
	{
		return new IntelProcessor();
	}
	Memory *CreateMemory()
	{
		return new DDR2();
	}
	MainBoard *CreateMainBoard()
	{
		return new MSIMotherboard();
	}
	Box *CreateBox()
	{
		return new SilverBox();
	}
	HDD *CreateHDD()
	{
		return new SamsungHDD();
	}
};

class OfficePCFactory : public IPCFactory
{
public:
	Processor *CreateProcessor()
	{
		return new AMDProcessor();
	}
	Memory *CreateMemory()
	{
		return new DDR1();
	}
	MainBoard *CreateMainBoard()
	{
		return new ASUSMotherboard();
	}
	Box *CreateBox()
	{
		return new BlackBox();
	}
	HDD *CreateHDD()
	{
		return new LGHDD();
	}
};

class PC
{
protected:
	Processor *proc;
	Memory *memor;
	MainBoard* mb;
	Box *box;
	HDD *hdd;

public:
	PC() {}

	void SetProcessor(Processor *proc)
	{
		this->proc = proc;
	}
	void SetMemory(Memory *memor)
	{
		this->memor = memor;
	}
	void SetMotherboard(MainBoard *mb)
	{
		this->mb = mb;
	}
	void SetBox(Box *box)
	{
		this->box = box;
	}
	void SetHDD(HDD *hdd)
	{
		this->hdd = hdd;
	}

	void ShowInfo() const
	{
		cout << "  PC:" << endl;
		cout << "-Processor:\t"; proc->ShowInfo();
		cout << "-Memory:\t"; memor->ShowInfo();
		cout << "-Mainboard:\t"; mb->ShowInfo();
		cout << "-Box:\t\t"; box->ShowInfo();
		cout << "-HDD:\t\t"; hdd->ShowInfo();
	}
};

class Configurator
{
	IPCFactory *factory;
	
public:
	Configurator() {}

	PC *GetPC(IPCFactory * factory)
	{
		this->factory = factory;		
		PC *pc = new PC();
		pc->SetProcessor(factory->CreateProcessor());
		pc->SetMemory(factory->CreateMemory());
		pc->SetMotherboard(factory->CreateMainBoard());
		pc->SetBox(factory->CreateBox());
		pc->SetHDD(factory->CreateHDD());
		return pc;
	}
};