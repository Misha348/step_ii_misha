#pragma once
#include <iostream>
#include <string>

using namespace std;

struct Firm
{
	string firmName;
	string spheere;
	string city;	
	string owner;	
	string phone;

	void Show() const
	{
		cout << "city: " << city << endl;
		cout << "owner: " << owner << endl;
		cout << "phone: " << phone << endl;
	}

	Firm *next;
};


class RefBook
{
private:
	Firm *head;
	Firm *tail;
	int size;

public:
	RefBook();
	~RefBook();

	void AddToBegining(string firmName, string city, string owner, string spheere, string phone);
	void AddToEnd(string firmName, string city, string owner, string spheere, string phone);
	void DelAll();
	void DelHead();
	bool IsEmpty() const;
	void Show() const;

	void SearchByFirmname(string firmN);
	void SearchByOwner(string owner);
	void SearchBySpheere(string spheere);
	void SearchByTel(string tel);	

	void WriteToFile();
	void ReadFromFile(ifstream & rdFile);	
};

class ListEmptyExept
{
public:
	ListEmptyExept() { }

	void messageListEmptyExept()
	{
		cout << "List is empty" << endl;
	}
};


class NoSearchingValExept
{	
	string data;
public:
	NoSearchingValExept(string data) : data(data) { }

	void messageNoSearchingValExept()
	{
		cout << "No firm by such data:  [" << data << "] in list" << endl;
	}
};


