#include <iostream>
#include <string>
#include "RefBook.h"
#include <fstream>
using namespace std;


ostream& operator << (ostream& os, const RefBook& obj)
{
	os.write((char*)&obj, sizeof(obj));	
	return os;
}


ifstream& operator >> (ifstream& is, RefBook& obj)
{
	obj.ReadFromFile(is);
	return is;
}

int main()
{

	RefBook rBook;
	rBook.AddToBegining("New Steel", "Dnipro", "Vasylchuk P.V.", "metalcraft", "000 123 3434");
	rBook.AddToEnd("Chairs", "Lviv", "Petrycyuk I.O.", "furniture", "000 222 6767");
	rBook.AddToEnd("Family meat", "Rivne", "Voloshyn A.A.", "nutritional", "000 678 0909");
	rBook.AddToEnd("New wears", "Rivne", "Malyuta A.Ya.", "textile", "000 455 4545");

	rBook.WriteToFile();

	try
	{
		rBook.Show();
	}
	catch(ListEmptyExept ex)
	{
		ex.messageListEmptyExept();
	}
	
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" << endl;
	try
	{
		rBook.SearchByFirmname("New wears");
		rBook.SearchByOwner("Voloshyn A.A.");		
		rBook.SearchByTel("000 123 3434");	
		rBook.SearchBySpheere("wrong spheere");
	}
	catch (NoSearchingValExept ex)
	{
		ex.messageNoSearchingValExept();
	}	
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" << endl;
	cout << "\tREAD FROM FILE" << endl;
	ifstream rdFile;
	rdFile.open("List.txt");
	
	RefBook rBook2;
	rdFile >> rBook2;
	rBook2.Show();	

	system("pause");
	return 0;
}