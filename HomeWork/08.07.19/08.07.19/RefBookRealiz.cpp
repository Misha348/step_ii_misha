#include <iostream>
#include <string>
#include "RefBook.h"
#include <fstream>
using namespace std;

RefBook::RefBook()
{
	head = nullptr;
	tail = nullptr;
	size = 0;
}

void RefBook::AddToBegining(string firmName, string city, string owner, string spheere, string phone)
{
	Firm *newFirm = new Firm;

	newFirm->firmName = firmName;
	newFirm->spheere = spheere;
	newFirm->city = city;
	newFirm->owner = owner;
	newFirm->phone = phone;
	newFirm->next = nullptr;

	if (head == nullptr)
		head = tail = newFirm;
	else
	{
		newFirm->next = head;
		head = newFirm;
	}
	size++;
}

void RefBook::AddToEnd(string firmName, string city, string owner, string spheere, string phone)
{
	Firm *newFirm = new Firm;

	newFirm->firmName = firmName;
	newFirm->spheere = spheere;
	newFirm->city = city;
	newFirm->owner = owner;
	newFirm->phone = phone;
	newFirm->next = nullptr;

	if (head == nullptr)
		head = tail = newFirm;
	else
	{
		tail->next = newFirm;
		tail = newFirm;
	}
	size++;
}

RefBook::~RefBook()
{
	DelAll();
}

void RefBook::DelAll()
{
	while (head != nullptr)
		DelHead();
}

void RefBook::DelHead()
{
	if (!IsEmpty())
	{
		Firm *tmp = head;
		head = head->next;
		delete tmp;
	}
	size--;
}

bool RefBook::IsEmpty() const
{
	return head == nullptr;
}

void RefBook::Show() const
{
	if (IsEmpty())
	{		
		throw ListEmptyExept();
	}
	Firm *current = head;
	while (current != nullptr)
	{
		cout << "firm: " << current->firmName << endl;
		cout << "spheere: " << current->spheere << endl;
		cout << "city: " << current->city << endl;
		cout << "owner: " << current->owner << endl;
		cout << "phone: " << current->phone << endl;
		cout << "=====================\n" << endl;
		current = current->next;
	}	
}

//enum FindType { FIRMNAME, OWNER, SPHEERE, TEL };
//template <typename T>
//void RefBook::FindBy(T value, FindType type)
//{
//	bool hadFound = false;
//	Firm *searchEl = head;
//	for (int i = 1; i != size + 1; i++)
//	{
//		T searchElValue;
//		switch (type)
//		{
//		case FIRMNAME:
//			searchElValue = searchEl->firmName;
//			break;
//		case OWNER:
//			searchElValue = searchEl->owner;
//			break;
//		case SPHEERE:
//			searchElValue = searchEl->spheere;
//			break;
//		case TEL:
//			searchElValue = searchEl->phone;
//			break;
//		}
//		if (searchElValue == value)
//		{
//			cout << "\t[firm exist]" << endl;
//			searchEl->Show();
//			cout << "\n";
//			hadFound = true;
//		}
//		searchEl = searchEl->next;
//	}
//	if (hadFound == false)
//		throw NoSearchingValExept<T>(value);
//}

void RefBook::SearchByFirmname(string firmN)
{
	bool hadFound = false;
	Firm *searchEl = head;
	for (int i = 1; i != size + 1; i++)
	{
		if (searchEl->firmName == firmN)
		{
			cout << "\t[firm exist]" << endl;
			searchEl->Show();
			cout << "\n";
			hadFound = true;
		}
		searchEl = searchEl->next;
	}
	if(hadFound == false)
	throw NoSearchingValExept(firmN);
}

void RefBook::SearchByOwner(string owner) 
{
	bool hadFound = false;
	Firm *searchEl = head;
	for (int i = 1; i != size + 1; i++)
	{
		if (searchEl->owner == owner)
		{
			cout << "\t[firm exist]" << endl;
			searchEl->Show();
			cout << "\n";
			hadFound = true;
		}
		searchEl = searchEl->next;
	}
	if (hadFound == false)
		throw NoSearchingValExept(owner);
}

void RefBook::SearchBySpheere(string spheere)
{
	bool hadFound = false;
	Firm *searchEl = head;
	for (int i = 1; i != size + 1; i++)
	{
		if (searchEl->spheere == spheere)
		{
			cout << "\t[firm exist]" << endl;
			searchEl->Show();
			cout << "\n";
			hadFound = true;
		}
		searchEl = searchEl->next;
	}
	if (hadFound == false)
		throw NoSearchingValExept(spheere);
}

void RefBook::SearchByTel(string tel)
{
	bool hadFound = false;
	Firm *searchEl = head;
	for (int i = 1; i != size + 1; i++)
	{
		if (searchEl->phone == tel)
		{
			cout << "\t[firm exist]" << endl;
			searchEl->Show();
			cout << "\n";
			hadFound = true;
		}
		searchEl = searchEl->next;
	}
	if (hadFound == false)
		throw NoSearchingValExept(tel);
}

void RefBook::WriteToFile()
{
	ofstream wrFile("List.txt", ios_base::out);
	if (wrFile)
	{
		Firm *tmpFirmRefBook = head;
		while (tmpFirmRefBook != nullptr)
		{
			wrFile << tmpFirmRefBook->firmName << endl
				   << tmpFirmRefBook->spheere << endl
				   << tmpFirmRefBook->city << endl
				   << tmpFirmRefBook->owner << endl
				   << tmpFirmRefBook->phone << endl;
			tmpFirmRefBook = tmpFirmRefBook->next;
		}
		wrFile.close();
	}
}

void RefBook::ReadFromFile(ifstream& rdFile)
{
	if (!rdFile.is_open())
	{
		cout << "can't open file.";
	}
	else
	{
		Firm *tmpFirm = new Firm;
		while (!rdFile.eof())
		{
			getline(rdFile, tmpFirm->firmName);
			getline(rdFile, tmpFirm->spheere);
			getline(rdFile, tmpFirm->city);
			getline(rdFile, tmpFirm->owner);
			getline(rdFile, tmpFirm->phone);			
			this->AddToEnd(tmpFirm->firmName, tmpFirm->spheere, tmpFirm->city, tmpFirm->owner, tmpFirm->phone);
		}		
	}
	
}

	
