#pragma once
#include <iostream>
#include <string>
#include "Animal.h"
using namespace std;

class Fish: public Animal
{
protected:
	string specious;
	

public:
	Fish(string type, int speed, float weight, string enviroment, string specious) :
		Animal(type, speed, weight, enviroment), specious(specious) { }

	void MakeTone();
	void abilAliveUnderwater();
	void Show();
};

class Carp : public Fish
{
private:
	string name;

public:
	Carp(string type, int speed, float weight, string enviroment, string specious, string name) :
		Fish(type, speed, weight, enviroment, specious), name(name) { }

	string GetFishName();
	void abilAliveUnderwater();	
	void Show();

};