#include <iostream>
#include <string>
#include "Animal.h"
#include "Bird.h"
#include "Reptile.h"
#include "Fish.h"


using namespace std;

void Animal::MakeTone()
{
	cout << "abstract animal produse some tone" << endl;
}

void Animal::MoveForm()
{
	cout << "abstract animal move in some specified method" << endl;
}

void Animal::Show()
{
	cout << "impossible to show data about abstract animal" << endl;
}

void Bird::MakeTone()
{
	cout << "abstract bird produse bird's specified tone";
}

void  Bird::MoveForm()
{
	cout << type << " moves with help of wriggling wings";
}

void  Bird::Show()
{
	cout << "impossible to show data about abstract bird" << endl;
}

void  Bird::AbilityToFly()
{
	cout << "ability to fly provides by wings" << endl;
}

void Hawk::MakeTone()
{
	cout << "Hawk produse whistling tone";
}

void Hawk::MoveForm()
{
	Bird::MoveForm();
}

void  Hawk::Show()
{
	cout << "type - " << type << endl;
	cout << "name - " << name << endl;
	if (IsPredator)
		cout << "predator" << endl;
	else
		cout << "herbivore" << endl;

	cout << "wing scope - " << wingScope << endl;
	cout << "weight - " << weight << endl;
	cout << "speed - " << speed << endl;
	if (emigratory)
		cout << "emigratory" << endl;
	else
		cout << "not emigratory" << endl;
	cout << "enviroment - " << enviroment << endl;
}

void Reptile::MakeTone()
{
	cout << "abstract reptile produse reptil's specified tone" << endl;
}

void Reptile::MoveForm()
{
	cout << "moves mainly within majority bends of body" << endl;
}

void Reptile::AbilityToSting()
{
	cout << "cause of " << virulence << " may to sting" << endl;
}

void Reptile::Show()
{
	cout << "impossible to show data about abstract reptile" << endl;
}

string ComodosVaran::GetReptName()
{
	return name;
}

void ComodosVaran::MakeTone()
{
	cout << "\n" << GetReptName() << "produse  specified tone like (kroh - kroh) " << endl;
}

void ComodosVaran::MoveForm()
{
	cout << GetReptName() << "moves mainly within majority bends of body" << endl;
}

void ComodosVaran::AbilityToSting()
{
	cout << "cause of virulence may to sting" << endl;
}

void ComodosVaran::Show()
{
	cout << "type - " << type << endl;
	cout << "name - " << name << endl;
	cout << "size - " << size << endl;	
	cout << "weight - " << weight << endl;
	cout << "speed - " << speed << endl;
	cout << "enviroment - " << enviroment << endl;
	cout << "scin type - " << scinType << endl;
	cout << "blood-type - " << bloodType << endl;
	cout << "virulence - ";
	if (virulence)
		cout << "yes" << endl;
	else
		cout << "no" << endl;	
}

void Fish::MakeTone()
{
	cout << "\ndoesn't provide any tone" << endl;
}

void Fish::abilAliveUnderwater()
{
	cout << "any fish has ability to alive under the water" << endl;
}

void Fish::Show()
{
	cout << "impossible to show data of abstract fish";
}

void Carp::abilAliveUnderwater()
{
	Fish::abilAliveUnderwater();
}

string Carp::GetFishName()
{
	return name;
}

void Carp::Show()
{
	cout << "type - " << type << endl;
	cout << "name - " << name << endl;
	cout << "weight - " << weight << endl;
	cout << "speed - " << speed << endl;
	cout << "enviroment - " << enviroment << endl;
	cout << "specious " << specious << endl;	
}
