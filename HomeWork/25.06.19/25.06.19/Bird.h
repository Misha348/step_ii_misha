#pragma once
#include <iostream>
#include <string>
#include "Animal.h"
using namespace std;

class Bird : public Animal
{
protected:
	bool emigratory;
	int wingScope;
	bool IsPredator;

public:
	Bird(string type, int speed, float weight, string enviroment, bool emigrat, int wingScope, bool IsPredator) :
		emigratory(emigrat), wingScope(wingScope), IsPredator(IsPredator), Animal(type, speed, weight, enviroment)
	{}

	void MakeTone();
	void MoveForm();
	void AbilityToFly();
	void Show();

};

class Hawk : public Bird
{
private:
	string name;
public:
	Hawk(string type, int speed, float weight, string enviroment, bool emigrat, int wingScope, bool IsPredator, string name) :
		name(name), Bird(type, speed, weight, enviroment, emigrat, wingScope, IsPredator)
	{}

	void MakeTone();
	void MoveForm();
	void  Show();

};
