#pragma once
#include <iostream>
#include <string>
#include "Animal.h"
using namespace std;

class Reptile: public Animal
{
protected:
	string scinType;
	string bloodType;
	bool virulence;

public:
	Reptile(string type, int speed, float weight, string enviroment, string scinType, string bloodType, bool virulence) :
		Animal(type, speed, weight, enviroment), scinType(scinType), bloodType(bloodType), virulence(virulence) {}

	void MakeTone();
	void MoveForm();
	void AbilityToSting();
	void Show();
};

class ComodosVaran: public Reptile
{
private:
	int size;
	string name;

public:
	ComodosVaran(string type, int speed, float weight, string enviroment, string scinType, string bloodType, bool virulence, int size, string name) :
		Reptile(type, speed, weight, enviroment, scinType, bloodType, virulence), size(size), name(name) {}

	string GetReptName();
	void MakeTone();
	void MoveForm();
	void AbilityToSting();
	void Show();
};