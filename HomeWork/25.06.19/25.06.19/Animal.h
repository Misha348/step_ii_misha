#pragma once
#include <iostream>
#include <string>
using namespace std;

class Animal
{
protected:
	string type;
	int speed;
	float weight;
	string enviroment;

public:
	Animal(string type, int speed, float weight, string enviroment) :
		type(type), speed(speed), weight(weight), enviroment(enviroment)
	{}

	void MakeTone();
	void MoveForm();
	void Show();	
};

