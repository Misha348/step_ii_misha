#include <iostream>
#include <string>
#include "Animal.h"
#include "Bird.h"
#include "Reptile.h"
#include "Fish.h"
using namespace std;

int main()
{
	Hawk hawk("bird",70, 7, "air", false, 80, true, "hawk");
	hawk.Show();
	cout << endl;
	hawk.MakeTone();
	cout << endl;
	hawk.MoveForm();
	cout << endl;
	hawk.AbilityToFly();
	cout << "===============================================\n";

	ComodosVaran varan("reptile", 50, 120, "land", "squama", "cold", true, 2, "comodos varan");
	varan.Show();
	varan.MakeTone();
	varan.MoveForm();
	varan.AbilityToSting();
	cout << "===============================================\n";

	Carp carp("fish", 40, 4.5, "water", "limnetic", "carp");
	carp.Show();
	carp.MakeTone();
	carp.abilAliveUnderwater();

	system("pause");
	return 0;
}

