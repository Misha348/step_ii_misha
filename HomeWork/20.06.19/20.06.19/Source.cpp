#include "DB.h"
#include <iostream>
#include <string>
using namespace std;


//			IT WAS SO LONG-SUFFERING TASK )))


int main()
{
	DB db;
	db.Add("2345AO", "violation 1");
	db.Add("2345AO", "violation 2");
	db.Add("2345AO", "violation 3");
	db.Add("2020XM", "violation 1");
	db.Add("7345TM", "violation 1");
	db.Add("7345TM", "violation 2");
	db.Add("1955RV", "violation 1");
	db.Add("1455ZP", "violation 1");
	db.Add("2876KR", "violation 1");
	db.Add("5631MI", "violation 1");
	db.Add("5631MI", "violation 2");
	
	cout << "  [All car data]\n" << endl;
	db.PrintSort();
	cout << "----------------------\n";

	cout << "  [Data by car number]\n" << endl;
	db.FindLine("7345TM");
	cout << "----------------------\n";

	cout << "  [Data by car diapason]\n" << endl;
	db.DiapasonData("1500", "3000");
	system("pause");
	return 0;
}