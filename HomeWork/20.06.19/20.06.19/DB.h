#pragma once
#include <iostream>
#include <string>
using namespace std;

class DB
{
private:
	class Node
	{
	public:
		string *violation;
		int size;
		string cNumb;
		Node *left;
		Node *right;

		Node()
		{
			violation = nullptr;
			size = 0;
			cNumb = "";
			left = nullptr;
			right = nullptr;
		}

		Node(string violation, string cNumb, int size = 0, Node* left = nullptr, Node* right = nullptr)
			: cNumb(cNumb), left(left), right(right)
		{
			//violation = violation;
			/*size = 0;*/
			AddViolation(violation);
		}

		void AddViolation(string v)
		{
			if (violation == nullptr)
			{
				violation = new string[size + 1];
				for (int i = 0; i < size; i++)
				{
					violation[i] = v;
				}
				size++;
			}
			else
			{
				string *tmpViol = new string[size + 1];
				for (int i = 0; i < size; i++)
				{
					tmpViol[i] = violation[i];
				}
				tmpViol[size] = v;
				delete[] violation;
				violation = tmpViol;
				size++;
			}

		}

		void ShowViolation() const
		{
			for (int i = 0; i < size; i++)
			{
				cout << "     " << violation[i] << endl;
			}
		}
	};

	Node *root;

	void Add(string cNumb, string viol, Node *&node)
	{
		if (node == nullptr)
		{
			node = new Node(viol, cNumb);
		}

		if (node->cNumb == cNumb)
		{
			node->AddViolation(viol);
		}
		else if (cNumb > node->cNumb)
			Add(cNumb, viol, node->right);
		else
			Add(cNumb, viol, node->left);
	}

	void PrintPKL(const Node* node) const
	{
		if (node != nullptr)
		{
			PrintPKL(node->right);
			cout << "car: " << node->cNumb << " " << node->violation << endl;
			PrintPKL(node->left);
		}
	}
	
	void PrintLKP(const Node* node) const
	{
		if (node != nullptr)
		{
			PrintLKP(node->left);			
			cout << "car: " << node->cNumb << " "; node->ShowViolation(); cout << "\n";
			PrintLKP(node->right);
		}
	}

	bool Find(string cNumb, const Node* node) const
	{
		if (node == nullptr) return false;

		if (node->cNumb == cNumb)
			return true;
		else if (cNumb > node->cNumb)
			return Find(cNumb, node->right);
		else
			return Find(cNumb, node->left);
	}

public:
	DB()
	{
		root = nullptr;
	}

	void Add(string cNumb, string viol)
	{
		Add(cNumb, viol, root);
	}

	void PrintPKL() const
	{
		PrintPKL(root);
	}

	void PrintLKP() const
	{
		PrintLKP(root);
	}


	void PrintSort(bool desc = false)
	{
		desc ? PrintPKL() : PrintLKP();
	}

	bool Find(string cNumb)
	{
		return Find(cNumb, root);
	}

	void FindLine(string cNumb) const
	{
		for (Node * n = root; n != nullptr;)
		{
			if (n->cNumb == cNumb)
			{
				cout << "car: " << n->cNumb << " "; n->ShowViolation();
				break;								//n->violation;
			}
			else if (cNumb > n->cNumb)
				n = n->right;
			else
				n = n->left;
		}
	}

	void DiapasonData1(Node *node, string min, string max)
	{
		if (node == nullptr)
			return;

		if (node->cNumb > min && node->cNumb < max)
		{
			DiapasonData1(node->left, min, max);
			{
				cout << "car: " << node->cNumb << " "; node->ShowViolation();  cout << "\n";
			}
		}
		//if (node->cNumb < max)
		//{
		//	//DiapasonData1(node->right, min, max);
		//}
	}

	void DiapasonData(string min, string max)
	{
		DiapasonData1(root, min, max);
	}
};





//class DB
//{
//private:
//	class Node
//	{
//	public:
//		string *violation;
//		int size;
//		int cNumb;
//		Node *left;
//		Node *right;
//
//		Node()
//		{
//			violation = nullptr;
//			/*size = 0;*/
//		}
//
//		Node(int cNumb, string violation, Node* left = nullptr, Node* right = nullptr)
//			: cNumb(cNumb), left(left), right(right)
//		{ 			
//			//violation = violation;
//			size = 0;
//			AddViolation(violation);
//			//cout << "size " << size << endl;
//		}
//
//		void AddViolation(string v)
//		{
//			if (violation == nullptr)
//			{
//				size = 1;
//				violation = new string(v);
//			}			
//			else 
//			{
//				cout << "size " << size << endl;
//				string *tmpViol = new string[size + 1];
//				for (int i = 0; i < size; i++)
//				{
//					tmpViol[i] = violation[i];
//				}
//				tmpViol[size] = v;
//				//cout << "size " << size << endl;
//				//delete[] violation;
//				violation = tmpViol;
//				++size;
//			}
//		}
//
//		void ShowViolation() const
//		{
//			for (int i = 0; i < size; i++)
//			{
//				cout << violation[i] << endl;
//			}
//		}
//	};
//
//	Node *root;
//	
//	void Add(int cNumb, string viol, Node *&node)
//	{
//		if (node == nullptr)
//		{
//			//TEST
//			node = new Node(cNumb, viol);
//		}			
//
//		else if (node->cNumb == cNumb)
//		{
//
//			node->AddViolation(viol);
//		}
//		else if (cNumb > node->cNumb)
//			Add(cNumb, viol,  node->right);
//		else
//			Add(cNumb, viol, node->left);
//	}
//
//	void PrintPKL(const Node* node) const
//	{
//		if (node != nullptr)
//		{
//			PrintPKL(node->right);
//			cout << node->cNumb << " - " << node->violation << endl;
//			PrintPKL(node->left);
//		}
//	}
//
//	void PrintLKP(const Node* node) const
//	{
//		if (node != nullptr)
//		{
//			PrintLKP(node->left);
//			cout << node->cNumb;
//			node->ShowViolation();
//			PrintLKP(node->right);
//		}
//	}
//
//	bool Find(int cNumb, const Node* node) const
//	{
//		if (node == nullptr) return false;
//
//		if (node->cNumb == cNumb)
//			return true;
//		else if (cNumb > node->cNumb)
//			return Find(cNumb, node->right);
//		else
//			return Find(cNumb, node->left);
//	}
//
//public:
//	DB()
//	{
//		root = nullptr;
//	}
//
//	void Add(int cNumb, string viol)
//	{
//		//TEST
//		Add(cNumb, viol, root);
//	}
//
//	void PrintPKL() const
//	{
//		PrintPKL(root);
//	}
//
//	void PrintLKP() const
//	{
//		PrintLKP(root);
//	}
//
//
//	void PrintSort(bool desc = false)
//	{
//		desc ? PrintPKL() : PrintLKP();
//	}
//
//	bool Find(int cNumb)
//	{
//		return Find(cNumb, root);
//	}
//	
//	void FindLine(int cNumb) const
//	{
//		for (Node * n = root; n != nullptr;)
//		{
//			if (n->cNumb == cNumb)
//			{
//				cout << n->violation << " - " << n->violation << endl;
//				break;
//			}
//			else if (cNumb > n->cNumb)
//				n = n->right;
//			else
//				n = n->left;
//		}
//		
//	}
//
//};

