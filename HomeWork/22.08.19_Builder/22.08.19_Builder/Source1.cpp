#include <iostream>
#include <string>
using namespace std;

class Automobile
{
public:
	string type;
	string automobileBody;
	string engine;
	string wheels;
	string bodySuffusion;
	string salonDetails;

	Automobile() {}
	Automobile(string automobileType) : type(automobileType) {}		

	void ShowAutomobile()
	{
		cout << "type - " << type << endl;
		cout << "automobileBody - " << automobileBody << endl;
		cout << "engine - " << engine << endl;
		cout << "wheels - " << wheels << endl;
		cout << "bodySuffusion - " << bodySuffusion << endl;
		cout << "salonDetails - " << salonDetails << endl;
	}
};

// automob builder
class AutomobileModelTechnology
{
protected:
	Automobile *automobile;
public:
	AutomobileModelTechnology() {}


	void ProduceNewautomobile()
	{
		automobile = new Automobile();
	}

	Automobile *ReceiveAutomobile() const
	{
		return automobile;
	}

	virtual void SetType() = 0;
	virtual void SetAutomobileBody() = 0;
	virtual void SetEngeene() = 0;
	virtual void SetWheels() = 0;
	virtual void MakeBodySuffusion() = 0;
	virtual void SetSalonDetails() = 0;	
};


class SportAutomobileTech: public AutomobileModelTechnology
{
	void SetType()
	{
		automobile->type = "SPORT";
	}

	void SetAutomobileBody()
	{
		automobile->automobileBody = "(type sport)";
	}

	void SetEngeene()
	{
		automobile->engine = "(type sport)";
	}

	void SetWheels()
	{
		automobile->wheels = "(type sport)";
	}

	void MakeBodySuffusion()
	{
		automobile->bodySuffusion = " (black)";
	}

	void SetSalonDetails()
	{
		automobile->salonDetails = "(type sport)";
	}
};

class MiniAutomobileTechnology : public AutomobileModelTechnology
{
public:
	void SetType()
	{
		automobile->type = "MINI";
	}

	void SetAutomobileBody()
	{
		automobile->automobileBody = "(type mini)";
	}

	void SetEngeene()
	{
		automobile->engine = "(type mini)";
	}

	void SetWheels()
	{
		automobile->wheels = "(type mini)";
	}

	void MakeBodySuffusion()
	{
		automobile->bodySuffusion = "(yellow)";
	}

	void SetSalonDetails()
	{
		automobile->salonDetails = "(type mini)";
	}
};

class OutlanderAutomobileTechnology: public AutomobileModelTechnology
{
public:
	void SetType()
	{
		automobile->type = "OUTLANDER";
	}

	void SetAutomobileBody()
	{
		automobile->automobileBody = "(type OUTLANDER)";
	}

	void SetEngeene()
	{
		automobile->engine = "(type OUTLANDER)";
	}

	void SetWheels()
	{
		automobile->wheels = "(type OUTLANDER)";
	}

	void MakeBodySuffusion()
	{
		automobile->bodySuffusion = "(white)";
	}

	void SetSalonDetails()
	{
		automobile->salonDetails = "(type OUTLANDER)";
	}
};

//director
class Conveyer
{
	AutomobileModelTechnology *builder;
public:
	void SetAutomobile(AutomobileModelTechnology *autoMobTechnol)
	{
		builder = autoMobTechnol;
	}	

	Automobile* ReceiveAutomobile()
	{
		builder->ProduceNewautomobile();
		builder->SetType();
		builder->SetAutomobileBody();
		builder->SetEngeene();
		builder->SetWheels();
		builder->SetSalonDetails();
		builder->MakeBodySuffusion();		
		return builder->ReceiveAutomobile();
	}
};

int main()
{

	Conveyer conver;

	SportAutomobileTech *sportTechnology = new SportAutomobileTech;
	conver.SetAutomobile(sportTechnology);
	auto sport = conver.ReceiveAutomobile();
	sport->ShowAutomobile();
	cout << "=========================" << endl;

	MiniAutomobileTechnology *miniTechnology = new MiniAutomobileTechnology;
	conver.SetAutomobile(miniTechnology);
	auto mini = conver.ReceiveAutomobile();
	mini->ShowAutomobile();
	cout << "=========================" << endl;

	OutlanderAutomobileTechnology *outlanderTechnology = new OutlanderAutomobileTechnology;
	conver.SetAutomobile(outlanderTechnology);
	auto outlander = conver.ReceiveAutomobile();
	outlander->ShowAutomobile();










	system("pause");
	return 0;
}