#pragma once
#include <iostream>
using namespace std;

class Plane
{
private:
	string type;
	string model;
	int pasOnBort;
	int maxPasQuan;

	friend ostream& operator << (ostream& out, const Plane& pl);
	friend bool operator==(const Plane& left, const Plane right);
	friend bool operator>(const Plane& left, const Plane right);	

public:
	Plane();
	Plane(string type, string  mod, int pasQ, int pasMax);
	
	void operator()(int pas);
	Plane& operator++();
	Plane& operator--();

	~Plane();



};

