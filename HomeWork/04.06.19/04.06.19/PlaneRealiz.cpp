#include <iostream>
using namespace std;
#include "Plane.h"

Plane::Plane()
{
	type = "non";
	model = "non";
	pasOnBort = 0;
	maxPasQuan = 0;
}

Plane::Plane(string type, string mod, int pasQ, int pasMax)
{
	this->type = type;
	this->model = mod;
	this->pasOnBort = pasQ;
	this->maxPasQuan = pasMax;
}

void Plane::operator()(int pas)
{
	pasOnBort += pas;
	if (pasOnBort > maxPasQuan)
	{
		cout << "to many. max - " << maxPasQuan << endl;
		pasOnBort = maxPasQuan;
	}
	else
		cout << "now on bort - " << pasOnBort << "pas " << endl;
}

Plane& Plane:: operator++()
{
	++pasOnBort;
	if (pasOnBort > maxPasQuan)
	{
		cout << "immpossible to add 1 pas. - no free places" << endl;
		pasOnBort = maxPasQuan;
	}	
	return *this;
}

Plane& Plane::operator--()
{
	--pasOnBort;
	if (pasOnBort < 0)
	{
		cout << "there's allready no pas ";
		pasOnBort = 0;
	}
		
	return *this;
}

Plane::~Plane(){}