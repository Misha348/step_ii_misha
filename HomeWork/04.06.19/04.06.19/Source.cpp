#include <iostream>
#include <string>
#include "Plane.h"
using namespace std;

ostream& operator << (ostream& out, const Plane& pl)
{
	out << "type:\t" << pl.type << "\n"
		<< "model:\t" << pl.model << "\n"
		<< "pas. quant: " << pl.pasOnBort << "\n"
		<< "max pas:  " << pl.maxPasQuan << "\n";
	return out;
}

bool operator==(const Plane& left, const Plane right)
{
	return left.type == right.type;
}

bool operator>(const Plane& left, const Plane right)
{
	return left.maxPasQuan > right.maxPasQuan;
}




int main()
{
	Plane pln1 = Plane("passenger", "boing 777", 283, 350);
	Plane pln2 = Plane("passenger", "boing 750", 296, 370);
	cout << pln1 << "===============\n" << pln2 << "\n";
	cout << "=================================" << endl;

	if (pln1 == pln2)
		cout << "planes types - equal" << endl;
	else
		cout << "planes types - not equal";
	cout << "=================================" << endl;

	if (pln1 > pln2)
		cout << "boing 777 has more pas places" << endl;
	else
		cout << "boing 750 has more pas places" << endl;
	cout << "=================================" << endl;

	pln1(70);
	cout << "=================================" << endl;

	++pln1;
	cout << pln1 << endl;
	cout << "=================================" << endl;

	--pln1;
	cout << pln1 << endl;
	cout << "=================================" << endl;

	system("pause");
	return 0;
}

