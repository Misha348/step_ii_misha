#pragma once
#include <iostream>
using namespace std;

class OutOfRange
{
	int pos;

public:
	OutOfRange(int pos) : pos(pos) { }
	void Message()
	{
		cout << "your possition is out of list range" << endl;
	}

};

template<typename T>
class List
{
private:
	
	struct Element
	{
		int numb;
		Element * next;
		Element * prev;
	};
	Element * head;
	Element * tail;

	int counter;
	int size;

public:
	List();
	List(const List<T> &obj);
	List(List<T> &&obj);
	~List();


	bool IsEmpty() const;
	
	void AddHead(T val);
	void AddTail(T val);
	void Add(T pos, T val);
	void DeleteHead();
	void DeleteTail();
	void DeletePos(T pos);
	void DelAll();

	List <T> &operator=(const List<T> &obj);
	List <T> &operator=(List<T> &&obj);
	List <T> operator+(const List<T> &L);
	List <T> operator-();

	void ShowList() const;
	
	void SortDown();
	void SortUp();
	List<T> SortList(bool dest);
};

template<typename T>
void List<T>::SortUp()
{
	Element * min;
	for (Element * i = head; i != nullptr; i = i->next)
	{
		min = i;
		for (Element * j = i->next; j != nullptr; j = j->next)
		{
			if (j->numb < min->numb)
			{
				min = j;
			}
		}
		int tmp = i->numb;
		i->numb = min->numb;
		min->numb = tmp;
	}
}

template<typename T>
void List<T>::SortDown()
{
	Element * min;
	for (Element * i = head; i != nullptr; i = i->next)
	{
		min = i;
		for (Element * j = i->next; j != nullptr; j = j->next)
		{
			if (j->numb > min->numb)
			{
				min = j;
			}
		}
		int tmp = i->numb;
		i->numb = min->numb;
		min->numb = tmp;
	}
}

template<typename T>
List<T> List<T>::SortList(bool dest)
{
	List newList = *this;
	dest ? newList.SortUp() : newList.SortDown();
	return newList;
}

template<typename T>
List <T> & List<T>::operator=(const List &obj)
{
	if (this == &obj)
		return *this;
	this->~List();
	Element *temp = obj.head;
	
	while (temp != nullptr)
	{
		AddTail(temp->numb);
		temp = temp->next;
	}
	return *this;
}

template<typename T>
List<T> & List<T>::operator=(List<T> &&obj)
{
	if (this == &obj)
		return *this;
	if (obj.head == nullptr)
	{
		this->head = tail = nullptr;
		size = 0;
	}
	else
	{
		this->size = obj.size;
		this->head = obj.head;
		this->tail = obj.tail;
		
		obj.head = tail = nullptr;
		obj.size = 0;
	}
	return *this;
}

template<typename T>
List <T> List<T>::operator+(const List<T> &L)
{
	List tmpList = *this;
	Element * tmp = L.head;
	while (tmp != nullptr)
	{
		tmpList.AddTail(tmp->numb);
		tmp = tmp->next;
	}
	return tmpList;
}
template<typename T>
List <T> List<T>::operator-()
{
	List result;
	Element * tmp = head;
	while (tmp != nullptr)
	{
		result.AddHead(tmp->numb);
		tmp = tmp->next;
	}
	return result;
}

template<typename T>
bool List<T>::IsEmpty() const
{
	return size == 0;
}

template<typename T>
List<T>::List()
{
	head = tail = nullptr;
	size = 0;
	counter = 0;
}

template<typename T>
List<T>::List(const List <T> &obj)
{
	head = tail = nullptr;
	size = 0;
	Element *tmp = obj.head;
	while (tmp != nullptr)
	{
		AddTail(tmp->numb);
		tmp = tmp->next;
	}
}

template<typename T>
List<T>::List(List<T> &&obj)
{
	this->size = obj.size;
	this->head = obj.head;
	this->tail = obj.tail;
	obj.size = 0;
	obj.head = nullptr;
	obj.tail = nullptr;
}

template<typename T>
List<T>::~List()
{
	DelAll();
}

template<typename T>
void List<T>::DelAll()
{
	while (head)
	{
		tail = head->next;
		delete head;
		head = tail;
	}
	size = 0;
}

template<typename T>
void  List<T>::AddHead(T val)
{
	Element *newElement = new Element;
	newElement->numb = val;
	newElement->prev = nullptr;
	newElement->next = nullptr;

	if (head == nullptr)
		head = tail = newElement;
	else
	{
		newElement->next = head;
		head->prev = newElement;
		head = newElement;
	}
	++size;
}

template<typename T>
void List<T>::AddTail(T val)
{
	Element * newElement = new Element;
	newElement->numb = val;
	newElement->next = nullptr;
	newElement->prev = nullptr;

	if (head == nullptr)
		head = tail = newElement; 
	else
	{
		tail->next = newElement; 
		newElement->prev = tail; 
		tail = newElement; 
	}
	++size;
}

template<typename T>
void List<T>::Add(T pos, T val)
{
	if (pos > size + 1 || pos <= 0)
		return;

	if (pos == 1)
	{
		AddHead(val);
		return;
	}
	if (pos == size + 1)
	{
		AddTail(val);
		return;
	}

	Element * newElem = new Element;
	newElem->numb = val;

	Element * temp = head;

	for (int p = 1; p != pos; ++p)
		temp = temp->next;

	newElem->next = temp;
	newElem->prev = temp->prev;
	temp->prev->next = newElem;
	temp->prev = newElem;
	++size;
}

template<typename T>
void List<T>::DeleteHead()
{
	if (!IsEmpty())
	{
		Element * temp = head;
		head = head->next;
		head->prev = nullptr;
		delete temp;

		if (head == nullptr)
			tail = nullptr;  
		--size;
	}
}

template<typename T>
void List<T>::DeleteTail()
{
	Element * delTail = tail;
	tail = tail->prev;
	delete delTail;

	if (tail == nullptr) 
		head = nullptr;
	else
		tail->next = nullptr;
	--size;
}

template<typename T>
void List<T>::DeletePos(T pos)
{
	if (pos > size || pos <= 0)
		return;

	if (pos == 1)
	{
		DeleteHead();
		return;
	}
	if (pos == size)
	{
		DeleteTail();
		return;
	}

	Element * delEl = head;

	for (int p = 1; p != pos; ++p) 
		delEl = delEl->next;

	delEl->prev->next = delEl->next; 
	delEl->next->prev = delEl->prev; 

	delete delEl;
	--size;
}

template<typename T>
void List<T>::ShowList() const
{
	if (IsEmpty())
	{
		cout << "List is empty!\n";
		return;
	}

	for (Element * item = head; item != nullptr; item = item->next)
	{
		cout << "Element: " << item->numb << endl;
	}
}
