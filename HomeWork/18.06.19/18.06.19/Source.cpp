#include "List.h"
#include <iostream>

using namespace std;

int main()
{
	List<int> list;

	cout << "  --ADD to BEGINING--\n";
	list.AddHead(5);	
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --ADD to END--\n";
	list.AddTail(7);
	list.AddTail(8);
	list.AddTail(3);
	list.AddTail(9);
	list.AddTail(10);
	list.AddTail(18);
	list.AddTail(-4);
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --ADD in POSITION--\n";
	list.Add(3, 4444);
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --DELETE HEAD--\n";
	list.DeleteHead();
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --DELETE TAIL--\n";
	list.DeleteTail();
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --DELETE from POSITION--\n";
	list.DeletePos(3);
	list.ShowList();
	cout << "\n====================\n";

	cout << "  --COPY LIST--\n";
	List<int> list1;
	list1 = list;
	list1.ShowList();
	cout << "\n====================\n";

	cout << "  -- LIST + LIST --\n";
	List<int> sumList;
	sumList = list1 + list;
	sumList.ShowList();
	cout << "\n====================\n";	

	cout << "  -- WRAPPED LIST (-LIST) --\n";
	List<int> wrapList;
	wrapList = -list;
	wrapList.ShowList();
	cout << "\n====================\n";	

	cout << "  -- SORTED LIST (UNCORRECT) --\n";
	bool dest = false;
	list.SortList(dest);
	list.ShowList();
	cout << "\n====================\n";

	cout << "  -- SORTED LIST (CORRECT) --\n";
	list = list.SortList(dest);
	list.ShowList();
	cout << "\n====================\n";

	list.DelAll();
	list.ShowList();
	cout << "\n====================\n";
	
	system("pause");
	return 0;
}

