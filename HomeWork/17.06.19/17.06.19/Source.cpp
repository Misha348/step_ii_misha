#include "List.h"
#include <iostream>
using namespace std;

int main()
{
	List list;
	
	cout << "  --ADD to BEGINING--\n";
	list.AddToBegining(100);
	list.Show();
	cout << "\n====================\n";

	cout << "  --ADD to END--\n";
	list.AddToEnd(3);
	list.AddToEnd(4);
	list.AddToEnd(555);
	list.AddToEnd(6);
	list.AddToEnd(7);
	list.Show();
	cout << "\n====================\n";
	
	cout << "  --DELETE HEAD--\n";
	list.DeleteHead();
	list.Show();
	cout << "\n====================\n";

	cout << "  --ADD in POSITION--\n";
	try
	{
		list.AddInPos(5555, 10);
	}
	catch(OutOfRangeExept exept)
	{
		exept.messageOutOfrange();
	}
	list.Show();
	cout << "\n====================\n";

	cout << "  --DELETE TAIL--\n";
	list.DelTail();
	list.Show();
	cout << "\n====================\n";

	cout << "  --DELETE from POSITION--\n";
	try
	{
		list.DelFromPos(73);
	}
	catch (OutOfRangeExept exept)
	{
		exept.messageOutOfrange();
	}	
	catch (ListEmptyExept exept)
	{
		exept.messageListEmptyExept();
	}
	list.Show();
	cout << "\n====================\n";

	cout << "  --SEARCH POSIT BY VALUE--\n";
	try
	{
		list.SearchEl(50);
	}
	catch (NoSearchingValExept exept)
	{
		exept.messageNoSearchingValExept();
	}
	catch (ListEmptyExept exept)
	{
		exept.messageListEmptyExept();
	}	
	cout << "\n====================\n";

	cout << "  --EXCHANGE VALUE--\n";
	list.Change(5555, 111);
	list.Show();
	cout << "\n====================\n";

	list.DelAll();
	try
	{
		list.Show();
	}
	catch (ListEmptyExept exept)
	{
		exept.messageListEmptyExept();
	}
	cout << "\n====================\n";
	system("pause");
	return 0;
}