#include "List.h"
#include <iostream>

using namespace std;

List::List()
{
	head = nullptr;
	tail = nullptr;
	size = 0;
}

List::~List()
{
	DelAll();
}


void List::AddToBegining(int num)
{
	Element * newElm = new Element;
	newElm->numb = num;
	newElm->next = nullptr;

	if (head == nullptr)
		head = tail = newElm;
	else
	{
		newElm->next = head;
		head = newElm;
	}
	size++;
}

void List::AddToEnd(int num)
{
	Element * newElm = new Element;
	newElm->numb = num;
	newElm->next = nullptr;

	if (head == nullptr)
		head = tail = newElm;
	else
	{
		tail->next = newElm;
		tail = newElm;
	}
	size++;
}

void List::AddInPos(int val, int pos)
{
	if (pos > size + 1 || pos <= 0)
	{
		//cout << "out of list bound" << endl;
		throw OutOfRangeExept(pos);
	}
		
	if (pos == 1)
	{
		AddToBegining(val);
		size++;
		return;
	}
	if (pos == size + 1)
	{
		AddToEnd(val);
		size++;
		return;
	}
	Element * newElem = new Element;
	newElem->numb = val;

	Element *tmp = head;

	for (int i = 1; i != pos - 1; i++)
	{
		tmp = tmp->next;
	}
	newElem->next = tmp->next;
	tmp->next = newElem;
	size++;
}

bool List::IsEmpty() const
{
	return head == nullptr;
}

void List::DeleteHead()
{
	if (!IsEmpty())
	{
		Element *tmp = head;
		head = head->next;
		delete tmp;
	}
	size--;
}

void List::DelTail()
{
	/*if (head)
	{
		Element *curEl = head;
		Element *prevEl = nullptr;

		while (curEl->next)
		{
			prevEl = curEl;
			curEl = curEl->next;
		}

		if (prevEl)
			prevEl->next = nullptr;
		delete curEl;
	}	*/
	if (head == nullptr)
		return;
	else if (head->next == nullptr)
	{
		Element *temp = head;
		delete temp;
		head = nullptr;
	}
	else
	{
		Element *one = head;
		Element *two = head->next;
		while (two->next != nullptr)
		{
			two = two->next;
			one = one->next;
		}
		one->next = nullptr;
		delete two;
	}
	size--;
}

void List::DelAll()
{
	while (head != nullptr)
		DeleteHead();
}

void List::DelFromPos(int pos)
{
	if (pos > size || pos <= 0)
	{
		//cout << "out of list bound" << endl;
		//return;
		throw OutOfRangeExept(pos);
	}
	if (pos == 1)
	{
		DeleteHead();
		return;
	}
	if (pos == size)
	{
		DelTail();
		return;
	}
	if (IsEmpty() == true)
		throw ListEmptyExept();

	Element *prevEl = head;

	for (int i = 1; i != pos - 1; i++)
	{		
		prevEl = prevEl->next;
	}

	Element *delEl = prevEl->next;
	prevEl->next = delEl->next;
	
	delete delEl;
	size--;
}

int List::SearchEl(int val)
{
	Element * searchlEl = head;

	for (int i = 1; i != size + 1; i++)
	{
		if (searchlEl->numb == val)
		{		
			cout << "your el. is by pos. " << i << endl;
			return i;
		}
		searchlEl = searchlEl->next;
	}
	//cout << "no such el" << endl;
	//return NULL;
	throw NoSearchingValExept(val);
}

int List::Change(int val, int newVal)
{
	Element * searchlEl = head;
	int k = 0;

	for (int i = 1; i != size + 1; i++)
	{
		if (searchlEl->numb == val)
		{
			searchlEl->numb = newVal;
			k++;			
		}
		searchlEl = searchlEl->next;
	}
	if (k > 0)
		return k;
	else
		return -1;
	
}

void List::Show() const
{
	if (IsEmpty())
	{
		/*cout << "List is empty!\n";
		return;*/
		throw ListEmptyExept();
	}
	Element *current = head;
	while (current != nullptr)
	{
		cout << "Element: " << current->numb << endl;
		current = current->next;
	}
}