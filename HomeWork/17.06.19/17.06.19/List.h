#pragma once
#include <iostream>
using namespace std;

struct  Element
{
	int numb;
	Element *next;
	
};

class List
{
private:
	Element *head;
	Element *tail;
	int size;

public:
	List();
	~List();
	
	void AddToBegining(int num);
	void AddToEnd(int num);
	void AddInPos(int val, int pos);

	void DelAll();
	void DeleteHead();
	void DelTail();
	void DelFromPos(int pos);

	int SearchEl(int val);
	int Change(int val, int newval);

	bool IsEmpty() const;	
	void Show() const;
};

class OutOfRangeExept
{
	int pos;
public:
	OutOfRangeExept(int pos) : pos(pos) { }

	void messageOutOfrange()
	{
		cout << "Possition: " << pos << " is out of range" << endl;
	}
	
};

class ListEmptyExept
{
public:
	ListEmptyExept() { }

	void messageListEmptyExept()
	{		
		cout << "List is empty" << endl;
	}
};

class NoSearchingValExept
{
	int el;
public:
	NoSearchingValExept(int el) : el(el) { }	

	void messageNoSearchingValExept()
	{
		cout << "there is no el. " << el << " in list" << endl;	
	}
};