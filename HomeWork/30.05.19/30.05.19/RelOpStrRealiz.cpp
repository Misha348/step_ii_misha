#include <iostream>
#include "RelOpStr.h"
using namespace std;

StringOfChars::StringOfChars()
{
	size = 0;
	char *str = nullptr;	
}


StringOfChars::StringOfChars(char* _str)
{
	size = strlen(_str);	
	str = new char[size + 1];
	for (int i = 0; i < size; i++)
	{
		str[i] = _str[i];
	}
	str[size] = '\0';
}

StringOfChars::StringOfChars(const StringOfChars &c_obj)
{
	size = c_obj.size;
	str = new char[size + 1];
	for (int i = 0; i < size; i++)
	{
		str[i] = c_obj.str[i];
	}
	str[size] = 0;	
}

StringOfChars StringOfChars::operator+(const StringOfChars &other_str) const
{	
	char tmpStr[100];	
	for (int i = 0; i < size; i++)
	{
		tmpStr[i] = str[i];
	}
	for (int i = size, j = 0; i < (size + other_str.size); i++, j++)
	{
		tmpStr[i] = other_str.str[j];
	}
	tmpStr[size + other_str.size] = 0;
	StringOfChars newStr(tmpStr);
	return newStr;
}

StringOfChars& StringOfChars::operator=(const StringOfChars &otherObj)
{
	
	if (str != nullptr) delete[] str;

	this->size = otherObj.size;	
	this->str = new char[otherObj.size + 1];

	for (int i = 0; i < otherObj.size; i++)
	{
		str[i] = otherObj.str[i];
	}
	str[size] = 0;	
	return *this;
}

// char * AddSymbol(char * str, char symbol)
// bool IsExist(char * str, char symbol)

StringOfChars StringOfChars::operator*(const StringOfChars &otherObj) const
{
	int k = 0;
	char tmpStr[30];
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < otherObj.size; j++)
		{
			if (str[i] == otherObj.str[j])
			{
				tmpStr[k] = str[i];
				++k;
			}
		}
	}
	tmpStr[k] = 0;
	//------------------------------------------
	int length = strlen(tmpStr);
	char* newTmp = new char[length+1];
	
	int c = 0;	
	int l = 0;

	for (int i = 0; i < length; i++)
	{
		int c = 0;
		for (int j = 0; j < length; j++)
		{
			if ((tmpStr[i]) != (newTmp[j]))
			{				
				c++;	
				if (c == length)
				{
					newTmp[l] = tmpStr[i];
					l++;
				}
			}
			if ((tmpStr[i]) == (newTmp[j]))
			{
				//continue;
			}
		}		
	}
	newTmp[l] = '\0';
	
	StringOfChars newStr(newTmp);
	delete[] newTmp;

	//char * newStr = nullptr;
	// ���� �� this.str
	// !IsExist(newStr.str, symbol)
	//		Add(newStr, symbol)

	// ���� �� other.str
	// !IsExist(newStr.str, symbol)
	//		Add(newStr, symbol)

	return newStr;	
}

void StringOfChars::Show()
{
	cout << "your string: " << str << endl;
}

void StringOfChars::ShowSimilarSbm()
{
	cout << "similar symbols: " << str << endl;
}

StringOfChars::~StringOfChars()
{
	delete[] str;
}

