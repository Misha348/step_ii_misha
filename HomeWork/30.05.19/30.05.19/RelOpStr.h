#pragma once
#include <iostream>
using namespace std;

class StringOfChars
{
private:

	char* str;
	int size;

public:

	StringOfChars();
	StringOfChars(char* _str);
	StringOfChars(const StringOfChars &c_obj);


	void Show();
	void ShowSimilarSbm();
	

	StringOfChars operator+(const StringOfChars &_str) const;
	StringOfChars& operator=(const StringOfChars &otherObj);
	StringOfChars operator*(const StringOfChars &otherObj) const;

	~StringOfChars();
};



