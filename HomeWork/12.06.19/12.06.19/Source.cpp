#include "Lohotron.h"
#include <ctime>
#include <iostream>
using namespace std;

void Show(Queue &q1, Queue &q2, Queue &q3)
{
	cout << "\n";
	for (int i = 0; i < q1.curSize; i++)
	{		
		cout << q1.data[i] << "\t" << q2.data[i] << "\t" << q3.data[i] << endl;
	}
	cout << "==================" << endl;
}

int Checkresult(Queue &q1, Queue &q2, Queue &q3, int &bal)
{	
	if (q1.data[0] == q2.data[0] && q1.data[0] == q3.data[0] && q2.data[0] == q3.data[0])
	{
		cout << "I line equal (+25)" << endl;
		bal += 25;
	}	
	else if (q1.data[1] == q2.data[1] && q1.data[1] == q3.data[1] && q2.data[1] == q3.data[1])
	{
		cout << "II line equal (+25)" << endl;
		bal += 25;
	}
		
	else if (q1.data[2] == q2.data[2] && q1.data[2] == q3.data[2] && q2.data[2] == q3.data[2])
	{
		cout << "III line equal (+25)" << endl;
		bal += 25;
	}
		
	else if ((q1.data[0] == q2.data[0] && q1.data[0] == q3.data[0] && q2.data[0] == q3.data[0]) && (q1.data[1] == q2.data[1] && q1.data[1] == q3.data[1] && q2.data[1] == q3.data[1]))
	{
		cout << "I + II (+50)";
		bal += 50;
	}
		
	else if ((q1.data[0] == q2.data[0] && q1.data[0] == q3.data[0] && q2.data[0] == q3.data[0]) && (q1.data[2] == q2.data[2] && q1.data[2] == q3.data[2] && q2.data[2] == q3.data[2]))
	{
		cout << "I + III (+50)";
		bal += 50;
	}		

	else if ((q1.data[1] == q2.data[1] && q1.data[1] == q3.data[1] && q2.data[1] == q3.data[1]) && (q1.data[2] == q2.data[2] && q1.data[2] == q3.data[2] && q2.data[2] == q3.data[2]))
	{
		cout << ("II + III (+50)");
		bal += 50;
	}		
	else
	{
		cout << "no any coincidences." << endl;
		bal -= 20;
	}
	return bal;
}
		

int main()
{
	srand(time(0));
		
	Queue q1(3);	
	for (int i = 0; i < 3; i++)
		q1.Add(rand() % 4);	
	
	Queue q2(3);
	for (int i = 0; i < 3; i++)
		q2.Add(rand() % 4);	
	
	Queue q3(3);
	for (int i = 0; i < 3; i++)
		q3.Add(rand() % 4);

	Show(q1, q2, q3);
	
	int choise = 0;
	bool exit = false;
	int balance = 100;

	while(true)
	{
		int roll1 = rand() % 3;
		int roll2 = rand() % 3;
		int roll3 = rand() % 3;
		
		cout << "Roll the drumm ?\t[ 1 ] - yes.\t[ 2 ] - no" << endl;
		cin >> choise;
		
		if(choise == 1)
		{
			q1.Roll(q1, roll1);
			q2.Roll(q2, roll2);
			q3.Roll(q3, roll3);	

			Show(q1, q2, q3);
			
			int res = Checkresult(q1, q2, q3, balance);
			cout << "BALANCE = " << res << endl;
		}	
		if (choise == 2)
		{
			exit = false;
			break;
		}
		if (balance < 20)
		{
			exit = false;
			break;
		}	
	}	

	system("pause");
	return 0;
}

