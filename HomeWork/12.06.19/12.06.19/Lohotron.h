#pragma once


class Queue
{
private:
	int *data;
	int maxSize;
	int curSize;
	friend void Show(Queue &q1, Queue &q2, Queue &q3);
	friend  int Checkresult(Queue &q1, Queue &q2, Queue &q3, int &bal);

public:
	Queue(int maxsize);
	~Queue();
	void Add(int el);
	int Extract();
	bool IsEmpty() const;
	bool IsFull() const;	
	void Roll(Queue &q, int roll);	
};

