#include "Lohotron.h"
#include <iostream>
using namespace std;

Queue::Queue(int maxSise)
{
	curSize = 0;
	this->maxSize = maxSise;
	data = new int[maxSise];
}

Queue::~Queue()
{
	delete[]data;
}

void Queue::Add(int el)
{
	if (!IsFull())
		data[curSize++] = el;
}

int Queue::Extract()
{
	if (!IsEmpty())
	{
		int first = data[0];
		for (int i = 0; i < curSize - 1; i++)
		{
			data[i] = data[i + 1]; //+
		}
		data[curSize - 1] = first; //-
		return first;
	}	
}

bool Queue::IsEmpty() const
{
	return curSize == 0;
}

bool Queue::IsFull() const
{
	return curSize == maxSize;
}

void Queue::Roll(Queue &q, int roll)
{
	while (roll > 0)
	{
		q.Extract();
		roll--;
	}	
}