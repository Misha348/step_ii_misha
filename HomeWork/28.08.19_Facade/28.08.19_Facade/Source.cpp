#include <iostream>
#include <string>
using namespace std;

class OutOfTimeAmountExeption
{
public:
	OutOfTimeAmountExeption() {}
	void OutOfTimeAmountExeptionMessage()
	{
		cout << "chosen incorrect time period. " << endl;
	}
};

class OutOfRoomQuantityExeption
{
public:
	OutOfRoomQuantityExeption() {}
	void OutOfRoomQuantityExeptionMessage()
	{
		cout << "chosen incorrect quantity of rooms. " << endl;
	}
};

class OutOfCategoryExeption
{
public:
	OutOfCategoryExeption() {}
	void OutOfcategoryExeptionMessage()
	{
		cout << "chosen incorrect category. " << endl;
	}
};

enum AmusementTimeDuration { THREEHOURS = 1, EVENING, NIGHT, HOLLIDAY};
enum DrinksCategories { BASIC = 1, EXPANDED, _FREE };
enum RestourantMealsCategoty { BREAKFEST = 1, LUNCH, DINNER, FREE };
enum MassageCategories { RELAX = 1, THAI, MEDICATIVE, POINT, CLASSIC };


class HotelRoomBooking
{
public:
	int RoomReserving(int roomsQuantity, AmusementTimeDuration amusDuration)
	{
		switch (roomsQuantity)
		{
		case 1:
			switch (amusDuration)
			{
			case THREEHOURS: return 500; break;
			case EVENING: return 1000; break;
			case NIGHT: return 1500; break;
			case HOLLIDAY: return 2000; break;
			default:
				throw OutOfTimeAmountExeption();
				break;
			}		
		case 2:
			switch (amusDuration)
			{
			case THREEHOURS: return 800; break;
			case EVENING: return 1500; break;
			case NIGHT: return 1800; break;
			case HOLLIDAY: return 2500; break;
			default:
				throw OutOfTimeAmountExeption();
				break;
			}
		default:
			throw OutOfCategoryExeption();
			break;
		}
	}
};

class Pool
{
public:
	int PoolUsing(AmusementTimeDuration amusDuration, float poolTime, int quantityOfpersons)
	{
		switch (amusDuration)
		{
		case THREEHOURS: return poolTime <= 3 ? poolTime * 100 * quantityOfpersons : throw OutOfTimeAmountExeption(); break;
		case EVENING: return poolTime <= 6 ? poolTime * 120 * quantityOfpersons : throw OutOfTimeAmountExeption(); break;
		case NIGHT: return poolTime <= 12 ? poolTime * 120 * quantityOfpersons : throw OutOfTimeAmountExeption(); break;
		case HOLLIDAY: return poolTime <= 48 ? poolTime * 80 * quantityOfpersons : throw OutOfTimeAmountExeption(); break;
		default:
			throw OutOfTimeAmountExeption();
			break;
		}
	}
};

class Bar
{
public:
	int ConsumingBarDrinks(AmusementTimeDuration amusDuration, DrinksCategories drinkCateg, int quantityOfpersons)
	{
		switch (drinkCateg)
		{		
		case BASIC:
			switch (amusDuration)
			{
			case THREEHOURS: return quantityOfpersons * 1000; break;
			case EVENING: return quantityOfpersons * 1500; break;
			case NIGHT: return quantityOfpersons * 2000; break;
			case HOLLIDAY: return quantityOfpersons * 4000; break;
			default:
				throw OutOfTimeAmountExeption();
				break;
			}
			break;
		case EXPANDED:
			switch (amusDuration)
			{
			case THREEHOURS: return quantityOfpersons * 1500; break;
			case EVENING: return quantityOfpersons * 2000; break;
			case NIGHT: return quantityOfpersons * 3000; break;
			case HOLLIDAY:return quantityOfpersons * 5000; break;
			default:
				throw OutOfTimeAmountExeption();
				break;
			}
			break;
		case _FREE:
			cout << "drinks - acording to menu prices (without discount)." << endl;
			break;
		default:
			throw OutOfCategoryExeption();
			break;
		}
	}
};

class Restourant
{
public:
	int ConsumingRestourantMeal(RestourantMealsCategoty mealCategor, AmusementTimeDuration amusDuration, int quantityOfpersons )
	{
		switch (amusDuration)
		{
		case THREEHOURS:
			switch (mealCategor)
			{
			case BREAKFEST: return 100 * quantityOfpersons;	break;
			case LUNCH: return 120 * quantityOfpersons;	break;
			case DINNER: return 140 * quantityOfpersons; break;
			case FREE:
				cout << "meals - acording to menu prices (without discount).\n" << endl;
			default:
				throw OutOfCategoryExeption();
				break;
			}
			break;
		case EVENING:
			switch (mealCategor)
			{			
			case DINNER:  return 140 * quantityOfpersons; break;
			case FREE:
				cout << "meals - acording to menu prices (without discount).\n" << endl; break;
			default:
				throw OutOfCategoryExeption();			
				break;
			}
			break;
		case NIGHT:
			switch (mealCategor)
			{			
			case FREE:
				cout << "meals - acording to menu prices (without discount).\n" << endl;
				break;
			default:
				throw OutOfCategoryExeption();
				break;
			}		
			break;
		case HOLLIDAY:
			switch (mealCategor)
			{
			case BREAKFEST: return 100 * quantityOfpersons; break;
			case LUNCH: return 120 * quantityOfpersons;	break;
			case DINNER: return 140 * quantityOfpersons; break;
			case FREE: cout << "meals - acording to menu prices (without discount).\n" << endl;
				break;
			default:
				throw OutOfCategoryExeption();
				break;
			}
			break;
		default:
			break;
		}
		
	}
};

class Biliard
{
public:
	int BilliardPlaying(float billiardPlayingTime)
	{
		return 150 * billiardPlayingTime;
	}
};

class Massage
{
public:
	float MassageProcedures(MassageCategories categor, float procedurTime, int quantityOfClients)
	{
		switch (categor)
		{
		case RELAX: return 250 * procedurTime * quantityOfClients; break;
		case THAI: return 300 * procedurTime * quantityOfClients; break;
		case MEDICATIVE: return 350 * procedurTime * quantityOfClients; break;
		case POINT: return 400 * procedurTime * quantityOfClients;	break;
		case CLASSIC: return 200 * procedurTime * quantityOfClients; break;
		default:
			throw OutOfCategoryExeption();
			break;
		}
	}
};

class ObarivResortFacade
{
private:
	HotelRoomBooking _HotelRoomBooking;
	Pool _Pool;
	Bar _Bar;
	Restourant _Restourant;
	Biliard _Biliard;
	Massage _Massage;

public:
	float CompleteRestPrice(AmusementTimeDuration amusTime, int roomsQuant, int quantityOfpersons, float poolTime, DrinksCategories drCategor, RestourantMealsCategoty mealCategor, float billiardPlayingTime, MassageCategories categor, float procedurTime )
	{
		int hotelRoomPrice = _HotelRoomBooking.RoomReserving(roomsQuant, amusTime);
		float poolTimePrice = _Pool.PoolUsing(amusTime, poolTime, quantityOfpersons);
		int barPrice = _Bar.ConsumingBarDrinks(amusTime, drCategor, quantityOfpersons);
		int restourantPrice = _Restourant.ConsumingRestourantMeal(mealCategor, amusTime, quantityOfpersons);
		int billiardPrice = _Biliard.BilliardPlaying(billiardPlayingTime);
		float massagePrice = _Massage.MassageProcedures(categor, procedurTime, quantityOfpersons);

		if (drCategor == 3 && mealCategor == 4)
			return hotelRoomPrice + poolTimePrice + billiardPrice + massagePrice;
		else if (drCategor == 3 && mealCategor != 4)
			return hotelRoomPrice + poolTimePrice + restourantPrice + billiardPrice + massagePrice;
		else if (mealCategor == 4 && drCategor != 3)
			return hotelRoomPrice + poolTimePrice + barPrice + billiardPrice + massagePrice;		
		else if(drCategor != 3 && mealCategor != 4)
			return hotelRoomPrice + poolTimePrice + barPrice + restourantPrice + billiardPrice + massagePrice;		
	}
};


int main()
{
	AmusementTimeDuration amusDuration;	
	DrinksCategories drCategor;
	RestourantMealsCategoty mealCategor;
	MassageCategories massageCategor;

	cout << "\t\t[ OBARIV RESORT ]\n" << endl;
	cout << "fill up possitions about rest:\n" << endl;

	int periodChoise;
	cout << "PERIOD OF REST: [ 1 ] - three hours (anytime).\n\t\t[ 2 ] - evening (18:00 - 1:00).\n\t\t[ 3 ] - night (22:00 - 8:00).\n\t\t[ 4 ] - holliday (saturday + sunday) " << endl;
	cout << "   option - ";
	cin >> periodChoise; cout << "\n\n";
	switch (periodChoise)
	{
	case 1: amusDuration = THREEHOURS; break;
	case 2: amusDuration = EVENING;	break;
	case 3: amusDuration = NIGHT; break;
	case 4: amusDuration = HOLLIDAY; break;
	default:
		break;
	}

	int quantityOfguests;
	cout << "QUANTUTY OF GUESTS: "; cin >> quantityOfguests;

	int roomChoise;
	cout << "\nROOM BOOKING:   [ 1 ] - 1 room reserve.\n\t\t[ 2 ] - 2 rooms reserve." << endl;
	cout << "   option - ";
	cin >> roomChoise; cout << "\n\n";

	int barChoise;
	cout << "BAR (choose some category of drinks for rest period):" << endl;
	cout << "\t\t[ 1 ] - basic (discount).\n\t\t[ 2 ] - expanded(discount).\n\t\t[ 3 ] - free(prices acording to menu)." << endl;
	cout << "   option - ";
	cin >> barChoise; cout << "\n\n";	switch (barChoise)
	{
	case 1: drCategor = BASIC; break;
	case 2: drCategor = EXPANDED; break;
	case 3: drCategor = _FREE; break;
	default:
		break;
	}

	int mealChoise;	
	cout << "MEAL (choose some category of meals acording to your rest period ):\n" << endl;
	cout << "\t\t[ THREE HOURS ] - available:  breakfast, lunch, dinner, no category(prices acording menu)." << endl;
	cout << "\t\t[ EVENING ]\t- available:  dinner, no category (prices acording menu)." << endl;
	cout << "\t\t[ NIGHT ]\t- available:  no category (prices acording menu)." << endl;
	cout << "\t\t[ HOLLIDAY ]\t- available:  breakfast, lunch, dinner, no category(prices acording menu).\n" << endl;	

	switch (periodChoise)
	{
	case 1:
		cout << "\t\t[ 1 ] - breakfast.\n\t\t[ 2 ] - lunch.\n\t\t[ 3 ] - dinner.\n\t\t[ 4 ] - no category (prices acording to menu)." << endl;
		break;
	case 2:
		cout << "\t\t[ 3 ] - dinner.\n\t\t[ 4 ] - no category (prices acording to menu)." << endl;
		break;
	case 3: 
		cout << "\t\t[ 4 ] - no category (prices acording to menu)." << endl;
		break;
	case 4:
		cout << "\t\t[ 1 ] - breakfast.\n\t\t[ 2 ] - lunch.\n\t\t[ 3 ] - dinner.\n\t\t[ 4 ] - no category (prices acording to menu)." << endl;
	default:
		break;
	}

	string answer;
	do
	{
		cout << "option: ";		
		cin >> mealChoise; 
		cout << "would you like offer smth else from menu? (yes/no): "; cin >> answer; cout << "\n\n";
		
	} while (answer != "no");

	switch (mealChoise)
	{
	case 1: mealCategor = BREAKFEST; break;
	case 2: mealCategor = LUNCH; break;
	case 3: mealCategor = DINNER; break;
	case 4: mealCategor = FREE; break;
	default:
		break;
	}
	
	float poolTime = 0;
	cout << "choose amount of time been in pool in hours(0,5  1  1,5...): " << endl;
	cout << "option - ";
	cin >> poolTime; cout << "\n\n";

	float billiardPlayingTime = 0;
	cout << "choose amount of time playing billiard in hours(0,5  1  1,5...): " << endl;
	cout << "option - ";
	cin >> billiardPlayingTime; cout << "\n\n";

	int massageChoise;
	cout << "choose kind of massage you may want:" << endl;
	cout << "\t\t[ 1 ] - relax.\n\t\t[ 2 ] - thai.\n\t\t[ 3 ] - medicative.\n\t\t[ 4 ] - point.\n\t\t[ 5 ] - classic. " << endl;
	cout << "option - ";
	cin >> massageChoise; cout << "\n\n";
	switch (massageChoise)
	{
	case 1: massageCategor = RELAX; break;
	case 2: massageCategor = THAI; break;
	case 3: massageCategor = MEDICATIVE; break;
	case 4: massageCategor = POINT; break;
	case 5: massageCategor = CLASSIC; break;
	default:
		break;
	}
	float procedurTime;
	int option;
	cout << "choose time of procedures:\n\t\t[ 1 ] - half an hour.\n\t\t[ 2 ] - hour.\n\t\t[ 3 ] - hour and half." << endl;
	cout << "option - ";
	cin >> option; cout << "\n\n";
	switch (option)
	{
	case 1: procedurTime = 0.5; break;
	case 2: procedurTime = 1; break;
	case 3: procedurTime = 1.5; break;
	default:
		break;
	}
	
	ObarivResortFacade rest;
	float totalPrice = rest.CompleteRestPrice(amusDuration, roomChoise, quantityOfguests, poolTime, drCategor, mealCategor, billiardPlayingTime, massageCategor, procedurTime);

	if (drCategor == _FREE || mealCategor == FREE)
		cout << "price for rest - " << totalPrice << "( + bill for drnks / meals. )" << endl;	
	else if(drCategor == _FREE && mealCategor == FREE)
		cout << "price for rest - " << totalPrice << " + bill for meals.  + bill from bar." << endl;
	else if(drCategor != _FREE && mealCategor != FREE)
		cout << "price for rest - " << totalPrice << endl;
	cout << "have a nice time!\n" << endl;
	

	system("pause");
	return 0;
}