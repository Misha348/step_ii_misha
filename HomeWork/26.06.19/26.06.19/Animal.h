#pragma once
#include <iostream>
#include <string>
using namespace std;

class Animal
{
private:
	string specious;
	string type;
	string name;
	int age;

public:
	Animal() : specious("abstract spc"), type("abstract type"), name("abstract name"), age(0) { }
	Animal(string specious, string type, string name, int age) : specious(specious), type(type), name(name), age(age) { }

	string GetType()
	{
		return type;
	}

	virtual void Sound()
	{
		cout << "animal provide some tone" << endl;
	}
	virtual void Type()
	{
		cout << "type - " << GetType() << endl;
	}
	void Show()
	{
		cout << "specious - " << specious << endl;
		cout << "type - " << type << endl;
		cout << "name - " << name << endl;
		cout << "age - " << age << endl;
	}

};
