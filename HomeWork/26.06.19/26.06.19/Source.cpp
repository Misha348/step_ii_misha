#include <iostream>
#include <string>
#include "Animal.h"
#include "DerivClasses.h"
using namespace std;

int main()
{
	Dog myDog("Volf", "dog", "Jim", 5, "doberman");
	Cat myCat("Forest cat", "domestic cat", "vasya", 7, "british");
	Parrot myParrot("Bird", "parrot", "kesha", 1, "long-beak", 30);
	Frog myFrog("Simple frog", "tropical frog", "Eddy", 2, "land/water");

	Animal myAnimal;

	Animal *dog = &myDog;	
	dog->Sound();
	dog->Type();
	dog->Show();
	cout << "============================\n" << endl;

	Animal *cat = &myCat;
	cat->Sound();
	cat->Type();
	cat->Show();
	cout << "============================\n" << endl;

	Animal *parrot = &myParrot;
	parrot->Sound();
	parrot->Type();
	parrot->Show();
	cout << "============================\n" << endl;

	Animal *frog = &myFrog;
	frog->Sound();
	frog->Type();
	frog->Show();
	cout << "============================\n" << endl;


	system("pause");
	return 0;
}