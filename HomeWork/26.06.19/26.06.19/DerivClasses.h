#pragma once
#include <iostream>
#include <string>
#include "Animal.h"
using namespace std;

class Dog : public Animal
{
private:
	string breed;

public:
	Dog(string specious, string type, string name, int age, string breed) :
		Animal(specious, type, name, age), breed(breed) { }

	virtual void Sound()
	{
		cout << "a dog provide tone \"bouf - bouf\"" << endl;
	}

	virtual void Type()
	{
		cout << "type - " << GetType() << endl;
	}

	void Show()
	{
		Animal::Show();
		cout << "breed - " << breed << endl;
	}

};

class Cat : public Animal
{
private:
	string breed;

public:
	Cat(string specious, string type, string name, int age, string breed) :
		Animal(specious, type, name, age), breed(breed) { }

	virtual void Sound()
	{
		cout << "a cat provide tone \"miu - miu\"" << endl;
	}

	virtual void Type()
	{
		cout << "type - " << GetType() << endl;
	}

	void Show()
	{
		Animal::Show();
		cout << "breed - " << breed << endl;
	}

};

class Parrot : public Animal
{
private:
	string beakType;
	int wingScope;

public:
	Parrot(string specious, string type, string name, int age, string beakType, int wingScope) :
		Animal(specious, type, name, age), beakType(beakType), wingScope(wingScope) { }

	virtual void Sound()
	{
		cout << "a parrot provide tone \" kviry - kvirly\"" << endl;
	}

	virtual void Type()
	{
		cout << "type - " << GetType() << endl;
	}

	void Show()
	{
		Animal::Show();
		cout << "beakType - " << beakType << endl;
		cout << "wingScope - " << wingScope << endl;
	}
};

class Frog : public Animal
{
private:
	string areal;


public:
	Frog(string specious, string type, string name, int age, string areal) :
		Animal(specious, type, name, age), areal(areal) { }

	virtual void Sound()
	{
		cout << "a frog provide tone \"vak - vak\"" << endl;
	}

	virtual void Type()
	{
		cout << "type - " << GetType() << endl;
	}

	void Show()
	{
		Animal::Show();
		cout << "areal - " << areal << endl;
	}

};