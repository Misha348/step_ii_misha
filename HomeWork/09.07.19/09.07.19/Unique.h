#pragma once
#include <iostream>
#include <memory>
//using std::swap;

template<typename T>
class my_unique_ptr
{
private:
	T *ptr;

public:
	my_unique_ptr(): ptr(nullptr) {}
	my_unique_ptr(T *ptr): ptr(ptr) {}
	~my_unique_ptr()
	{
		delete ptr;
	}

	/*template<typename T>
	my_unique_ptr<T> (my_unique_ptr<T> &&obj)
	{
		ptr = obj.ptr;
		obj.ptr = nullptr;
	}*/

	/*template<typename T>
	my_unique_ptr<T> &operator=(my_unique_ptr <T> &&obj)
	{
		if (this != &obj)
		{
			ptr = obj.ptr;
			obj.ptr = nullptr;
		}
		return *this;
	}*/	
	
	my_unique_ptr(my_unique_ptr &&obj)
	{
		ptr = obj.ptr;
		obj.ptr = nullptr;
	}

	my_unique_ptr &operator=(my_unique_ptr &&obj)
	{
		if (this != &obj)
		{
			ptr = obj.ptr;
			obj.ptr = nullptr;
		}
		return *this;
	}

	T operator*() const { return *ptr; }
	T *operator->() const { return ptr; } // * ?

	
	T* Get() 
	{
		return ptr;
	}

	bool IsNull() 
	{
		if ((Get() != nullptr))
			return true;
		else
			false;
	}
	
	T* Release()
	{
		T *tmp = ptr;
		ptr = nullptr;
		return tmp;
	}

	template<typename T>
	void Reset(T *ptr = nullptr)
	{
		T* tmp = this->ptr;
		this->ptr = ptr;
		if (tmp != nullptr)
			delete tmp;
		/*this->ptr = ptr;
		delete ptr;*/
		//return *this;
	}

	template<typename T>
	void Swap(my_unique_ptr<T> &obj)
	{
		//swap(ptr, obj.ptr);
		T *tmp = obj.ptr;
		obj = ptr;
		ptr = tmp;
	}	
};



