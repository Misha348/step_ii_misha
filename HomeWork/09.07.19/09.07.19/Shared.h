#pragma once
#include <iostream>
#include <memory>

template<typename T>
class my_shared_ptr
{
private:
	T *ptr;
	int *count;

public:
	my_shared_ptr() : ptr(nullptr), count(nullptr) {}
	my_shared_ptr(T *ptr)
	{
		this->ptr = ptr;
		count = new int(1);
	
	} 
	~my_shared_ptr() // not full ?
	{
		delete ptr;
	}
	
	my_shared_ptr(my_shared_ptr &obj)
	{
		ptr = obj.ptr;
		count = obj.count;
		//obj.ptr = nullptr;
		//obj.count = nullptr; // ?? 
		*(count++); // 
	}

	my_shared_ptr &operator=(my_shared_ptr &obj)
	{
		if (this != &obj)
		{
			*(count)--;			
		}
		ptr = obj.ptr;
		count = obj.count;
		obj.ptr = nullptr;
		*(count)++;

		return *this;
	}

	T operator*() const { return *ptr; }
	T *operator->() const { return ptr; } // * ?

	
	T* Get() 
	{
		return ptr;
	}

	bool IsNull() 
	{
		if ((Get() != nullptr))
			return true;
		else
			false;
	}

	//template<typename T>
	void Reset(T *ptr = nullptr)
	{/*
		T* tmp = this->ptr;
		this->ptr = ptr;
		if (tmp != nullptr)
			delete tmp;	*/
		if (count != nullptr)
		{
			if (*count == 1)
			{
				delete count;
				delete ptr;
			}
		}
		else
			*(count--);		
	}

	template<typename T>
	void Swap(my_shared_ptr<T> &obj)
	{
		//swap(ptr, obj.ptr);
		T *tmp = obj.ptr;
		obj = ptr;
		ptr = tmp;
	}	

	bool IsUnique()
	{
		if (*count == 1)
			return true;
		else
			return false;
	}

	int UseCount()
	{
		return *count;
	}
};