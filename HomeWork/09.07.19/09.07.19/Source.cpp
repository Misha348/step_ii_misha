#include <iostream>
#include <memory>
#include "Unique.h"
#include "Shared.h"


int main()
{
	std::cout << "\T\TUNIQUE_PTR" << std::endl;

	my_unique_ptr<int> ptr(new int(10));
	std::cout << "(just) ptr = " << *ptr << std::endl;

	int *ptr2 = ptr.Release();
	std::cout << "ptr2 = ptr.Release = " << *ptr2 << std::endl;		

	ptr.Reset(new int(9));	
	std::cout << "(ptr.Reset) ptr = " << *ptr << std::endl;

	my_unique_ptr<int> ptr4(new int(5));
	std::cout << "(just) ptr4 = " << *ptr4 << std::endl;
	ptr.Swap(ptr4);
	std::cout << "ptr.Swap(ptr4)" << std::endl;
	std::cout << "ptr = " << *ptr << std::endl;
	std::cout << "ptr4 = " << *ptr4 << std::endl;	

	ptr.IsNull();
	std::cout << "=============================" << std::endl;


	//std::cout << "\t\tSHARED_PTR" << std::endl;
	//my_shared_ptr<int> ptr1(new int(5));
	//std::cout << "(Use_Count) ptr1 - " << ptr1.UseCount() << "\n";
	//std::cout << "(Unique ) ptr1 - " << ((ptr1.IsUnique()) ? "true" : "false") << "\n\n";

	//my_shared_ptr<int> ptr2(new int(10));
	//std::cout << "(Use_Count ) ptr2 - " << ptr2.UseCount() << "\n";
	//std::cout << "(Unique )ptr2 - " << ((ptr2.IsUnique()) ? "true" : "false") << "\n\n";

	//my_shared_ptr<int> ptr3(ptr1);
	////std::cout << "(Use_Count) ptr1 - " << ptr1.UseCount() << "\n";
	////std::cout << "(Unique ) ptr1 - " << ((ptr1.IsUnique()) ? "true" : "false") << "\n\n";

	//std::cout << "(Use_Count ) ptr3 - " << ptr3.UseCount() << "\n";
	//std::cout << "(Unique )ptr3 - " << ((ptr3.IsUnique()) ? "true" : "false") << "\n\n";
	//std::cout << "=============================" << std::endl; 

	//ptr3 = ptr2;
	//std::cout << "(Use_Count ) ptr2 - " << ptr2.UseCount() << "\n";
	//std::cout << "(Unique )ptr2 - " << ((ptr2.IsUnique()) ? "true" : "false") << "\n\n";

	//std::cout << "(Use_Count ) ptr3 - " << ptr3.UseCount() << "\n";
	//std::cout << "(Unique )ptr3 - " << ((ptr3.IsUnique()) ? "true" : "false") << "\n\n";

	system("pause");
	return 0;
}