# include <iostream>
# include <string>
# include "Transport.h"
# include "Lading.h"
using namespace std;

void capacityTable()
{
	cout << "1 - not more than 2 cub.m" << endl;
	cout << "2 - not more than 5 cub.m" << endl;
	cout << "3 - not more than 10 cub.m" << endl;
	cout << "4 - not more than 20 cub.m" << endl;
	cout << "5 - not more than 50 cub.m" << endl;
	cout << "6 - not more than 10 cub.m" << endl;
}

int main()
{
	DeliveryTransportFactory factory;

	OrdersInAngar order;
	order.AddLading("letters", 2, 3);
	order.AddLading("fish", 1, 2);
	//order.AddLading("logs", 40, 20);

	Transport *transport; 

	for (Order item : order.orders)
	{
		transport = factory.CreateDeliveryTransport(item);

		transport->ShowTransportInfo();
		cout << endl;
	}


	system("pause");
	return 0;
}