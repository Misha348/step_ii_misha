#pragma once
# include <iostream>
# include <string>
# include <vector>
using namespace std;

class Order
{	
	string name;
	int capacity;
	int termForDelivery;
public:
	
	Order() {}
	Order(string orderName, int ladCapacity, int deliveryTerm) : name(orderName), capacity(ladCapacity), termForDelivery(deliveryTerm) {}

	int GetCapasity()
	{
		return capacity;
	}

	int GetTermForDelivery()
	{
		return termForDelivery;
	}

	void ShowOrder()
	{
		cout << "order name - " << name << endl;
		cout << "order capacity - " << capacity << endl;
		cout << "time for delivery - " << termForDelivery << endl;
	}
};

class OrdersInAngar
{		
public:
	vector<Order> orders;
	void AddLading(string name, int capacity, int termFordelivery)
	{
		Order newLading(name, capacity, termFordelivery);
		orders.push_back(newLading);
	}	
};