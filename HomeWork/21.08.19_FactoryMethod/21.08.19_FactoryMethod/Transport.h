#pragma once
# include <iostream>
# include <string>
#include "Lading.h"
using namespace std;


enum TransporType { CAR = 1, TRUCK, TRAIN, PLAIN, SHIP };
enum TranspCapacityCategor { FIRST = 3, SECOND = 10, THIRD = 20, FOURTH = 50, FIFTH = 100 };
enum TransportDeliveryCategory { _FIRST = 3, _SECOND = 7, _THIRD = 14, _FOURTH = 21, _FIFTH = 31 };

class Transport
{
protected:
	TransporType transpType;
	int averMaxPathPerDay;
public:
	const int capacity;
	Transport() : capacity(0) {}
	Transport(TransporType newTranspType, int newCapacity, int newSpeedAbility) : capacity(newCapacity)
	{
		this->transpType = newTranspType;
		this->averMaxPathPerDay = newSpeedAbility;
	}

	virtual void ShowTransportInfo() = 0;
};

class Car : public Transport
{
public:
	static const int cap = 2;
	Car() : Transport(TransporType::CAR, 2, 3) {}
	void ShowTransportInfo()
	{
		cout << "Type - " << transpType << endl;
		cout << "capacity - " << capacity << endl;
		cout << "averSpeed - " << averMaxPathPerDay << endl;
	}

	static int GetC()
	{
		return cap;
	}
};

class Truck : public Transport
{
public:
	Truck() : Transport(TransporType::TRUCK, 10, 7) {}
	void ShowTransportInfo()
	{
		cout << "Type - " << transpType << endl;
		cout << "capacity - " << capacity << endl;
		cout << "averSpeed - " << averMaxPathPerDay << endl;
	}
};

class DeliveryTransportFactory
{
public:
	Transport *CreateDeliveryTransport(Order& obj)
	{
		TransporType transport;
			
		if( (TranspCapacityCategor::FIRST >= (obj.GetCapasity())) &&
			(TransportDeliveryCategory::_FIRST >= (obj.GetTermForDelivery())))
		{
			transport = TransporType::CAR;
		}
		else if ( (22 - (obj.GetCapasity()) <= 2) && (9 - (obj.GetTermForDelivery()) <= 2) )
		{
			transport = TransporType::TRUCK;
		}

		switch (transport)
		{
		case CAR:
			return new Car();
		case TRUCK:
			break;
		case TRAIN:
			break;
		case PLAIN:
			break;
		case SHIP:
			break;
		default:
			break;
		}
		
	}
};