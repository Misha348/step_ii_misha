#define _CRT_SECURE_NO_WARNINGS
# include <iostream>
# include <string>
# include <ctime>
#include <fstream>
#include <Windows.h>
#include <conio.h>
#include <vector>
#include <atltime.h>
using namespace std;

string Now()
{	
	string time;
	CTime obj;
	obj = obj.GetCurrentTime();
	int Hour = obj.GetHour();
	int Minutes = obj.GetMinute();
	int Seconds = obj.GetSecond();
	time = to_string(Hour) + ":" + to_string(Minutes) + ":" + to_string(Seconds);
	return time;
}

class OutOfConnectionExeption
{
public:
	void OutOfConnectionMessage()
	{
		cout << "error with connection." << endl;
	}
};

struct LogData
{
	string name;
	string surname;
	string tel;
	string email;
	string password;
	string time;	

	void FillLogData()
	{		
		cout << "name:  "; cin >> name;
		cout << "surname:  ";  cin >> surname;
		cout << "phone:  "; cin >> tel;
		cout << "email:  "; cin >> email;
		cout << "passwd:  ";  cin >> password;	
		time = Now();		
	}
};

class DataBase
{
private:
	const string conectionToRemoteDataBasePath = "RDB.txt";		

protected:
	int signal;
	bool isConnect;
	string userConectionPathToRemoteDataBase;

public:

	DataBase() {}

	void SetUserConectionPath(string userConectionPath)
	{
		this->userConectionPathToRemoteDataBase = userConectionPath;
	}

	void SetResultOfConectionToRDB(string userConectionPath)
	{
		if (userConectionPath == conectionToRemoteDataBasePath)
			isConnect = true;	
		else
			isConnect = false;		
	}	

	bool GetIsConnect()
	{
		return isConnect;
	}

	string GetConectionPathToRemoteDataBase()
	{
		return conectionToRemoteDataBasePath;
	}

	int checkResultOfConectionSignalQuality()
	{				
		if (isConnect)
		{
			
			this->signal = (rand() % 100 + 1);
			return signal;					
		}
		else
			throw OutOfConnectionExeption();
	}

	virtual void Log(LogData &log)
	{
		ofstream wrFile(conectionToRemoteDataBasePath, ios_base::app);
		wrFile << log.name << " "
			<< log.surname << " "
			<< log.tel << " "
			<< log.email << " "
			<< log.password << " "
			<< log.time << "\n";			
		wrFile.close();
	}
};

class ProxyDataBase: public DataBase
{
private:
	const string proxyConectionPath = "LDB.txt";
	LogData log_;
	DataBase db;

public:
	ProxyDataBase() {}
	ProxyDataBase(LogData &log)
	{
		this->log_ = log;
	}	

	void Log(LogData &log) override
	{
		ofstream wrFile(proxyConectionPath, ios_base::app);
		wrFile << log.name << " "
			<< log.surname << " "
			<< log.tel << " "
			<< log.email << " "
			<< log.password << " "
			<< log.time << "\n";		
		wrFile.close();
	}	

	void RemoveLogDataFromLocDataBaseToRemDataBase()
	{
		ifstream rdFile("LDB.txt", ios_base::in);	

		LogData tmpData;
		while (!rdFile.eof())
		{			
			rdFile >> tmpData.name;
			rdFile >> tmpData.surname;
			rdFile >> tmpData.tel;
			rdFile >> tmpData.email;
			rdFile >> tmpData.password;
			rdFile >> tmpData.time;

			
		}
		rdFile.close();

		DataBase::Log(tmpData);	
	}

	void CheckingConnectionToRemoteDataBase()
	{		
		int firstConnection = (rand() % 100 + 1);
		if (firstConnection < 40)
		{
			cout << "connection exist but signal bad (" << firstConnection << "% ), file -> localDB.\n try to connect once more: " << endl;
			Log(log_);
			_getch();			
			do
			{
				signal = checkResultOfConectionSignalQuality();
				cout << "connection exist but signal bad (" << firstConnection << "% ), file -> localDB.\n try to connect once more: " << endl;
				Log(log_);
				_getch();
			} while (signal < 40);
			RemoveLogDataFromLocDataBaseToRemDataBase();
			cout << "connection exist, signal good " << signal <<  " file -> remoteDB. " << endl;
		}
		else
		{
			cout << "connection exist, signal good ( " << firstConnection << "% ) file -> remoteDB. " << endl;
			db.Log(log_);
		}		
	}
};


int main()
{
	srand(time(NULL));
	string userEnteredPath;
	cout << "enter path for connection to remote data base: "; cin >> userEnteredPath;

	cout << "  [fill up logs data]:" << endl;
	LogData log;
	log.FillLogData();	

	ProxyDataBase prxDB(log);
	prxDB.SetUserConectionPath(userEnteredPath);
	prxDB.SetResultOfConectionToRDB(userEnteredPath);
	prxDB.CheckingConnectionToRemoteDataBase();

	system("pause");
	return 0;
}