#pragma once

class String
{
private:

	char *str;
	int size;	

public:

	void Show();
	void SetString(char *myStr, int size);
	void SetStringFromKeyboard(char *myString, int size);
	char* GetString();
	void ClearString();
	void Append(char* myString, char* newStr);
	int GetLength(char *myString, int size);

	String();
	String(char sbm, int size);
	String(char *_str, int size);
	~String();

};