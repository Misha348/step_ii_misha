#include <iostream>
#include "String.h"

using namespace std;

int main()
{
	char smb;
	int size = 0;
	cout << "\t CONSTRUCTOR I\n" << endl;
	cout << "enter size: "; cin >> size;
	cout << "enter some symbol: "; cin >> smb;
	String str1 = String(smb, size);
	str1.Show();

	cout << "========================================" << endl;
	cin.ignore();

	cout << "\t CONSTRUCTOR II\n" << endl;	
	char myString[255];	 
	int size2 = 0;	
	cout << "enter some string: ";	cin.getline(myString, 255);
	size2 = strlen(myString);	
	String str2 = String(myString, size2);
	str2.Show();

	cout << "========================================" << endl;	

	cout << endl;
	cout << "Seter & Getter for hardcored string:" << endl;
	char myStr[255] = { "some abstract string." };
	int size3 = 0;	
	size3 = strlen(myStr);
	str2.SetString(myStr, size3);
	str2.GetString();
	str2.Show();

	cout << "========================================" << endl;

	cout << "Seter & Getter for keyboard_string string:" << endl;
	str2.SetStringFromKeyboard(myString, size2);		
	str2.GetString();
	str2.Show();

	cout << "========================================" << endl;

	str2.GetLength(myString, size2);
	cout << "quantity of symb: " << str2.GetLength(myString, size2) << endl;

	cout << "========================================" << endl;	

	char newStr[255];
	cout << "enter some string to add: ";	cin.getline(newStr, 255);
	str2.Append(myString, newStr);
	cout << "========================================" << endl;
	
	str2.ClearString();
	cout << "Cleaned." << endl;
	str2.Show();


	system("pause");
	return 0;
}
