#include <iostream>
#include <string>
#include "String.h"
using namespace std;

void String::Show()
{
	cout << "your string: "<< str << endl;
}

void String::SetString(char *myStr, int size)
{
	if (str != nullptr)
		delete[] str;
	str = new char[size + 1];
	for (int i = 0; i < size; i++)
	{
		str[i] = myStr[i];
	}
	str[size] = 0;
}

void String::SetStringFromKeyboard(char *myString, int size )
{	
	if (str != nullptr)	
		delete[] str;
	str = new char[size + 1];
	for (int i = 0; i < size; i++)
	{
		str[i] = myString[i];
	}
	str[size] = 0;
}

char*  String::GetString()
{
	if (str)
	return str;
}

void String::ClearString()
{	
	if (str != nullptr)
		delete[] str;
	/*myString;
	memset(myString, NULL, 255);*/
}

void String::Append(char* myString, char* newStr)
{
	char result[255] = " ";

	strcat_s(result, 255, myString);
	strcat_s(result, 255, newStr);
	cout << result << endl;
}

int String::GetLength(char *myString, int size)
{
	int sbmNumb = 0;
	sbmNumb = strlen(myString);

	return sbmNumb;
}

String::String()
{
	int size = 0;
	char *str = nullptr;	
}

String::String(char smb, int size)
{	
	if (str != nullptr)	delete[] str;
	this->size = size;
	str = new char[size+1];
	for (int i = 0; i < size; i++)
	{
		str[i] = smb;
	}
	str[size] = 0;
}

String::String(char *_str, int size)
{
	if (str != nullptr)	delete[] str;
	str = new char[size + 1];
	for (int i = 0; i < size; i++)
	{
		str[i] = _str[i];
	}
	str[size] = 0;
}

String::~String()
{
	////delete[]str;
}