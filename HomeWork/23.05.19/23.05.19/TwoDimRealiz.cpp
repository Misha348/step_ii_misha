#include <iostream>;
#include <ctime>;
#include "ClassTwoDimArr.h"
using namespace std;

void TwoDimArr::AddRow(int *arr1d,  int ind)
{
	rows++;
	int **newArr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		newArr[i] = new int[cols];
	}	

	for (int i = 0; i < ind; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}			
	
	for (int j = 0; j < cols; j++)
	{
		newArr[ind][j] = arr1d[j];
	}			

	for (int i = ind + 1; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i - 2][j];
		}
	}	
	
	for (int i = 0; i < rows - 1; i++)
	{
		delete[] arr[i];
	}
	delete[] arr; 
	arr = newArr;
	//delete[] arr;
	//arr = newArr;
}

void TwoDimArr::DeleteRowByPos(int pos)
{
	rows--;
	int **newArr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		newArr[i] = new int[cols];
	}

	for (int i = 0; i < pos; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i][j];
		}
	}

	for (int i = pos; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			newArr[i][j] = arr[i + 1][j];
		}
	}
	delete[] arr;
	arr = newArr;
}

void TwoDimArr::ShowArr()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			cout << arr[i][j] << "\t";
		}
		cout << endl;
	}		
}

void TwoDimArr::Clear()
{
}

TwoDimArr::TwoDimArr()
{
	int **arr = nullptr;
	int cols = 0;
	int rows = 0;
}

TwoDimArr::TwoDimArr(int rows, int cols, int value)
{
	this->rows = rows;
	this->cols = cols;

	arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		arr[i] = new int[cols];
	}		

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			arr[i][j] = value;
		}
	}	
}

TwoDimArr::TwoDimArr(int **someArr, int rows, int cols)
{
	this->rows = rows;
	this->cols = cols;
	
	arr = new int *[rows];
	for (int i = 0; i < rows; i++)
		arr[i] = new int[cols];

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			arr[i][j] = someArr[i][j];
}

TwoDimArr::~TwoDimArr()
{
	for (int i = 0; i < rows; i++)
	{
		delete[] arr[i];
	}
	delete[] arr;
}




