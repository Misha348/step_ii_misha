#include <iostream>
#include <ctime>
#include "ClassTwoDimArr.h"
using namespace std;

void Menu()
{
	cout << "[ 1 ] - add row.\n[ 2 ] - delete row.\n[ 3 ] - show arr.\n[ 4 ] - clear arr.\n[ 5 ] - Exit." << endl;
}

int main()
{
	srand(unsigned(time(NULL)));
	
	int rows = 0, cols = 0, value = 0;

	cout << "Enter quantity of rows: "; cin >> rows;
	cout << "Enter quantity of cols: "; cin >> cols;
	cout << "Enter value: "; cin >> value;
	cout << "================================" << endl;

	cout << "\tI CONSTRUCTOR\n" << endl;
	//TwoDimArr arr = TwoDimArr(rows, cols, value);
	TwoDimArr arr9(rows, cols, value);
	arr9.ShowArr();
	
	cout << "================================" << endl;
	cout << "\tII CONSTRUCTOR\n" << endl;

	int **Arr = new int *[rows];
	for (int i = 0; i < rows; i++)
	{
		Arr[i] = new int[cols];
	}		

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			Arr[i][j] = rand() % 10;
	}
		

	TwoDimArr arr1(Arr, rows, cols);
	arr1.ShowArr();
	cout << "================================" << endl;

	bool exit = false;
	int choise = 0;	

	while (exit = true)
	{
		Menu();
		cin >> choise;
		if (choise == 1)
		{
			int p = 0;
			int rowPosToAdd = 0;
			cout << "Enter row's posit. to add: ";
			do
			{
				cin >> p;
				rowPosToAdd = p;
				if (rowPosToAdd > rows || rowPosToAdd < 0)
					cout << "No such amount of rows in arr. Try again:";

			} while (rowPosToAdd >= rows || rowPosToAdd < 0);

			int *arr1d = new int[cols];
			for (int i = 0; i < cols; i++)
			{
				arr1d[i] = 888;
			}

			arr9.AddRow(arr1d, rowPosToAdd - 1);
			delete[]arr1d;
			rows++;
		}
		if (choise == 2)
		{
			int delPos = 0;
			cout << "Enter row's posit. to del: "; cin >> delPos;
			arr1.DeleteRowByPos(delPos);
		}
		if (choise == 3)
		{
			arr9.ShowArr();
		}
		if (choise == 4)
		{
			arr1.~TwoDimArr();
		}
		if (choise == 5)
		{
			exit = false;
			break;
		}
	}
	

	

		
	


	system("pause");
	return 0;
}
