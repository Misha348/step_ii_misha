#pragma once
#include <iostream>;
using namespace std;

class TwoDimArr
{
private:
	int **arr;
	int cols, rows;

public:
	void AddRow(int *arr1d, int pos);
	void DeleteRowByPos(int pos);
	void ShowArr();
	void Clear();

	TwoDimArr();
	TwoDimArr(int value, int rows, int cols);
	TwoDimArr(int **someArr, int newRows, int newCols);
	~TwoDimArr();

};

//int **arr, int rows, int cols